#include "../config.h"

/* Ubuntu needs it */

#define HAVE_STDINT_H 1
#define HAVE_MBSTATE_T 1
#define HAVE_NAMESPACES 1
#undef NOT_USING_DJVU_NAMESPACE
#define AUTOCONF 1
#define HAVE_STDINCLUDES 1
#define THREADMODEL POSIXTHREADS
