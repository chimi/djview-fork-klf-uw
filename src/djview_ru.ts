<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.0" language="ru">
<context>
    <name>QApplication</name>
    <message>
        <location filename="djview.cpp" line="415"/>
        <source>Option &apos;-fix&apos; is deprecated.</source>
        <translation>Опция &apos;-fix&apos; устарела.</translation>
    </message>
</context>
<context>
    <name>QDjView</name>
    <message>
        <location filename="qdjview.cpp" line="147"/>
        <source>100%</source>
        <comment>zoomCombo</comment>
        <translation>100%</translation>
    </message>
    <message>
        <location filename="qdjview.cpp" line="484"/>
        <source>&amp;100%</source>
        <comment>Zoom|</comment>
        <translation>&amp;100%</translation>
    </message>
    <message>
        <location filename="qdjview.cpp" line="143"/>
        <source>1:1</source>
        <comment>zoomCombo</comment>
        <translation>1:1</translation>
    </message>
    <message>
        <location filename="qdjview.cpp" line="146"/>
        <source>150%</source>
        <comment>zoomCombo</comment>
        <translation>150%</translation>
    </message>
    <message>
        <location filename="qdjview.cpp" line="478"/>
        <source>150%</source>
        <comment>Zoom|</comment>
        <translation>150%</translation>
    </message>
    <message>
        <location filename="qdjview.cpp" line="3229"/>
        <source>%1 files (*.%2);;</source>
        <comment>save image filter</comment>
        <translation>%1 файлы (*.%2);;</translation>
    </message>
    <message>
        <location filename="qdjview.cpp" line="145"/>
        <source>200%</source>
        <comment>zoomCombo</comment>
        <translation>200%</translation>
    </message>
    <message>
        <location filename="qdjview.cpp" line="472"/>
        <source>&amp;200%</source>
        <comment>Zoom|</comment>
        <translation>&amp;200%</translation>
    </message>
    <message>
        <location filename="qdjview.cpp" line="144"/>
        <source>300%</source>
        <comment>zoomCombo</comment>
        <translation>300%</translation>
    </message>
    <message>
        <location filename="qdjview.cpp" line="466"/>
        <source>&amp;300%</source>
        <comment>Zoom|</comment>
        <translation>&amp;300%</translation>
    </message>
    <message>
        <location filename="qdjview.cpp" line="496"/>
        <source>&amp;50%</source>
        <comment>Zoom|</comment>
        <translation>&amp;50%</translation>
    </message>
    <message>
        <location filename="qdjview.cpp" line="149"/>
        <source>50%</source>
        <comment>zoomCombo</comment>
        <translation>50%</translation>
    </message>
    <message>
        <location filename="qdjview.cpp" line="148"/>
        <source>75%</source>
        <comment>zoomCombo</comment>
        <translation>75%</translation>
    </message>
    <message>
        <location filename="qdjview.cpp" line="490"/>
        <source>&amp;75%</source>
        <comment>Zoom|</comment>
        <translation>&amp;75%</translation>
    </message>
    <message>
        <location filename="qdjview.cpp" line="4038"/>
        <source>About DjView</source>
        <translation>О DjView</translation>
    </message>
    <message>
        <location filename="qdjview.cpp" line="588"/>
        <source>&amp;About DjView...</source>
        <translation>&amp;О DjView...</translation>
    </message>
    <message>
        <location filename="qdjview.cpp" line="3185"/>
        <location filename="qdjview.cpp" line="3232"/>
        <source>All files</source>
        <comment>save filter</comment>
        <translation>Все файлы</translation>
    </message>
    <message>
        <location filename="qdjview.cpp" line="160"/>
        <source>Background</source>
        <comment>modeCombo</comment>
        <translation>Фон</translation>
    </message>
    <message>
        <location filename="qdjview.cpp" line="615"/>
        <source>&amp;Background</source>
        <comment>Display|</comment>
        <translation>&amp;Фон</translation>
    </message>
    <message>
        <location filename="qdjview.cpp" line="526"/>
        <source>&amp;Backward</source>
        <comment>Go|</comment>
        <translation>&amp;Назад</translation>
    </message>
    <message>
        <location filename="qdjview.cpp" line="528"/>
        <source>Backward in history.</source>
        <translation>Вернуться назад по истории.</translation>
    </message>
    <message>
        <location filename="qdjview.cpp" line="3486"/>
        <source>Cannot decode document.</source>
        <translation>Не удается декодировать документ.</translation>
    </message>
    <message>
        <location filename="qdjview.cpp" line="3484"/>
        <source>Cannot decode page %1.</source>
        <translation>Не удается декодировать страницу &apos;%1&apos;.</translation>
    </message>
    <message>
        <location filename="qdjview.cpp" line="3248"/>
        <source>Cannot determine file format.
Filename &apos;%1&apos; has no suffix.</source>
        <translation>Невозможно определить формат файла.
Имя файла &apos;%1&apos; не имеет суффикса.</translation>
    </message>
    <message>
        <location filename="qdjview.cpp" line="2582"/>
        <location filename="qdjview.cpp" line="2612"/>
        <source>Cannot find page named: %1</source>
        <translation>Не удается найти страницу с названием %1</translation>
    </message>
    <message>
        <location filename="qdjview.cpp" line="2550"/>
        <source>Cannot find page numbered: %1</source>
        <translation>Не удается найти страницу с номером %1</translation>
    </message>
    <message>
        <location filename="djview.cpp" line="435"/>
        <source>cannot open &apos;%1&apos;.</source>
        <translation>не удается открыть &apos;%1&apos;.</translation>
    </message>
    <message>
        <location filename="qdjview.cpp" line="2483"/>
        <source>Cannot open file &apos;%1&apos;.</source>
        <translation>Не удается открыть файл &apos;%1&apos;.</translation>
    </message>
    <message>
        <location filename="qdjview.cpp" line="2522"/>
        <source>Cannot open URL &apos;%1&apos;.</source>
        <translation>Не удается открыть URL &apos;%1&apos;.</translation>
    </message>
    <message>
        <location filename="qdjview.cpp" line="3790"/>
        <source>Cannot resolve link &apos;%1&apos;</source>
        <translation>Невозможно разрешить ссылку &apos;%1&apos;</translation>
    </message>
    <message>
        <location filename="qdjview.cpp" line="3818"/>
        <source>Cannot spawn a browser for url &apos;%1&apos;</source>
        <translation>Невозможно запустить браузер для URL &apos;%1&apos;</translation>
    </message>
    <message>
        <location filename="qdjview.cpp" line="3203"/>
        <location filename="qdjview.cpp" line="3268"/>
        <source>Cannot write file &apos;%1&apos;.
%2.</source>
        <translation>Не удается записать файл &apos;%1&apos;.
%2.</translation>
    </message>
    <message>
        <location filename="qdjview.cpp" line="4251"/>
        <source>&amp;Clear History</source>
        <translation>&amp;Очистить историю</translation>
    </message>
    <message>
        <location filename="qdjview.cpp" line="384"/>
        <source>&amp;Close</source>
        <comment>File|</comment>
        <translation>&amp;Закрыть</translation>
    </message>
    <message>
        <location filename="qdjview.cpp" line="393"/>
        <source>Close all windows and quit the application.</source>
        <translation>Закрыть все окна и завершить приложение.</translation>
    </message>
    <message>
        <location filename="qdjview.cpp" line="387"/>
        <source>Close this window.</source>
        <translation>Закрыть окно.</translation>
    </message>
    <message>
        <location filename="qdjview.cpp" line="595"/>
        <source>&amp;Color</source>
        <comment>Display|</comment>
        <translation>&amp;Цвет</translation>
    </message>
    <message>
        <location filename="qdjview.cpp" line="157"/>
        <source>Color</source>
        <comment>modeCombo</comment>
        <translation>Цвет</translation>
    </message>
    <message>
        <location filename="qdjview.cpp" line="659"/>
        <source>&amp;Continuous</source>
        <comment>Layout|</comment>
        <translation>&amp;Непрерывный</translation>
    </message>
    <message>
        <location filename="qdjview.cpp" line="1031"/>
        <source>Control Left Mouse Button</source>
        <translation>Control + левая кнопка мыши</translation>
    </message>
    <message>
        <location filename="qdjview.cpp" line="3845"/>
        <source>Copy image (%1x%2 pixels)</source>
        <translation>Скопировать изображение (%1x%2 пикселей)</translation>
    </message>
    <message>
        <location filename="qdjview.cpp" line="3838"/>
        <source>Copy text (%1)</source>
        <translation>Скопировать текст (%1)</translation>
    </message>
    <message>
        <location filename="qdjview.cpp" line="370"/>
        <source>Create a new DjView window.</source>
        <translation>Открыть новое окно DjView.</translation>
    </message>
    <message>
        <location filename="qdjview.cpp" line="403"/>
        <source>Ctrl+E</source>
        <comment>File|ExportAs</comment>
        <translation>Ctrl+E</translation>
    </message>
    <message>
        <location filename="qdjview.cpp" line="415"/>
        <source>Ctrl+F</source>
        <comment>Edit|Find</comment>
        <translation>Ctrl+F</translation>
    </message>
    <message>
        <location filename="qdjview.cpp" line="641"/>
        <source>Ctrl+F10</source>
        <comment>Settings|Show toolbar</comment>
        <translation>Ctrl+F10</translation>
    </message>
    <message>
        <location filename="qdjview.cpp" line="653"/>
        <source>Ctrl+F11</source>
        <comment>View|FullScreen</comment>
        <translation>Ctrl+F11</translation>
    </message>
    <message>
        <location filename="qdjview.cpp" line="432"/>
        <source>Ctrl+F2</source>
        <comment>Edit|Select</comment>
        <translation>Ctrl+F2</translation>
    </message>
    <message>
        <location filename="qdjview.cpp" line="421"/>
        <source>Ctrl+F3</source>
        <comment>Edit|Find Next</comment>
        <translation>Ctrl+F3</translation>
    </message>
    <message>
        <location filename="qdjview.cpp" line="661"/>
        <source>Ctrl+F4</source>
        <comment>Layout|Continuous</comment>
        <translation>Ctrl+F4</translation>
    </message>
    <message>
        <location filename="qdjview.cpp" line="669"/>
        <source>Ctrl+F5</source>
        <comment>Layout|SideBySide</comment>
        <translation>Ctrl+F5</translation>
    </message>
    <message>
        <location filename="qdjview.cpp" line="634"/>
        <source>Ctrl+F9</source>
        <comment>Settings|Show sidebar</comment>
        <translation>Ctrl+F9</translation>
    </message>
    <message>
        <location filename="qdjview.cpp" line="575"/>
        <source>Ctrl+I</source>
        <comment>Edit|Information</comment>
        <translation>Ctrl+I</translation>
    </message>
    <message>
        <location filename="qdjview.cpp" line="581"/>
        <source>Ctrl+M</source>
        <comment>Edit|Metadata</comment>
        <translation>Ctrl+M</translation>
    </message>
    <message>
        <location filename="qdjview.cpp" line="368"/>
        <source>Ctrl+N</source>
        <comment>File|New</comment>
        <translation>Ctrl+N</translation>
    </message>
    <message>
        <location filename="qdjview.cpp" line="374"/>
        <source>Ctrl+O</source>
        <comment>File|Open</comment>
        <translation>Ctrl+O</translation>
    </message>
    <message>
        <location filename="qdjview.cpp" line="409"/>
        <source>Ctrl+P</source>
        <comment>File|Print</comment>
        <translation>Ctrl+P</translation>
    </message>
    <message>
        <location filename="qdjview.cpp" line="391"/>
        <source>Ctrl+Q</source>
        <comment>File|Quit</comment>
        <translation>Ctrl+Q</translation>
    </message>
    <message>
        <location filename="qdjview.cpp" line="397"/>
        <source>Ctrl+S</source>
        <comment>File|SaveAs</comment>
        <translation>Ctrl+S</translation>
    </message>
    <message>
        <location filename="qdjview.cpp" line="385"/>
        <source>Ctrl+W</source>
        <comment>File|Close</comment>
        <translation>Ctrl+W</translation>
    </message>
    <message>
        <location filename="qdjview.cpp" line="3488"/>
        <source>Decoding DjVu document</source>
        <translation>Декодирование документа DjVu</translation>
    </message>
    <message>
        <location filename="qdjview.cpp" line="445"/>
        <source>Decrease the magnification.</source>
        <translation>Уменьшить масштаб.</translation>
    </message>
    <message>
        <location filename="qdjview.cpp" line="1875"/>
        <source>Deprecated option &apos;%1&apos;</source>
        <translation>Устаревшая опция &apos;%1&apos;</translation>
    </message>
    <message>
        <location filename="qdjview.cpp" line="770"/>
        <location filename="qdjview.cpp" line="835"/>
        <source>&amp;Display</source>
        <comment>View|Display</comment>
        <translation>По&amp;казывать</translation>
    </message>
    <message>
        <location filename="qdjview.cpp" line="596"/>
        <source>Display everything.</source>
        <translation>Показать все.</translation>
    </message>
    <message>
        <location filename="qdjview.cpp" line="2196"/>
        <location filename="qdjview.cpp" line="2463"/>
        <source>DjView</source>
        <translation>DjView</translation>
    </message>
    <message>
        <location filename="qdjview.cpp" line="4058"/>
        <source>DjVu files</source>
        <translation>Файлы DjVu</translation>
    </message>
    <message>
        <location filename="qdjview.cpp" line="738"/>
        <source>&amp;Edit</source>
        <comment>Edit|</comment>
        <translation>&amp;Правка</translation>
    </message>
    <message>
        <location filename="qdjview.cpp" line="4078"/>
        <source>Enter the URL of a DjVu document:</source>
        <translation>Введите URL документа DjVu:</translation>
    </message>
    <message>
        <location filename="qdjview.cpp" line="3202"/>
        <location filename="qdjview.cpp" line="3247"/>
        <location filename="qdjview.cpp" line="3267"/>
        <source>Error - DjView</source>
        <comment>dialog caption</comment>
        <translation>Ошибка - DjView</translation>
    </message>
    <message>
        <location filename="qdjview.cpp" line="402"/>
        <source>&amp;Export as...</source>
        <comment>File|</comment>
        <translation>&amp;Экспорт...</translation>
    </message>
    <message>
        <location filename="qdjview.cpp" line="2919"/>
        <source>Export - DjView</source>
        <comment>dialog caption</comment>
        <translation>Экспорт - DjView</translation>
    </message>
    <message>
        <location filename="qdjview.cpp" line="405"/>
        <source>Export DjVu page or document to other formats.</source>
        <translation>Экспортировать страницы или документ DjVu в другие форматы.</translation>
    </message>
    <message>
        <location filename="qdjview.cpp" line="642"/>
        <source>F10</source>
        <comment>Settings|Show toolbar</comment>
        <translation>F10</translation>
    </message>
    <message>
        <location filename="qdjview.cpp" line="654"/>
        <source>F11</source>
        <comment>View|FullScreen</comment>
        <translation>F11</translation>
    </message>
    <message>
        <location filename="qdjview.cpp" line="433"/>
        <source>F2</source>
        <comment>Edit|Select</comment>
        <translation>F2</translation>
    </message>
    <message>
        <location filename="qdjview.cpp" line="422"/>
        <source>F3</source>
        <comment>Edit|Find Next</comment>
        <translation>F3</translation>
    </message>
    <message>
        <location filename="qdjview.cpp" line="662"/>
        <source>F4</source>
        <comment>Layout|Continuous</comment>
        <translation>F4</translation>
    </message>
    <message>
        <location filename="qdjview.cpp" line="670"/>
        <source>F5</source>
        <comment>Layout|SideBySide</comment>
        <translation>F5</translation>
    </message>
    <message>
        <location filename="qdjview.cpp" line="635"/>
        <source>F9</source>
        <comment>Settings|Show sidebar</comment>
        <translation>F9</translation>
    </message>
    <message>
        <location filename="qdjview.cpp" line="718"/>
        <source>&amp;File</source>
        <comment>File|</comment>
        <translation>&amp;Файл</translation>
    </message>
    <message>
        <location filename="qdjview.cpp" line="414"/>
        <source>&amp;Find...</source>
        <comment>Edit|</comment>
        <translation>&amp;Найти...</translation>
    </message>
    <message>
        <location filename="qdjview.cpp" line="420"/>
        <source>Find &amp;Next</source>
        <comment>Edit|</comment>
        <translation>Найти &amp;далее</translation>
    </message>
    <message>
        <location filename="qdjview.cpp" line="423"/>
        <source>Find next occurence of search text in the document.</source>
        <translation>Найти следующее вхождение текста в документе.</translation>
    </message>
    <message>
        <location filename="qdjview.cpp" line="426"/>
        <source>Find &amp;Previous</source>
        <comment>Edit|</comment>
        <translation>Найти &amp;ранее</translation>
    </message>
    <message>
        <location filename="qdjview.cpp" line="428"/>
        <source>Find previous occurence of search text in the document.</source>
        <translation>Найти предыдущее вхождение текста в документе.</translation>
    </message>
    <message>
        <location filename="qdjview.cpp" line="417"/>
        <source>Find text in the document.</source>
        <translation>Найти текст в документе.</translation>
    </message>
    <message>
        <location filename="qdjview.cpp" line="502"/>
        <source>&amp;First Page</source>
        <comment>Go|</comment>
        <translation>&amp;Первая страница</translation>
    </message>
    <message>
        <location filename="qdjview.cpp" line="141"/>
        <source>FitPage</source>
        <comment>zoomCombo</comment>
        <translation>Страница целиком</translation>
    </message>
    <message>
        <location filename="qdjview.cpp" line="454"/>
        <source>Fit &amp;Page</source>
        <comment>Zoom|</comment>
        <translation>&amp;Страница целиком</translation>
    </message>
    <message>
        <location filename="qdjview.cpp" line="140"/>
        <source>FitWidth</source>
        <comment>zoomCombo</comment>
        <translation>По ширине страницы</translation>
    </message>
    <message>
        <location filename="qdjview.cpp" line="448"/>
        <source>Fit &amp;Width</source>
        <comment>Zoom|</comment>
        <translation>По &amp;ширине страницы</translation>
    </message>
    <message>
        <location filename="qdjview.cpp" line="159"/>
        <source>Foreground</source>
        <comment>modeCombo</comment>
        <translation>Передний план</translation>
    </message>
    <message>
        <location filename="qdjview.cpp" line="608"/>
        <source>&amp;Foreground</source>
        <comment>Display|</comment>
        <translation>&amp;Передний план</translation>
    </message>
    <message>
        <location filename="qdjview.cpp" line="532"/>
        <source>&amp;Forward</source>
        <comment>Go|</comment>
        <translation>&amp;Вперед</translation>
    </message>
    <message>
        <location filename="qdjview.cpp" line="534"/>
        <source>Forward in history.</source>
        <translation>Вперед по истории.</translation>
    </message>
    <message>
        <location filename="qdjview.cpp" line="652"/>
        <source>F&amp;ull Screen</source>
        <comment>View|</comment>
        <translation>На весь &amp;экран</translation>
    </message>
    <message>
        <location filename="qdjview.cpp" line="788"/>
        <location filename="qdjview.cpp" line="808"/>
        <source>&amp;Go</source>
        <comment>Go|</comment>
        <translation>&amp;Переход</translation>
    </message>
    <message>
        <location filename="qdjview.cpp" line="3708"/>
        <location filename="qdjview.cpp" line="3710"/>
        <source>Go: page %1.</source>
        <translation>Перейти к странице %1.</translation>
    </message>
    <message>
        <location filename="qdjview.cpp" line="802"/>
        <source>&amp;Help</source>
        <comment>Help|</comment>
        <translation>&amp;Справка</translation>
    </message>
    <message>
        <location filename="qdjview.cpp" line="1095"/>
        <source>&lt;html&gt;&lt;b&gt;Continuous layout.&lt;/b&gt;&lt;br/&gt; Display all the document pages arranged vertically inside the scrollable document viewing area.&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;b&gt;Непрерывный вид (рулоном).&lt;/b&gt;&lt;br/&gt; Показывает все страницы документа в вертикальном расположении в прокручиваемой области просмотра документа.&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="qdjview.cpp" line="1111"/>
        <source>&lt;html&gt;&lt;b&gt;Cursor information.&lt;/b&gt;&lt;br/&gt; Display the position of the mouse cursor expressed in page coordinates. &lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;b&gt;Информация курсора.&lt;/b&gt;&lt;br/&gt; Показывает позицию курсора мыши, выраженную в координатах страницы. &lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="qdjview.cpp" line="1067"/>
        <source>&lt;html&gt;&lt;b&gt;Display mode.&lt;/b&gt;&lt;br/&gt; DjVu images compose a background layer and a foreground layer using a stencil. The display mode specifies with layers should be displayed.&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;b&gt;Режим отображения.&lt;/b&gt;&lt;br/&gt; Изображения DjVu составляются из фона и основного слоя с помощью трафарета. Режим отображения задает слои, которые должны отображаться.&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="qdjview.cpp" line="1083"/>
        <source>&lt;html&gt;&lt;b&gt;Document and page information.&lt;/b&gt;&lt;br&gt; Display a dialog window for viewing encoding information pertaining to the document or to a specific page.&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;b&gt;Информация о документе и странице.&lt;/b&gt;&lt;br&gt; Отображает диалоговое окно для просмотра информации, относящейся к документу или к отдельной странице.&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="qdjview.cpp" line="1089"/>
        <source>&lt;html&gt;&lt;b&gt;Document and page metadata.&lt;/b&gt;&lt;br&gt; Display a dialog window for viewing metadata pertaining to the document or to a specific page.&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;b&gt;Метаданные документа и страниц.&lt;/b&gt;&lt;br&gt; Отображает диалоговое окно для просмотра метааднных, относящихся к документу или к отдельной странице.&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="qdjview.cpp" line="1129"/>
        <source>&lt;html&gt;&lt;b&gt;Document viewing area.&lt;/b&gt;&lt;br/&gt; This is the main display area for the DjVu document. But you must first open a DjVu document to see anything.&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;b&gt;Область просмотра документа.&lt;/b&gt;&lt;br/&gt; Это - основная область отображения документа DjVu. Но, чтобы что-то увидеть, Вы должны сначала открыть документ DjVu.&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="qdjview.cpp" line="1116"/>
        <source>&lt;html&gt;&lt;b&gt;Document viewing area.&lt;/b&gt;&lt;br/&gt; This is the main display area for the DjVu document. &lt;ul&gt;&lt;li&gt;Arrows and page keys to navigate the document.&lt;/li&gt;&lt;li&gt;Space and BackSpace to read the document.&lt;/li&gt;&lt;li&gt;Keys &lt;tt&gt;+&lt;/tt&gt; &lt;tt&gt;-&lt;/tt&gt; &lt;tt&gt;[&lt;/tt&gt; &lt;tt&gt;]&lt;/tt&gt; to zoom or rotate the document.&lt;/li&gt;&lt;li&gt;Left Mouse Button for panning and selecting links.&lt;/li&gt;&lt;li&gt;%3 for displaying the contextual menu.&lt;/li&gt;&lt;li&gt;%1 Left Mouse Button for selecting text or images.&lt;/li&gt;&lt;li&gt;%2 for popping the magnification lens.&lt;/li&gt;&lt;/ul&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;b&gt;Область просмотра документа.&lt;/b&gt;&lt;br/&gt; Это - основная область отображения документа DjVu. &lt;ul&gt;&lt;li&gt;Используйте стрелки и клавиши прокрутки страниц для перемещения по документу.&lt;/li&gt;&lt;li&gt;Пробел и BackSpace - чтение документа.&lt;/li&gt;&lt;li&gt;Клавиши &lt;tt&gt;+&lt;/tt&gt; &lt;tt&gt;-&lt;/tt&gt; &lt;tt&gt;[&lt;/tt&gt; &lt;tt&gt;]&lt;/tt&gt; - изменение масштаба и поворот документа.&lt;/li&gt;&lt;li&gt;Левая клавиша мыши - прокрутка и выбор ссылок.&lt;/li&gt;&lt;li&gt;%3 - отображение контекстного меню.&lt;/li&gt;&lt;li&gt;%1 + левая кнопка мыши - выделение текста и изображений.&lt;/li&gt;&lt;li&gt;%2 - показ лупы.&lt;/li&gt;&lt;/ul&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="qdjview.cpp" line="1075"/>
        <source>&lt;html&gt;&lt;b&gt;Navigating the document.&lt;/b&gt;&lt;br/&gt; The page selector lets you jump to any page by name. The navigation buttons jump to the first page, the previous page, the next page, or the last page. &lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;b&gt;Навигация по документу.&lt;/b&gt;&lt;br/&gt; Вы можете перейти к любой странице по её имени. Кнопки навигации позволяют перейти к первой, предыдущей, следующей или последней странице. &lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="qdjview.cpp" line="1105"/>
        <source>&lt;html&gt;&lt;b&gt;Page information.&lt;/b&gt;&lt;br/&gt; Display information about the page located under the cursor: the sequential page number, the page size in pixels, and the page resolution in dots per inch. &lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;b&gt;Информация о странице.&lt;/b&gt;&lt;br/&gt; Отображает информацию о странице, которая находится под курсором: порядковый номер страницы, размер в пикселах и разрешение в точках на дюйм. &lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="qdjview.cpp" line="1060"/>
        <source>&lt;html&gt;&lt;b&gt;Rotating the pages.&lt;/b&gt;&lt;br/&gt; Choose to display pages in portrait or landscape mode. You can also turn them upside down.&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;b&gt;Поворот страниц.&lt;/b&gt;&lt;br/&gt; Выберите показ страниц в книжной или альбомной ориентации. Вы также можете перевернуть их.&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="qdjview.cpp" line="1040"/>
        <source>&lt;html&gt;&lt;b&gt;Selecting a rectangle.&lt;/b&gt;&lt;br/&gt; Once a rectangular area is selected, a popup menu lets you copy the corresponding text or image. Instead of using this tool, you can also hold %1 and use the Left Mouse Button.&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;b&gt;Выделение прямоугольной области.&lt;/b&gt;&lt;br/&gt; После выделения Вы можете скопировать текст или изображение. Вместо этого инструмента Вы также можете использовать левую кнопку мыши, удерживая клавишу %1.&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="qdjview.cpp" line="1100"/>
        <source>&lt;html&gt;&lt;b&gt;Side by side layout.&lt;/b&gt;&lt;br/&gt; Display pairs of pages side by side inside the scrollable document viewing area.&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;b&gt;Показ страниц парами.&lt;/b&gt;&lt;br/&gt; Отображает пары страниц рядом в прокручиваемой области просмотра документа.&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="qdjview.cpp" line="1048"/>
        <source>&lt;html&gt;&lt;b&gt;Zooming.&lt;/b&gt;&lt;br/&gt; Choose a zoom level for viewing the document. Zoom level 100% displays the document for a 100 dpi screen. Zoom levels &lt;tt&gt;Fit Page&lt;/tt&gt; and &lt;tt&gt;Fit Width&lt;/tt&gt; ensure that the full page or the page width fit in the window. &lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;b&gt;Изменение масштаба.&lt;/b&gt;&lt;br/&gt; Выберите масштаб для просмотра документа. Масштаб 100% подходит для просмотра документа на экране 100 dpi (100 точек/дюйм). &lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="qdjview.cpp" line="4018"/>
        <source>&lt;html&gt;&lt;h2&gt;DjVuLibre DjView %1&lt;/h2&gt;%2&lt;p&gt;Viewer for DjVu documents&lt;br&gt;&lt;a href=http://djvulibre.djvuzone.org&gt;http://djvulibre.djvuzone.org&lt;/a&gt;&lt;br&gt;Copyright © 2006-- Léon Bottou.&lt;/p&gt;&lt;p align=justify&gt;&lt;small&gt;This program is free software. You can redistribute or modify it under the terms of the GNU General Public License as published by the Free Software Foundation. This program is distributed &lt;i&gt;without any warranty&lt;/i&gt;. See the GNU General Public License for more details.&lt;/small&gt;&lt;/p&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;h2&gt;DjVuLibre DjView %1&lt;/h2&gt;%2&lt;p&gt;Программа просмотра документов DjVu&lt;br&gt;&lt;a href=http://djvulibre.djvuzone.org&gt;http://djvulibre.djvuzone.org&lt;/a&gt;&lt;br&gt;Copyright © 2006-- Léon Bottou.&lt;/p&gt;&lt;p align=justify&gt;&lt;small&gt;Данная программа - свободное программное обеспечение. Вы можете распространять и/или модифицировать ее на условиях Стандартной Общественной Лицензии GNU (GNU General Public License), опубликованной Фондом Свободного ПО (Free Software Foundation). Данная прогдамма распространяется &lt;i&gt;без каких-либо гарантий&lt;/i&gt;. Подробности смотрите в Стандартной Общественной Лицензии GNU (GNU General Public License).&lt;/small&gt;&lt;/p&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="qdjview.cpp" line="1445"/>
        <source>Illegal value &apos;%2&apos; for option &apos;%1&apos;.</source>
        <translation>Недопустимое значение &apos;%2&apos; для опции &apos;%1&apos;.</translation>
    </message>
    <message>
        <location filename="qdjview.cpp" line="3261"/>
        <source>Image format %1 not supported.</source>
        <translation>Формат изображения %1 не поддерживается.</translation>
    </message>
    <message>
        <location filename="qdjview.cpp" line="440"/>
        <source>Increase the magnification.</source>
        <translation>Увеличить масштаб.</translation>
    </message>
    <message>
        <location filename="qdjview.cpp" line="574"/>
        <source>&amp;Information...</source>
        <comment>Edit|</comment>
        <translation>&amp;Информация...</translation>
    </message>
    <message>
        <location filename="qdjview.cpp" line="4095"/>
        <source>Information - DjView</source>
        <comment>dialog caption</comment>
        <translation>Информация - DjView</translation>
    </message>
    <message>
        <location filename="qdjview.cpp" line="3714"/>
        <source> (in other window.)</source>
        <translation> (в новом окне)</translation>
    </message>
    <message>
        <location filename="qdjview.cpp" line="504"/>
        <source>Jump to first document page.</source>
        <translation>Перейти к первой странице документа.</translation>
    </message>
    <message>
        <location filename="qdjview.cpp" line="522"/>
        <source>Jump to last document page.</source>
        <translation>Перейти к последней странице документа.</translation>
    </message>
    <message>
        <location filename="qdjview.cpp" line="510"/>
        <source>Jump to next document page.</source>
        <translation>Перейти к следующей странице документа.</translation>
    </message>
    <message>
        <location filename="qdjview.cpp" line="516"/>
        <source>Jump to previous document page.</source>
        <translation>Перейти к предыдущей странице документа.</translation>
    </message>
    <message>
        <location filename="qdjview.cpp" line="520"/>
        <source>&amp;Last Page</source>
        <comment>Go|</comment>
        <translation>&amp;Последняя страница</translation>
    </message>
    <message>
        <location filename="qdjview.cpp" line="3712"/>
        <source>Link: %1</source>
        <translation>Ссылка: %1</translation>
    </message>
    <message>
        <location filename="qdjview.cpp" line="485"/>
        <source>Magnify 100%</source>
        <translation>Увеличение 100%</translation>
    </message>
    <message>
        <location filename="qdjview.cpp" line="479"/>
        <source>Magnify 150%</source>
        <translation>Увеличение 150%</translation>
    </message>
    <message>
        <location filename="qdjview.cpp" line="473"/>
        <source>Magnify 20%</source>
        <translation>Увеличение 20%</translation>
    </message>
    <message>
        <location filename="qdjview.cpp" line="467"/>
        <source>Magnify 300%</source>
        <translation>Увеличение 300%</translation>
    </message>
    <message>
        <location filename="qdjview.cpp" line="497"/>
        <source>Magnify 50%</source>
        <translation>Увеличение 50%</translation>
    </message>
    <message>
        <location filename="qdjview.cpp" line="491"/>
        <source>Magnify 75%</source>
        <translation>Увеличение 75%</translation>
    </message>
    <message>
        <location filename="qdjview.cpp" line="579"/>
        <source>&amp;Metadata...</source>
        <comment>Edit|</comment>
        <translation>&amp;Метаданные...</translation>
    </message>
    <message>
        <location filename="qdjview.cpp" line="4110"/>
        <source>Metadata - DjView</source>
        <comment>dialog caption</comment>
        <translation>Метаданные - DjView</translation>
    </message>
    <message>
        <location filename="qdjview.cpp" line="367"/>
        <source>&amp;New</source>
        <comment>File|</comment>
        <translation>&amp;Новый</translation>
    </message>
    <message>
        <location filename="qdjview.cpp" line="508"/>
        <source>&amp;Next Page</source>
        <comment>Go|</comment>
        <translation>&amp;Следующая страница</translation>
    </message>
    <message>
        <location filename="qdjview.cpp" line="460"/>
        <source>One &amp;to one</source>
        <comment>Zoom|</comment>
        <translation>&amp;Один к одному</translation>
    </message>
    <message>
        <location filename="qdjview.cpp" line="616"/>
        <source>Only display the background layer.</source>
        <translation>Показывать только слой фона.</translation>
    </message>
    <message>
        <location filename="qdjview.cpp" line="602"/>
        <source>Only display the document bitonal stencil.</source>
        <translation>Показывать только двухцветный шаблон.</translation>
    </message>
    <message>
        <location filename="qdjview.cpp" line="609"/>
        <source>Only display the foreground layer.</source>
        <translation>Показывать только слой переднего плана.</translation>
    </message>
    <message>
        <location filename="qdjview.cpp" line="373"/>
        <source>&amp;Open</source>
        <comment>File|</comment>
        <translation>&amp;Открыть</translation>
    </message>
    <message>
        <location filename="qdjview.cpp" line="376"/>
        <source>Open a DjVu document.</source>
        <translation>Открыть документ DjVu.</translation>
    </message>
    <message>
        <location filename="qdjview.cpp" line="380"/>
        <source>Open a remote DjVu document.</source>
        <translation>Открыть удаленный документ DjVu.</translation>
    </message>
    <message>
        <location filename="qdjview.cpp" line="4060"/>
        <source>Open - DjView</source>
        <comment>dialog caption</comment>
        <translation>Открыть - DjView</translation>
    </message>
    <message>
        <location filename="qdjview.cpp" line="2523"/>
        <source>Opening DjVu document</source>
        <translation>Открытие документа DjVu</translation>
    </message>
    <message>
        <location filename="qdjview.cpp" line="2484"/>
        <source>Opening DjVu file</source>
        <translation>Открытие файла DjVu</translation>
    </message>
    <message>
        <location filename="qdjview.cpp" line="379"/>
        <source>Open &amp;Location...</source>
        <comment>File|</comment>
        <translation>Открыть &amp;адрес...</translation>
    </message>
    <message>
        <location filename="qdjview.cpp" line="4077"/>
        <source>Open Location - DjView</source>
        <comment>dialog caption</comment>
        <translation>Открыть адрес - DjView</translation>
    </message>
    <message>
        <location filename="qdjview.cpp" line="724"/>
        <source>Open &amp;Recent</source>
        <translation>Открыть &amp;недавние</translation>
    </message>
    <message>
        <location filename="qdjview.cpp" line="1921"/>
        <source>Option &apos;%1&apos; is not implemented.</source>
        <translation>Опция &apos;%1&apos; не реализована.</translation>
    </message>
    <message>
        <location filename="qdjview.cpp" line="1926"/>
        <source>Option &apos;%1&apos; is not recognized.</source>
        <translation>Опция &apos;%1&apos; не распознана.</translation>
    </message>
    <message>
        <location filename="qdjview.cpp" line="1622"/>
        <source>Option &apos;%1&apos; requires a standalone viewer.</source>
        <translation>Опция &apos;%1&apos; требует режима самостоятельного приложения.</translation>
    </message>
    <message>
        <location filename="qdjview.cpp" line="1438"/>
        <source>Option &apos;%1&apos; requires boolean argument.</source>
        <translation>Опция &apos;%1&apos; требует логического аргумента.</translation>
    </message>
    <message>
        <source> P%1 %2x%3 %4dpi </source>
        <translation type="obsolete"> P%1 %2x%3 %4dpi </translation>
    </message>
    <message>
        <location filename="qdjview.cpp" line="628"/>
        <source>Prefere&amp;nces...</source>
        <comment>Settings|</comment>
        <translation>&amp;Настройки...</translation>
    </message>
    <message>
        <location filename="qdjview.cpp" line="514"/>
        <source>&amp;Previous Page</source>
        <comment>Go|</comment>
        <translation>&amp;Предыдущая страница</translation>
    </message>
    <message>
        <location filename="qdjview.cpp" line="408"/>
        <source>&amp;Print...</source>
        <comment>File|</comment>
        <translation>&amp;Печать...</translation>
    </message>
    <message>
        <location filename="qdjview.cpp" line="2886"/>
        <source>Print - DjView</source>
        <comment>dialog caption</comment>
        <translation>Печать - DjView</translation>
    </message>
    <message>
        <location filename="qdjview.cpp" line="411"/>
        <source>Print the DjVu document.</source>
        <translation>Печать документа DjVu.</translation>
    </message>
    <message>
        <location filename="qdjview.cpp" line="390"/>
        <source>&amp;Quit</source>
        <comment>File|</comment>
        <translation>&amp;Выход</translation>
    </message>
    <message>
        <location filename="qdjview.cpp" line="1033"/>
        <source>Right Mouse Button</source>
        <translation>правую кнопку мыши</translation>
    </message>
    <message>
        <location filename="qdjview.cpp" line="762"/>
        <location filename="qdjview.cpp" line="827"/>
        <source>&amp;Rotate</source>
        <comment>View|Rotate</comment>
        <translation>По&amp;ворот</translation>
    </message>
    <message>
        <location filename="qdjview.cpp" line="550"/>
        <source>Rotate &amp;0°</source>
        <comment>Rotate|</comment>
        <translation>Поворот на &amp;0°</translation>
    </message>
    <message>
        <location filename="qdjview.cpp" line="562"/>
        <source>Rotate &amp;180°</source>
        <comment>Rotate|</comment>
        <translation>Поворот на &amp;180°</translation>
    </message>
    <message>
        <location filename="qdjview.cpp" line="568"/>
        <source>Rotate &amp;270°</source>
        <comment>Rotate|</comment>
        <translation>Поворот на &amp;270°</translation>
    </message>
    <message>
        <location filename="qdjview.cpp" line="556"/>
        <source>Rotate &amp;90°</source>
        <comment>Rotate|</comment>
        <translation>Поворот на &amp;90°</translation>
    </message>
    <message>
        <location filename="qdjview.cpp" line="538"/>
        <source>Rotate &amp;Left</source>
        <comment>Rotate|</comment>
        <translation>Повернуть против &amp;часовой стрелки</translation>
    </message>
    <message>
        <location filename="qdjview.cpp" line="546"/>
        <source>Rotate page image clockwise.</source>
        <translation>Повернуть страницу по часовой стрелке.</translation>
    </message>
    <message>
        <location filename="qdjview.cpp" line="540"/>
        <source>Rotate page image counter-clockwise.</source>
        <translation>Повернуть страницу против часовой стрелки.</translation>
    </message>
    <message>
        <location filename="qdjview.cpp" line="544"/>
        <source>Rotate &amp;Right</source>
        <comment>Rotate|</comment>
        <translation>Повернуть &amp;по часовой стрелке</translation>
    </message>
    <message>
        <location filename="qdjview.cpp" line="396"/>
        <source>Save &amp;as...</source>
        <comment>File|</comment>
        <translation>Сохранить &amp;как...</translation>
    </message>
    <message>
        <location filename="qdjview.cpp" line="2903"/>
        <source>Save - DjView</source>
        <comment>dialog caption</comment>
        <translation>Сохранить - DjView</translation>
    </message>
    <message>
        <location filename="qdjview.cpp" line="3847"/>
        <source>Save image as...</source>
        <translation>Сохранить изображение как...</translation>
    </message>
    <message>
        <location filename="qdjview.cpp" line="3233"/>
        <source>Save Image - DjView</source>
        <comment>dialog caption</comment>
        <translation>Сохранить изображение - DjView</translation>
    </message>
    <message>
        <location filename="qdjview.cpp" line="3839"/>
        <source>Save text as...</source>
        <translation>Сохранить текст как...</translation>
    </message>
    <message>
        <location filename="qdjview.cpp" line="3186"/>
        <source>Save Text - DjView</source>
        <comment>dialog caption</comment>
        <translation>Сохранить текст - DjView</translation>
    </message>
    <message>
        <location filename="qdjview.cpp" line="399"/>
        <source>Save the DjVu document.</source>
        <translation>Сохранить документ DjVU.</translation>
    </message>
    <message>
        <location filename="qdjview.cpp" line="431"/>
        <source>&amp;Select</source>
        <comment>Edit|</comment>
        <translation>&amp;Выделение</translation>
    </message>
    <message>
        <location filename="qdjview.cpp" line="435"/>
        <source>Select a rectangle in the document.</source>
        <translation>Прямоугольное выделение в документе.</translation>
    </message>
    <message>
        <location filename="qdjview.cpp" line="461"/>
        <source>Set full resolution magnification.</source>
        <translation>Установить масштаб по разрешению изображения.</translation>
    </message>
    <message>
        <location filename="qdjview.cpp" line="455"/>
        <source>Set magnification to fit page.</source>
        <translation>Установить масштаб по размеру страниц.</translation>
    </message>
    <message>
        <location filename="qdjview.cpp" line="449"/>
        <source>Set magnification to fit page width.</source>
        <translation>Установить масштаб по ширине страниц.</translation>
    </message>
    <message>
        <location filename="qdjview.cpp" line="551"/>
        <source>Set natural page orientation.</source>
        <translation>Установить исходную ориентацию страницы.</translation>
    </message>
    <message>
        <location filename="qdjview.cpp" line="796"/>
        <source>&amp;Settings</source>
        <comment>Settings|</comment>
        <translation>&amp;Настройки</translation>
    </message>
    <message>
        <location filename="qdjview.cpp" line="427"/>
        <source>Shift+F3</source>
        <comment>Edit|Find Previous</comment>
        <translation>Shift+F3</translation>
    </message>
    <message>
        <location filename="qdjview.cpp" line="636"/>
        <source>Show/hide the side bar.</source>
        <translation>Показать/скрыть боковую панель.</translation>
    </message>
    <message>
        <location filename="qdjview.cpp" line="643"/>
        <source>Show/hide the standard tool bar.</source>
        <translation>Показать/скрыть стандартную панель инструментов.</translation>
    </message>
    <message>
        <location filename="qdjview.cpp" line="647"/>
        <source>Show/hide the status bar.</source>
        <translation>Показать/скрыть строку состояния.</translation>
    </message>
    <message>
        <location filename="qdjview.cpp" line="576"/>
        <source>Show information about the document encoding and structure.</source>
        <translation>Показать информацию о структуре и кодировании документа.</translation>
    </message>
    <message>
        <location filename="qdjview.cpp" line="592"/>
        <source>Show information about this program.</source>
        <translation>Показать информацию о программе.</translation>
    </message>
    <message>
        <location filename="qdjview.cpp" line="633"/>
        <source>Show &amp;Sidebar</source>
        <comment>Settings|</comment>
        <translation>Показать &amp;боковую панель</translation>
    </message>
    <message>
        <location filename="qdjview.cpp" line="646"/>
        <source>Show Stat&amp;usbar</source>
        <comment>Settings|</comment>
        <translation>Показать строку &amp;состояния</translation>
    </message>
    <message>
        <location filename="qdjview.cpp" line="583"/>
        <source>Show the document and page meta data.</source>
        <translation>Показать метаданные документа и страниц.</translation>
    </message>
    <message>
        <location filename="qdjview.cpp" line="630"/>
        <source>Show the preferences dialog.</source>
        <translation>Показать диалог настроек.</translation>
    </message>
    <message>
        <location filename="qdjview.cpp" line="640"/>
        <source>Show &amp;Toolbar</source>
        <comment>Settings|</comment>
        <translation>Показать &amp;панель инструментов</translation>
    </message>
    <message>
        <location filename="qdjview.cpp" line="667"/>
        <source>Side &amp;by Side</source>
        <comment>Layout|</comment>
        <translation>&amp;Пары страниц</translation>
    </message>
    <message>
        <location filename="qdjview.cpp" line="601"/>
        <source>&amp;Stencil</source>
        <comment>Display|</comment>
        <translation>&amp;Шаблон</translation>
    </message>
    <message>
        <location filename="qdjview.cpp" line="158"/>
        <source>Stencil</source>
        <comment>modeCombo</comment>
        <translation>Шаблон</translation>
    </message>
    <message>
        <location filename="qdjview.cpp" line="142"/>
        <source>Stretch</source>
        <comment>zoomCombo</comment>
        <translation>Растянуть</translation>
    </message>
    <message>
        <location filename="qdjview.cpp" line="3184"/>
        <source>Text files</source>
        <comment>save filter</comment>
        <translation>Текстовые файлы</translation>
    </message>
    <message>
        <location filename="qdjview.cpp" line="663"/>
        <source>Toggle continuous layout mode.</source>
        <translation>Показывать страницы непрерывным полотном.</translation>
    </message>
    <message>
        <location filename="qdjview.cpp" line="656"/>
        <source>Toggle full screen mode.</source>
        <translation>Полноэкранный режим.</translation>
    </message>
    <message>
        <location filename="qdjview.cpp" line="671"/>
        <source>Toggle side-by-side layout mode.</source>
        <translation>Показывать страницы парами.</translation>
    </message>
    <message>
        <location filename="qdjview.cpp" line="1563"/>
        <source>Toolbar option &apos;%1&apos; is not implemented.</source>
        <translation>Опция панели инструментов &apos;%1&apos; не реализована.</translation>
    </message>
    <message>
        <location filename="qdjview.cpp" line="1576"/>
        <source>Toolbar option &apos;%1&apos; is not recognized.</source>
        <translation>Опция панели инструментов &apos;%1&apos; не распознана.</translation>
    </message>
    <message>
        <location filename="qdjview.cpp" line="557"/>
        <source>Turn page on its left side.</source>
        <translation>Поворот налево.</translation>
    </message>
    <message>
        <location filename="qdjview.cpp" line="569"/>
        <source>Turn page on its right side.</source>
        <translation>Поворот направо.</translation>
    </message>
    <message>
        <location filename="qdjview.cpp" line="563"/>
        <source>Turn page upside-down.</source>
        <translation>Перевернуть страницу.</translation>
    </message>
    <message>
        <location filename="qdjview.cpp" line="2825"/>
        <source>Unrecognized sidebar options &apos;%1&apos;.</source>
        <translation>Опция боковой панели &apos;%1&apos; не распознана.</translation>
    </message>
    <message>
        <location filename="qdjview.cpp" line="747"/>
        <source>&amp;View</source>
        <comment>View|</comment>
        <translation>&amp;Вид</translation>
    </message>
    <message>
        <location filename="qdjview.cpp" line="3625"/>
        <source> x=%1 y=%2 </source>
        <translation> x=%1 y=%2 </translation>
    </message>
    <message>
        <location filename="qdjview.cpp" line="748"/>
        <location filename="qdjview.cpp" line="813"/>
        <source>&amp;Zoom</source>
        <comment>View|Zoom</comment>
        <translation>&amp;Масштаб</translation>
    </message>
    <message>
        <location filename="qdjview.cpp" line="438"/>
        <source>Zoom &amp;In</source>
        <comment>Zoom|</comment>
        <translation>&amp;Увеличить</translation>
    </message>
    <message>
        <location filename="qdjview.cpp" line="443"/>
        <source>Zoom &amp;Out</source>
        <comment>Zoom|</comment>
        <translation>У&amp;меньшить</translation>
    </message>
    <message>
        <location filename="qdjview.cpp" line="3851"/>
        <source>Zoom to rectangle</source>
        <translation>Увеличить по выделению</translation>
    </message>
    <message>
        <location filename="qdjview.cpp" line="3701"/>
        <source>Go: 1 page forward.</source>
        <translation>Перейти на 1 страницу вперед.</translation>
    </message>
    <message numerus="yes">
        <location filename="qdjview.cpp" line="3702"/>
        <source>Go: %n pages forward.</source>
        <translation>
            <numerusform>Перейти на %n страницу вперед.</numerusform>
            <numerusform>Перейти на %n страницы вперед.</numerusform>
            <numerusform>Перейти на %n страниц вперед.</numerusform>
        </translation>
    </message>
    <message>
        <location filename="qdjview.cpp" line="3704"/>
        <source>Go: 1 page backward.</source>
        <translation>Перейти на 1 страницу назад.</translation>
    </message>
    <message numerus="yes">
        <location filename="qdjview.cpp" line="3705"/>
        <source>Go: %n pages backward.</source>
        <translation>
            <numerusform>Перейти на %n страницу назад.</numerusform>
            <numerusform>Перейти на %n страницы назад.</numerusform>
            <numerusform>Перейти на %n страниц назад.</numerusform>
        </translation>
    </message>
    <message numerus="yes">
        <location filename="qdjview.cpp" line="3832"/>
        <source>%n characters</source>
        <translation>
            <numerusform>%n символ</numerusform>
            <numerusform>%n символа</numerusform>
            <numerusform>%n символов</numerusform>
        </translation>
    </message>
    <message>
        <location filename="qdjview.cpp" line="695"/>
        <source>Copy &amp;URL</source>
        <comment>Edit|</comment>
        <translation>Скопировать &amp;URL</translation>
    </message>
    <message>
        <location filename="qdjview.cpp" line="161"/>
        <source>Hidden Text</source>
        <comment>modeCombo</comment>
        <translation>Скрытый текст</translation>
    </message>
    <message>
        <location filename="qdjview.cpp" line="696"/>
        <source>Save an URL for the current page into the clipboard.</source>
        <translation>Сохранить URL текущей страницы в буфер обмена.</translation>
    </message>
    <message>
        <location filename="qdjview.cpp" line="697"/>
        <source>Ctrl+C</source>
        <comment>Edit|CopyURL</comment>
        <translation>Ctrl+C</translation>
    </message>
    <message>
        <location filename="qdjview.cpp" line="700"/>
        <source>Copy &amp;Outline</source>
        <comment>Edit|</comment>
        <translation>Скопировать список &amp;глав</translation>
    </message>
    <message>
        <location filename="qdjview.cpp" line="701"/>
        <source>Save the djvused code for the outline into the clipboard.</source>
        <translation>Сохранить в буфер обмена список глав, определенный утилитой djvused.</translation>
    </message>
    <message>
        <location filename="qdjview.cpp" line="704"/>
        <source>Copy &amp;Annotations</source>
        <comment>Edit|</comment>
        <translation>Скопировать &amp;примечания</translation>
    </message>
    <message>
        <location filename="qdjview.cpp" line="705"/>
        <source>Save the djvused code for the page annotations into the clipboard.</source>
        <translation>Сохранить в буфер обмена примечания, извлеченные из документа утилитой djvused.</translation>
    </message>
    <message>
        <location filename="qdjview.cpp" line="2358"/>
        <source>Thumbnails</source>
        <translation>Макеты страниц</translation>
    </message>
    <message>
        <location filename="qdjview.cpp" line="2365"/>
        <source>Outline</source>
        <translation>Главы</translation>
    </message>
    <message>
        <location filename="qdjview.cpp" line="2372"/>
        <source>Find</source>
        <translation>Поиск</translation>
    </message>
    <message>
        <location filename="qdjview.cpp" line="3618"/>
        <source> P%1/%2 %3x%4 %5dpi </source>
        <translation> P%1/%2 %3x%4 %5dpi </translation>
    </message>
    <message>
        <location filename="qdjview.cpp" line="3842"/>
        <source>Copy text into the clipboard.</source>
        <translation>Сохранить текст в буфер обмена.</translation>
    </message>
    <message>
        <location filename="qdjview.cpp" line="3843"/>
        <source>Save text into a file.</source>
        <translation>Сохранить текст в файл.</translation>
    </message>
    <message>
        <location filename="qdjview.cpp" line="3849"/>
        <source>Save image into a file.</source>
        <translation>Сохранить изображение в файл.</translation>
    </message>
    <message>
        <location filename="qdjview.cpp" line="3852"/>
        <source>Zoom the selection to fit the window.</source>
        <translation>Увеличить выделенную область до размеров окна.</translation>
    </message>
    <message>
        <location filename="qdjview.cpp" line="3858"/>
        <source>Copy URL</source>
        <translation>Скопировать URL</translation>
    </message>
    <message>
        <location filename="qdjview.cpp" line="3859"/>
        <source>Save into the clipboard an URL that highlights the selection.</source>
        <translation>Сохранить в буфер обмена URL с информацией о выделении.</translation>
    </message>
    <message>
        <location filename="qdjview.cpp" line="3861"/>
        <source>Copy Maparea</source>
        <translation>Скопировать maparea</translation>
    </message>
    <message>
        <location filename="qdjview.cpp" line="3862"/>
        <source>Save into the clipboard a maparea annotation expression for program djvused.</source>
        <translation>Сохранить в буфер обмена директиву примечаний maparea, соответствующую выделению, полученную c помощью утилиты djvused.</translation>
    </message>
    <message>
        <location filename="qdjview.cpp" line="3848"/>
        <source>Copy image into the clipboard.</source>
        <translation>Скопировать изображение в буфер обмена.</translation>
    </message>
    <message>
        <location filename="qdjview.cpp" line="675"/>
        <source>Co&amp;ver Page</source>
        <comment>Layout|</comment>
        <translation>&amp;Титульный лист</translation>
    </message>
    <message>
        <location filename="qdjview.cpp" line="679"/>
        <source>Ctrl+F6</source>
        <comment>Layout|CoverPage</comment>
        <translation>Ctrl+F6</translation>
    </message>
    <message>
        <location filename="qdjview.cpp" line="680"/>
        <source>F6</source>
        <comment>Layout|CoverPage</comment>
        <translation>F6</translation>
    </message>
    <message>
        <location filename="qdjview.cpp" line="681"/>
        <source>Show the cover page alone in side-by-side mode.</source>
        <translation>Показывать первую страницу отдельно в режиме пар страниц.</translation>
    </message>
    <message>
        <location filename="qdjview.cpp" line="685"/>
        <source>&amp;Right to Left</source>
        <comment>Layout|</comment>
        <translation>&amp;Справа налево</translation>
    </message>
    <message>
        <location filename="qdjview.cpp" line="689"/>
        <source>Ctrl+Shift+F6</source>
        <comment>Layout|RightToLeft</comment>
        <translation>Ctrl+Shift+F6</translation>
    </message>
    <message>
        <location filename="qdjview.cpp" line="690"/>
        <source>Shift+F6</source>
        <comment>Layout|RightToLeft</comment>
        <translation>Shift+F6</translation>
    </message>
    <message>
        <location filename="qdjview.cpp" line="691"/>
        <source>Show pages right-to-left in side-by-side mode.</source>
        <translation>Показывать страницы справа налево в режиме пар страниц.</translation>
    </message>
    <message>
        <location filename="qdjview.cpp" line="4059"/>
        <source>All files</source>
        <translation>Все файлы</translation>
    </message>
    <message>
        <location filename="qdjview.cpp" line="622"/>
        <source>&amp;Hidden Text</source>
        <comment>Display|</comment>
        <translation>&amp;Скрытый текст</translation>
    </message>
    <message>
        <location filename="qdjview.cpp" line="623"/>
        <source>Overlay a representation of the hidden text layer.</source>
        <translation>Показывать также слой скрытого текста.</translation>
    </message>
</context>
<context>
    <name>QDjViewApplication</name>
    <message>
        <location filename="djview.cpp" line="238"/>
        <source>cannot open &apos;%1&apos;.</source>
        <translation>не удается открыть &apos;%1&apos;.</translation>
    </message>
    <message>
        <location filename="djview.cpp" line="304"/>
        <source>Usage: djview [options] [filename-or-url]
Common options include:
-help~~~Prints this message.
-verbose~~~Prints all warning messages.
-display &lt;xdpy&gt;~~~Select the X11 display &lt;xdpy&gt;.
-geometry &lt;xgeom&gt;~~~Select the initial window geometry.
-font &lt;xlfd&gt;~~~Select the X11 name of the main font.
-style &lt;qtstyle&gt;~~~Select the QT user interface style.
-fullscreen, -fs~~~Start djview in full screen mode.
-page=&lt;page&gt;~~~Jump to page &lt;page&gt;.
-zoom=&lt;zoom&gt;~~~Set zoom factor.
-continuous=&lt;yn&gt;~~~Set continuous layout.
-sidebyside=&lt;yn&gt;~~~Set side-by-side layout.
</source>
        <translation>Использование: djview [опции] [имя файла или URL]
Основные опции:
-help~~~Выводит это сообщение.
-verbose~~~Выводить все предупреждения.
-display &lt;xdpy&gt;~~~Выбор дисплея X11.
-geometry &lt;xgeom&gt;~~~Выбор исходной геометрии окна.
-font &lt;xlfd&gt;~~~Выбор имени X11 основного шрифта.
-style &lt;qtstyle&gt;~~~Выбор стиля пользовательского интерфейса QT.
-fullscreen, -fs~~~Запустить в полноэкранном режиме.
-page=&lt;page&gt;~~~Перейти к странице &lt;page&gt;.
-zoom=&lt;zoom&gt;~~~Задать масштаб.
-continuous=&lt;yn&gt;~~~Установить непрерывный просмотр.
-sidebyside=&lt;yn&gt;~~~Показывать пары страниц.
</translation>
    </message>
</context>
<context>
    <name>QDjViewDjVuExporter</name>
    <message>
        <location filename="qdjviewexporters.cpp" line="391"/>
        <source>&amp;Cancel</source>
        <translation>&amp;Отмена</translation>
    </message>
    <message>
        <location filename="qdjviewexporters.cpp" line="390"/>
        <source>Con&amp;tinue</source>
        <translation>&amp;Продолжить</translation>
    </message>
    <message>
        <location filename="qdjviewexporters.cpp" line="327"/>
        <source>DjVu Bundled Document</source>
        <translation>Связанный документ DjVu (в одном файле)</translation>
    </message>
    <message>
        <location filename="qdjviewexporters.cpp" line="328"/>
        <location filename="qdjviewexporters.cpp" line="332"/>
        <source>DjVu Files (*.djvu *.djv)</source>
        <translation>Файлы DjVu (*.djvu *.djv)</translation>
    </message>
    <message>
        <location filename="qdjviewexporters.cpp" line="331"/>
        <source>DjVu Indirect Document</source>
        <translation>Непрямой документ DjVu (страницы в отдельных файлах)</translation>
    </message>
    <message>
        <location filename="qdjviewexporters.cpp" line="384"/>
        <source>&lt;html&gt; This file belongs to a non empty directory. Saving an indirect document creates many files in this directory. Do you want to continue and risk overwriting files in this directory?&lt;/html&gt;</source>
        <translation>&lt;html&gt; Этот файл находится в непустой директории. При сохранении в непрямом формате будет создано множество файлов в этой директории. Вы рискуете заменить файлы в данной директории. Хотите продолжить?&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="qdjviewexporters.cpp" line="383"/>
        <source>Question - DjView</source>
        <comment>dialog caption</comment>
        <translation>Вопрос - DjView</translation>
    </message>
    <message>
        <location filename="qdjviewexporters.cpp" line="435"/>
        <source>Save job creation failed!</source>
        <translation>Не удалось запустить процесс сохранения!</translation>
    </message>
    <message>
        <location filename="qdjviewexporters.cpp" line="420"/>
        <source>System error: %1.</source>
        <translation>Системная ошибка: %1.</translation>
    </message>
    <message>
        <location filename="qdjviewexporters.cpp" line="418"/>
        <source>Unknown error.</source>
        <translation>Неизвестная ошибка.</translation>
    </message>
</context>
<context>
    <name>QDjViewErrorDialog</name>
    <message>
        <location filename="qdjviewerrordialog.ui" line="16"/>
        <location filename="qdjviewdialogs.cpp" line="125"/>
        <source>DjView Error</source>
        <translation>Ошибка DjView</translation>
    </message>
    <message>
        <location filename="qdjviewdialogs.cpp" line="162"/>
        <source>Error - DjView</source>
        <comment>dialog caption</comment>
        <translation>Ошибка - DjView</translation>
    </message>
    <message>
        <location filename="qdjviewerrordialog.ui" line="110"/>
        <source>&amp;Ok</source>
        <translation>&amp;OK</translation>
    </message>
</context>
<context>
    <name>QDjViewExportDialog</name>
    <message>
        <location filename="qdjviewdialogs.cpp" line="1330"/>
        <source>A file with this name already exists.
Do you want to replace it?</source>
        <translation>Файл с таким именем уже существует.
Заменить его?</translation>
    </message>
    <message>
        <location filename="qdjviewdialogs.cpp" line="1403"/>
        <source>All files</source>
        <comment>save filter</comment>
        <translation>Все файлы</translation>
    </message>
    <message>
        <location filename="qdjviewexportdialog.ui" line="256"/>
        <location filename="qdjviewdialogs.cpp" line="1333"/>
        <source>&amp;Cancel</source>
        <translation>&amp;Отмена</translation>
    </message>
    <message>
        <location filename="qdjviewexportdialog.ui" line="60"/>
        <source>C&amp;urrent page</source>
        <translation>&amp;Текущую страницу</translation>
    </message>
    <message>
        <location filename="qdjviewexportdialog.ui" line="214"/>
        <source>&amp;Defaults</source>
        <translation>&amp;По умолчанию</translation>
    </message>
    <message>
        <location filename="qdjviewexportdialog.ui" line="119"/>
        <source>Destination</source>
        <translation>Результат</translation>
    </message>
    <message>
        <location filename="qdjviewexportdialog.ui" line="13"/>
        <source>Dialog</source>
        <translation>Диалог</translation>
    </message>
    <message>
        <location filename="qdjviewexportdialog.ui" line="50"/>
        <source>&amp;Document</source>
        <translation>&amp;Документ</translation>
    </message>
    <message>
        <location filename="qdjviewdialogs.cpp" line="1323"/>
        <source>Error - DjView</source>
        <comment>dialog caption</comment>
        <translation>Ошибка - DjView</translation>
    </message>
    <message>
        <location filename="qdjviewexportdialog.ui" line="26"/>
        <location filename="qdjviewexportdialog.ui" line="38"/>
        <source>Export</source>
        <translation>Экспортировать</translation>
    </message>
    <message>
        <location filename="qdjviewdialogs.cpp" line="1410"/>
        <source>Export - DjView</source>
        <comment>dialog caption</comment>
        <translation>Экспорт - DjView</translation>
    </message>
    <message>
        <location filename="qdjviewexportdialog.ui" line="139"/>
        <source>Format:</source>
        <translation>Формат:</translation>
    </message>
    <message>
        <location filename="qdjviewdialogs.cpp" line="1170"/>
        <source>&lt;html&gt;&lt;b&gt;Saving.&lt;/b&gt;&lt;br/&gt; You can save the whole document or a page range under a variety of formats. Selecting certain formats creates additional dialog pages for specifying format options.&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;b&gt;Сохранение.&lt;/b&gt;&lt;br/&gt; Вы можете сохранить весь документ или диапазон страниц в различных форматах. После выбора определенного формата появляются дополнительные вкладки для настройки формата.&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="qdjviewexportdialog.ui" line="240"/>
        <source>&amp;Ok</source>
        <translation>&amp;OK</translation>
    </message>
    <message>
        <location filename="qdjviewdialogs.cpp" line="1324"/>
        <source>Overwriting the current file is not allowed!</source>
        <translation>Замена текущего файла не позволяется!</translation>
    </message>
    <message>
        <location filename="qdjviewexportdialog.ui" line="75"/>
        <source>&amp;Pages</source>
        <translation>&amp;Страницы от</translation>
    </message>
    <message>
        <location filename="qdjviewdialogs.cpp" line="1329"/>
        <source>Question - DjView</source>
        <comment>dialog caption</comment>
        <translation>Вопрос - DjView</translation>
    </message>
    <message>
        <location filename="qdjviewdialogs.cpp" line="1332"/>
        <source>&amp;Replace</source>
        <translation>&amp;Заменить</translation>
    </message>
    <message>
        <location filename="qdjviewexportdialog.ui" line="283"/>
        <source>Stop</source>
        <translation>Остановить</translation>
    </message>
    <message>
        <location filename="qdjviewdialogs.cpp" line="1374"/>
        <source>This operation has been interrupted.</source>
        <translation>Операция была прервана.</translation>
    </message>
    <message>
        <location filename="qdjviewdialogs.cpp" line="1370"/>
        <source>This operation has failed.</source>
        <translation>Не удалось выполнить операцию.</translation>
    </message>
    <message>
        <location filename="qdjviewexportdialog.ui" line="85"/>
        <source>to</source>
        <translation>до</translation>
    </message>
</context>
<context>
    <name>QDjViewExportPS1</name>
    <message>
        <location filename="qdjviewexportps1.ui" line="48"/>
        <source>&amp;Color</source>
        <translation>&amp;Цветной</translation>
    </message>
    <message>
        <location filename="qdjviewexportps1.ui" line="36"/>
        <source>Color</source>
        <translation>Цвет</translation>
    </message>
    <message>
        <location filename="qdjviewexportps1.ui" line="16"/>
        <source>Form</source>
        <translation>Форма</translation>
    </message>
    <message>
        <location filename="qdjviewexportps1.ui" line="58"/>
        <source>&amp;GrayScale</source>
        <translation>&amp;Оттенки серого</translation>
    </message>
    <message>
        <location filename="qdjviewexportps1.ui" line="111"/>
        <source>Language Level</source>
        <translation>Уровень языка PostScript</translation>
    </message>
    <message>
        <location filename="qdjviewexportps1.ui" line="68"/>
        <source>Marks</source>
        <translation>Разметка</translation>
    </message>
    <message>
        <location filename="qdjviewexportps1.ui" line="99"/>
        <source>PostScript</source>
        <translation>Cтандарт PostScript</translation>
    </message>
    <message>
        <location filename="qdjviewexportps1.ui" line="87"/>
        <source>Print crop &amp;marks</source>
        <translation>Печатать &amp;разметку страницы</translation>
    </message>
    <message>
        <location filename="qdjviewexportps1.ui" line="80"/>
        <source>Print image &amp;frame</source>
        <translation>Печатать &amp;рамку изображения</translation>
    </message>
</context>
<context>
    <name>QDjViewExportPS2</name>
    <message>
        <location filename="qdjviewexportps2.ui" line="76"/>
        <source> %</source>
        <translation> %</translation>
    </message>
    <message>
        <location filename="qdjviewexportps2.ui" line="112"/>
        <source>Automatic</source>
        <translation>Автоматически</translation>
    </message>
    <message>
        <location filename="qdjviewexportps2.ui" line="16"/>
        <source>Form</source>
        <translation>Форма</translation>
    </message>
    <message>
        <location filename="qdjviewexportps2.ui" line="129"/>
        <source>&amp;Landscape</source>
        <translation>&amp;Альбомная</translation>
    </message>
    <message>
        <location filename="qdjviewexportps2.ui" line="100"/>
        <source>Orientation</source>
        <translation>Ориентация</translation>
    </message>
    <message>
        <location filename="qdjviewexportps2.ui" line="122"/>
        <source>&amp;Portrait</source>
        <translation>&amp;Книжная</translation>
    </message>
    <message>
        <location filename="qdjviewexportps2.ui" line="48"/>
        <source>Scale to &amp;fit the page</source>
        <translation>Масштабировать по &amp;размеру страницы</translation>
    </message>
    <message>
        <location filename="qdjviewexportps2.ui" line="36"/>
        <source>Scaling</source>
        <translation>Масштабирование</translation>
    </message>
    <message>
        <location filename="qdjviewexportps2.ui" line="66"/>
        <source>&amp;Zoom</source>
        <translation>&amp;Масштаб</translation>
    </message>
</context>
<context>
    <name>QDjViewExportPS3</name>
    <message>
        <location filename="qdjviewexportps3.ui" line="223"/>
        <source>/100</source>
        <translation>/100</translation>
    </message>
    <message>
        <location filename="qdjviewexportps3.ui" line="35"/>
        <source>Advanced</source>
        <translation>Продвинутые настройки</translation>
    </message>
    <message>
        <location filename="qdjviewexportps3.ui" line="65"/>
        <source>at most </source>
        <translation>не более </translation>
    </message>
    <message>
        <location filename="qdjviewexportps3.ui" line="196"/>
        <source>Center:</source>
        <translation>Центральное поле:</translation>
    </message>
    <message>
        <location filename="qdjviewexportps3.ui" line="16"/>
        <source>Form</source>
        <translation>Форма</translation>
    </message>
    <message>
        <location filename="qdjviewexportps3.ui" line="239"/>
        <source>per page.</source>
        <translation>на страницу.</translation>
    </message>
    <message>
        <location filename="qdjviewexportps3.ui" line="216"/>
        <source>plus</source>
        <translation>плюс</translation>
    </message>
    <message>
        <location filename="qdjviewexportps3.ui" line="203"/>
        <source> points</source>
        <translation> точек</translation>
    </message>
    <message>
        <location filename="qdjviewexportps3.ui" line="160"/>
        <source> points.</source>
        <translation> точек.</translation>
    </message>
    <message>
        <location filename="qdjviewexportps3.ui" line="98"/>
        <source>Print </source>
        <translation>Печатать </translation>
    </message>
    <message>
        <location filename="qdjviewexportps3.ui" line="28"/>
        <source>Print sheets suitable for folding a booklet.</source>
        <translation>Печать листов для составления буклета.</translation>
    </message>
    <message>
        <location filename="qdjviewexportps3.ui" line="112"/>
        <source>rectos and versos.</source>
        <translation>четные и нечетные листы.</translation>
    </message>
    <message>
        <location filename="qdjviewexportps3.ui" line="117"/>
        <source>rectos only.</source>
        <translation>только нечетные листы.</translation>
    </message>
    <message>
        <location filename="qdjviewexportps3.ui" line="55"/>
        <source>Sheets per booklet: </source>
        <translation>Страниц на буклет: </translation>
    </message>
    <message>
        <location filename="qdjviewexportps3.ui" line="153"/>
        <source>Shift rectos and versos by </source>
        <translation>Смещать четные листы от нечетных листов на </translation>
    </message>
    <message>
        <location filename="qdjviewexportps3.ui" line="62"/>
        <source>Unlimited</source>
        <translation>Без ограничений</translation>
    </message>
    <message>
        <location filename="qdjviewexportps3.ui" line="122"/>
        <source>versos only.</source>
        <translation>только четные листы.</translation>
    </message>
</context>
<context>
    <name>QDjViewExportPrn</name>
    <message>
        <location filename="qdjviewexportprn.ui" line="117"/>
        <source> %</source>
        <translation> %</translation>
    </message>
    <message>
        <location filename="qdjviewexportprn.ui" line="150"/>
        <source>Automatic</source>
        <translation>Автоматически</translation>
    </message>
    <message>
        <location filename="qdjviewexportprn.ui" line="21"/>
        <source>Color</source>
        <translation>Цвет</translation>
    </message>
    <message>
        <location filename="qdjviewexportprn.ui" line="30"/>
        <source>&amp;Color</source>
        <translation>&amp;Цвет</translation>
    </message>
    <message>
        <location filename="qdjviewexportprn.ui" line="13"/>
        <source>Form</source>
        <translation>Форма</translation>
    </message>
    <message>
        <location filename="qdjviewexportprn.ui" line="40"/>
        <source>&amp;GrayScale</source>
        <translation>&amp;Оттенки серого</translation>
    </message>
    <message>
        <location filename="qdjviewexportprn.ui" line="173"/>
        <source>&amp;Landscape</source>
        <translation>&amp;Альбомная</translation>
    </message>
    <message>
        <location filename="qdjviewexportprn.ui" line="50"/>
        <source>Marks</source>
        <translation>Разметка</translation>
    </message>
    <message>
        <location filename="qdjviewexportprn.ui" line="141"/>
        <source>Orientation</source>
        <translation>Ориентация</translation>
    </message>
    <message>
        <location filename="qdjviewexportprn.ui" line="163"/>
        <source>&amp;Portrait</source>
        <translation>&amp;Книжная</translation>
    </message>
    <message>
        <location filename="qdjviewexportprn.ui" line="69"/>
        <source>Print crop &amp;marks</source>
        <translation>Печатать &amp;разметку страницы</translation>
    </message>
    <message>
        <location filename="qdjviewexportprn.ui" line="59"/>
        <source>Print image &amp;frame</source>
        <translation>Печатать &amp;рамку изображения</translation>
    </message>
    <message>
        <location filename="qdjviewexportprn.ui" line="89"/>
        <source>Scale to &amp;fit the page</source>
        <translation>Масштабировать по &amp;размеру страницы</translation>
    </message>
    <message>
        <location filename="qdjviewexportprn.ui" line="83"/>
        <source>Scaling</source>
        <translation>Масштабирование</translation>
    </message>
    <message>
        <location filename="qdjviewexportprn.ui" line="104"/>
        <source>&amp;Zoom</source>
        <translation>&amp;Масштаб</translation>
    </message>
</context>
<context>
    <name>QDjViewExportTiff</name>
    <message>
        <location filename="qdjviewexporttiff.ui" line="167"/>
        <source>Allow &amp;deflate compression.</source>
        <translation>Позволять сжатие &amp;без потерь deflate.</translation>
    </message>
    <message>
        <location filename="qdjviewexporttiff.ui" line="95"/>
        <source>Allow &amp;lossy JPEG compression.</source>
        <translation>Позволять сжатие JPEG с &amp;потерями.</translation>
    </message>
    <message>
        <location filename="qdjviewexporttiff.ui" line="76"/>
        <source>Compression</source>
        <translation>Сжатие</translation>
    </message>
    <message>
        <location filename="qdjviewexporttiff.ui" line="44"/>
        <source> dpi</source>
        <translation> dpi</translation>
    </message>
    <message>
        <location filename="qdjviewexporttiff.ui" line="88"/>
        <source>Force &amp;bitonal G4 compression.</source>
        <translation>Применять &amp;черно-белое сжатие G4.</translation>
    </message>
    <message>
        <location filename="qdjviewexporttiff.ui" line="13"/>
        <source>Form</source>
        <translation>Форма</translation>
    </message>
    <message>
        <location filename="qdjviewexporttiff.ui" line="129"/>
        <source>JPEG &amp;quality</source>
        <translation>&amp;Качество JPEG</translation>
    </message>
    <message>
        <location filename="qdjviewexporttiff.ui" line="37"/>
        <source>Maximum image resolution </source>
        <translation>Максимальное разрешение изображения </translation>
    </message>
    <message>
        <location filename="qdjviewexporttiff.ui" line="25"/>
        <source>Resolution</source>
        <translation>Разрешение</translation>
    </message>
</context>
<context>
    <name>QDjViewFind</name>
    <message>
        <location filename="qdjviewsidebar.cpp" line="1555"/>
        <source>Case sensitive</source>
        <translation>С учетом регистра</translation>
    </message>
    <message>
        <location filename="qdjviewsidebar.cpp" line="1585"/>
        <source>Find Next (F3) </source>
        <translation>Найти далее (F3) </translation>
    </message>
    <message>
        <location filename="qdjviewsidebar.cpp" line="1580"/>
        <source>Find Previous (Shift+F3) </source>
        <translation>Найти ранее (Shift+F3) </translation>
    </message>
    <message>
        <location filename="qdjviewsidebar.cpp" line="1642"/>
        <source>&lt;html&gt;&lt;b&gt;Finding text.&lt;/b&gt;&lt;br/&gt; Search hits appear progressively as soon as you type a search string. Typing enter jumps to the next hit. To move to the previous or next hit, you can also use the arrow buttons or the shortcuts &lt;tt&gt;F3&lt;/tt&gt; or &lt;tt&gt;Shift-F3&lt;/tt&gt;. You can also double click a page name. Use the &lt;tt&gt;Options&lt;/tt&gt; menu to search words only or to specify the case sensitivity.&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;b&gt;Поиск текста.&lt;/b&gt;&lt;br/&gt; Результаты поиска появляются постепенно по мере набора. Нажмите Enter для перехода к следующему результату. Используйте кнопки со стрелками или &lt;tt&gt;F3&lt;/tt&gt; и &lt;tt&gt;Shift-F3&lt;/tt&gt; для перехода к следующему/предыдущему результату. Вы также можете использовать двойной щелчок на названии страницы. В меню &lt;tt&gt;Настройки&lt;/tt&gt; можно настроить поиск только слов и чувствительность к регистру.&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="qdjviewsidebar.cpp" line="1595"/>
        <source>Options</source>
        <translation>Настройки</translation>
    </message>
    <message>
        <location filename="qdjviewsidebar.cpp" line="1558"/>
        <source>Words only</source>
        <translation>Только слова</translation>
    </message>
    <message>
        <location filename="qdjviewsidebar.cpp" line="1561"/>
        <source>Regular expression</source>
        <translation>Регулярное выражение</translation>
    </message>
    <message>
        <location filename="qdjviewsidebar.cpp" line="1591"/>
        <source>Reset search options to default values.</source>
        <translation>Вернуться к настройкам поиска по умолчанию.</translation>
    </message>
    <message>
        <location filename="qdjviewsidebar.cpp" line="1652"/>
        <source>Specify whether search hits must begin on a word boundary.</source>
        <translation>Искать по первым буквам целых слов.</translation>
    </message>
    <message>
        <location filename="qdjviewsidebar.cpp" line="1654"/>
        <source>Specify whether searches are case sensitive.</source>
        <translation>Требовать точного совпадения, различая заглавные и строчные буквы.</translation>
    </message>
    <message>
        <location filename="qdjviewsidebar.cpp" line="1656"/>
        <source>Regular expressions describe complex string matching patterns.</source>
        <translation>Регулярные выражения предназначены для ввода сложных шаблонов поиска текста.</translation>
    </message>
    <message>
        <location filename="qdjviewsidebar.cpp" line="1658"/>
        <source>&lt;html&gt;&lt;b&gt;Regular Expression Quick Guide&lt;/b&gt;&lt;ul&gt;&lt;li&gt;The dot &lt;tt&gt;.&lt;/tt&gt; matches any character.&lt;/li&gt;&lt;li&gt;Most characters match themselves.&lt;/li&gt;&lt;li&gt;Prepend a backslash &lt;tt&gt;\&lt;/tt&gt; to match special    characters &lt;tt&gt;()[]{}|*+.?!^$\&lt;/tt&gt;.&lt;/li&gt;&lt;li&gt;&lt;tt&gt;\b&lt;/tt&gt; matches a word boundary.&lt;/li&gt;&lt;li&gt;&lt;tt&gt;\w&lt;/tt&gt; matches a word character.&lt;/li&gt;&lt;li&gt;&lt;tt&gt;\d&lt;/tt&gt; matches a digit character.&lt;/li&gt;&lt;li&gt;&lt;tt&gt;\s&lt;/tt&gt; matches a blank character.&lt;/li&gt;&lt;li&gt;&lt;tt&gt;\n&lt;/tt&gt; matches a newline character.&lt;/li&gt;&lt;li&gt;&lt;tt&gt;[&lt;i&gt;a&lt;/i&gt;-&lt;i&gt;b&lt;/i&gt;]&lt;/tt&gt; matches characters in range    &lt;tt&gt;&lt;i&gt;a&lt;/i&gt;&lt;/tt&gt;-&lt;tt&gt;&lt;i&gt;b&lt;/i&gt;&lt;/tt&gt;.&lt;/li&gt;&lt;li&gt;&lt;tt&gt;[^&lt;i&gt;a&lt;/i&gt;-&lt;i&gt;b&lt;/i&gt;]&lt;/tt&gt; matches characters outside range    &lt;tt&gt;&lt;i&gt;a&lt;/i&gt;&lt;/tt&gt;-&lt;tt&gt;&lt;i&gt;b&lt;/i&gt;&lt;/tt&gt;.&lt;/li&gt;&lt;li&gt;&lt;tt&gt;&lt;i&gt;a&lt;/i&gt;|&lt;i&gt;b&lt;/i&gt;&lt;/tt&gt; matches either regular expression    &lt;tt&gt;&lt;i&gt;a&lt;/i&gt;&lt;/tt&gt; or regular expression &lt;tt&gt;&lt;i&gt;b&lt;/i&gt;&lt;/tt&gt;.&lt;/li&gt;&lt;li&gt;&lt;tt&gt;&lt;i&gt;a&lt;/i&gt;{&lt;i&gt;n&lt;/i&gt;,&lt;i&gt;m&lt;/i&gt;}&lt;/tt&gt; matches regular expression    &lt;tt&gt;&lt;i&gt;a&lt;/i&gt;&lt;/tt&gt; repeated &lt;tt&gt;&lt;i&gt;n&lt;/i&gt;&lt;/tt&gt; to &lt;tt&gt;&lt;i&gt;m&lt;/i&gt;&lt;/tt&gt;    times.&lt;/li&gt;&lt;li&gt;&lt;tt&gt;&lt;i&gt;a&lt;/i&gt;?&lt;/tt&gt;, &lt;tt&gt;&lt;i&gt;a&lt;/i&gt;*&lt;/tt&gt;, and &lt;tt&gt;&lt;i&gt;a&lt;/i&gt;+&lt;/tt&gt;    are shorthands for &lt;tt&gt;&lt;i&gt;a&lt;/i&gt;{0,1}&lt;/tt&gt;, &lt;tt&gt;&lt;i&gt;a&lt;/i&gt;{0,}&lt;/tt&gt;,     and &lt;tt&gt;&lt;i&gt;a&lt;/i&gt;{1,}&lt;/tt&gt;.&lt;/li&gt;&lt;li&gt;Use parentheses &lt;tt&gt;()&lt;/tt&gt; to group regular expressions     before &lt;tt&gt;?+*{&lt;/tt&gt;.&lt;/li&gt;&lt;/ul&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;b&gt;Справка по регулярным выражениям&lt;/b&gt;&lt;ul&gt;&lt;li&gt;Точка (&lt;tt&gt;&lt;b&gt;.&lt;/b&gt;&lt;/tt&gt;) сопоставляется с любым символом.&lt;/li&gt;&lt;li&gt;Большинство символов в регулярном выражении сопоставляется с теми же символами в тексте.&lt;/li&gt;&lt;li&gt;Добавляйте обратный слэш (&lt;tt&gt;&lt;b&gt;\&lt;/b&gt;&lt;/tt&gt;) перед символами &lt;tt&gt;&lt;b&gt;()[]{}|*+.?!^$\&lt;/b&gt;&lt;/tt&gt;, если хотите отменить их специальное значение.&lt;/li&gt;&lt;li&gt;&lt;tt&gt;&lt;b&gt;\b&lt;/b&gt;&lt;/tt&gt; сопоставляется с границей слова.&lt;/li&gt;&lt;li&gt;&lt;tt&gt;&lt;b&gt;\w&lt;/b&gt;&lt;/tt&gt; сопоставляется с любым символом, который может входить в слово (буквы, цифры, знак нижнего подчёркивания).&lt;/li&gt;&lt;li&gt;&lt;tt&gt;&lt;b&gt;\d&lt;/b&gt;&lt;/tt&gt; сопоставляется с цифрой.&lt;/li&gt;&lt;li&gt;&lt;tt&gt;&lt;b&gt;\s&lt;/b&gt;&lt;/tt&gt; сопоставляется с пробельным символом.&lt;/li&gt;&lt;li&gt;&lt;tt&gt;&lt;b&gt;\n&lt;/b&gt;&lt;/tt&gt; сопоставляется с символом новой строки.&lt;/li&gt;&lt;li&gt;&lt;tt&gt;&lt;b&gt;[a-b]&lt;/b&gt;&lt;/tt&gt; сопоставляется с любым символом в диапазоне от &lt;tt&gt;&lt;b&gt;a&lt;/b&gt;&lt;/tt&gt; до &lt;tt&gt;&lt;b&gt;b&lt;/b&gt;&lt;/tt&gt;.&lt;/li&gt;&lt;li&gt;&lt;tt&gt;&lt;b&gt;[^a-b]&lt;/b&gt;&lt;/tt&gt; сопоставляется с любым символом вне диапазона от &lt;tt&gt;&lt;b&gt;a&lt;/b&gt;&lt;/tt&gt; до &lt;tt&gt;&lt;b&gt;b&lt;/b&gt;&lt;/tt&gt;.&lt;/li&gt;&lt;li&gt;&lt;tt&gt;&lt;b&gt;a|b&lt;/b&gt;&lt;/tt&gt; сопоставляется с одним из регулярных выражений &lt;tt&gt;&lt;b&gt;a&lt;/b&gt;&lt;/tt&gt; и &lt;tt&gt;&lt;b&gt;b&lt;/b&gt;&lt;/tt&gt;.&lt;/li&gt;&lt;li&gt;&lt;tt&gt;&lt;b&gt;a{n,m}&lt;/b&gt;&lt;/tt&gt; соответствует регулярному выражению &lt;tt&gt;&lt;b&gt;a&lt;/b&gt;&lt;/tt&gt;, повторенному от &lt;tt&gt;&lt;b&gt;n&lt;/b&gt;&lt;/tt&gt; до &lt;tt&gt;&lt;b&gt;m&lt;/b&gt;&lt;/tt&gt; раз.&lt;/li&gt;&lt;li&gt;&lt;tt&gt;&lt;b&gt;a?&lt;/b&gt;&lt;/tt&gt;, &lt;tt&gt;&lt;b&gt;a*&lt;/b&gt;&lt;/tt&gt;, и &lt;tt&gt;&lt;b&gt;a+&lt;/b&gt;&lt;/tt&gt; являются сокращениями для &lt;tt&gt;&lt;b&gt;a{0,1}&lt;/b&gt;&lt;/tt&gt;, &lt;tt&gt;&lt;b&gt;a{0,}&lt;/b&gt;&lt;/tt&gt; и &lt;tt&gt;&lt;b&gt;a{1,}&lt;/b&gt;&lt;/tt&gt; соответственно.&lt;/li&gt;&lt;li&gt;Используйте скобки &lt;tt&gt;&lt;b&gt;()&lt;/b&gt;&lt;/tt&gt; для группировки регулярного выражения перед символами &lt;tt&gt;&lt;b&gt;?+*{&lt;/b&gt;&lt;/tt&gt;.&lt;/li&gt;&lt;/ul&gt;&lt;/html&gt;</translation>
    </message>
</context>
<context>
    <name>QDjViewFind::Model</name>
    <message>
        <location filename="qdjviewsidebar.cpp" line="946"/>
        <source>1 hit</source>
        <translation>1 результат</translation>
    </message>
    <message>
        <location filename="qdjviewsidebar.cpp" line="1373"/>
        <source>&lt;html&gt;Document is not searchable. No page contains information about its textual content.&lt;/html&gt;</source>
        <translation>&lt;html&gt;Поиск в документе невозможен. Ни одна из страниц не содержит текстовой информации.&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="qdjviewsidebar.cpp" line="1368"/>
        <source>No hits!</source>
        <translation>Нет совпадений!</translation>
    </message>
    <message>
        <location filename="qdjviewsidebar.cpp" line="1015"/>
        <source>Page %1 (1 hit)</source>
        <translation>Page %1 (1 результат)</translation>
    </message>
    <message>
        <location filename="qdjviewsidebar.cpp" line="1333"/>
        <source>Searching page %1.</source>
        <translation>Поиск на странице %1.</translation>
    </message>
    <message>
        <location filename="qdjviewsidebar.cpp" line="1320"/>
        <source>Searching page %1 (waiting for data.)</source>
        <translation>Поиск на странице %1 (ожидание данных)</translation>
    </message>
    <message numerus="yes">
        <location filename="qdjviewsidebar.cpp" line="947"/>
        <source>%n hits</source>
        <translation>
            <numerusform>%n совпадение</numerusform>
            <numerusform>%n совпадения</numerusform>
            <numerusform>%n совпадений</numerusform>
        </translation>
    </message>
    <message numerus="yes">
        <location filename="qdjviewsidebar.cpp" line="1017"/>
        <source>Page %1 (%n hits)</source>
        <translation>
            <numerusform>Страница %1 (%n результат)</numerusform>
            <numerusform>Страница %1 (%n результата)</numerusform>
            <numerusform>Страница %1 (%n результатов)</numerusform>
        </translation>
    </message>
    <message>
        <location filename="qdjviewsidebar.cpp" line="1379"/>
        <source>&lt;html&gt;Invalid regular expression.&lt;/html&gt;</source>
        <translation>&lt;html&gt;Ошибка в регулярном выражении.&lt;/html&gt;</translation>
    </message>
</context>
<context>
    <name>QDjViewImgExporter</name>
    <message>
        <location filename="qdjviewexporters.cpp" line="1848"/>
        <source>%1 Files (*.%2)</source>
        <comment>JPG Files</comment>
        <translation>Файлы %1 (*.%2)</translation>
    </message>
    <message>
        <location filename="qdjviewexporters.cpp" line="1847"/>
        <source>%1 Image</source>
        <comment>JPG Image</comment>
        <translation>Изображение %1</translation>
    </message>
    <message>
        <location filename="qdjviewexporters.cpp" line="1914"/>
        <source>Cannot render page.</source>
        <translation>Невозможно преобразовать страницу.</translation>
    </message>
    <message>
        <location filename="qdjviewexporters.cpp" line="1924"/>
        <source>Image format %1 not supported.</source>
        <translation>Формат изображения %1 не поддерживается.</translation>
    </message>
</context>
<context>
    <name>QDjViewInfoDialog</name>
    <message>
        <location filename="qdjviewdialogs.cpp" line="465"/>
        <source>Bundled DjVu document</source>
        <translation>Связанный документ DjVu (в одном файле)</translation>
    </message>
    <message>
        <location filename="qdjviewinfodialog.ui" line="204"/>
        <source>&amp;Close</source>
        <translation>&amp;Закрыть</translation>
    </message>
    <message>
        <location filename="qdjviewinfodialog.ui" line="16"/>
        <source>Dialog</source>
        <translation>Диалог</translation>
    </message>
    <message>
        <location filename="qdjviewinfodialog.ui" line="29"/>
        <source>&amp;Document</source>
        <translation>&amp;Документ</translation>
    </message>
    <message>
        <location filename="qdjviewinfodialog.ui" line="88"/>
        <source>&amp;File</source>
        <translation>&amp;Файл</translation>
    </message>
    <message>
        <location filename="qdjviewdialogs.cpp" line="225"/>
        <source>File #</source>
        <translation>№ файла</translation>
    </message>
    <message>
        <location filename="qdjviewinfodialog.ui" line="108"/>
        <source>File: </source>
        <translation>Файл: </translation>
    </message>
    <message>
        <location filename="qdjviewdialogs.cpp" line="448"/>
        <source>File #%1 - </source>
        <translation>Файл №%1 - </translation>
    </message>
    <message>
        <location filename="qdjviewdialogs.cpp" line="225"/>
        <source>File Name</source>
        <translation>Имя файла</translation>
    </message>
    <message>
        <location filename="qdjviewdialogs.cpp" line="226"/>
        <source>File Size</source>
        <translation>Размер файла</translation>
    </message>
    <message>
        <location filename="qdjviewdialogs.cpp" line="226"/>
        <source>File Type</source>
        <translation>Тип файла</translation>
    </message>
    <message>
        <location filename="qdjviewdialogs.cpp" line="259"/>
        <source>&lt;html&gt;&lt;b&gt;Document information&lt;/b&gt;&lt;br&gt;This panel shows information about the document and its component files. Select a component file to display detailled information in the &lt;tt&gt;File&lt;/tt&gt; tab. Double click a component file to show the corresponding page in the main window. &lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;b&gt;Информация о документе&lt;/b&gt;&lt;br&gt;Эта панель показывает информацию о документе и его файлах-компонентах. Выберите файл для просмотра подробной информации во вкладке &lt;tt&gt;Файл&lt;/tt&gt;. Вы можете перейти к странице двойным щелчком по соответствующей строке. &lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="qdjviewdialogs.cpp" line="267"/>
        <source>&lt;html&gt;&lt;b&gt;File and page information&lt;/b&gt;&lt;br&gt;This panel shows the structure of the DjVu data corresponding to the component file or the page selected in the &lt;tt&gt;Document&lt;/tt&gt; tab. The arrow buttons jump to the previous or next component file.&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;b&gt;Информация о файле или странице&lt;/b&gt;&lt;br&gt;Эта панель показывает структуру данных DjVu, соответствующих компонентному файлу или странице, выбранной во вкладке &lt;tt&gt;Документ&lt;/tt&gt;. Кнопки со стрелками позволяют перейти к предыдущему или следующему компонентному файлу.&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="qdjviewdialogs.cpp" line="467"/>
        <source>Indirect DjVu document</source>
        <translation>Непрямой документ DjVu (страницы в отдельных файлах)</translation>
    </message>
    <message>
        <location filename="qdjviewdialogs.cpp" line="509"/>
        <location filename="qdjviewdialogs.cpp" line="516"/>
        <source>n/a</source>
        <translation>недоступен</translation>
    </message>
    <message>
        <location filename="qdjviewdialogs.cpp" line="469"/>
        <source>Obsolete bundled DjVu document</source>
        <translation>Устаревший формат связанного документа DjVu</translation>
    </message>
    <message>
        <location filename="qdjviewdialogs.cpp" line="471"/>
        <source>Obsolete indexed DjVu document</source>
        <translation>Устаревший формат индексированного документа DjVu</translation>
    </message>
    <message>
        <location filename="qdjviewdialogs.cpp" line="523"/>
        <source> Page </source>
        <translation> Страница </translation>
    </message>
    <message>
        <location filename="qdjviewdialogs.cpp" line="227"/>
        <source>Page #</source>
        <translation>№ страницы</translation>
    </message>
    <message>
        <location filename="qdjviewdialogs.cpp" line="440"/>
        <source>Page #%1</source>
        <translation>Страница №%1</translation>
    </message>
    <message>
        <location filename="qdjviewdialogs.cpp" line="437"/>
        <source>Page #%1 - « %2 »</source>
        <translation>Страница №%1 - « %2 »</translation>
    </message>
    <message>
        <location filename="qdjviewdialogs.cpp" line="227"/>
        <source>Page Title</source>
        <translation>Заголовок страницы</translation>
    </message>
    <message>
        <location filename="qdjviewdialogs.cpp" line="527"/>
        <source> Shared </source>
        <translation> Общий </translation>
    </message>
    <message>
        <location filename="qdjviewdialogs.cpp" line="445"/>
        <source>Shared annotations</source>
        <translation>Общие примечания</translation>
    </message>
    <message>
        <location filename="qdjviewdialogs.cpp" line="447"/>
        <source>Shared data</source>
        <translation>Общие данные</translation>
    </message>
    <message>
        <location filename="qdjviewdialogs.cpp" line="461"/>
        <source>Single DjVu page</source>
        <translation>Одиночная страница DjVu</translation>
    </message>
    <message>
        <location filename="qdjviewdialogs.cpp" line="443"/>
        <source>Thumbnails</source>
        <translation>Макеты страниц</translation>
    </message>
    <message>
        <location filename="qdjviewdialogs.cpp" line="525"/>
        <source> Thumbnails </source>
        <translation> Макеты страниц </translation>
    </message>
    <message>
        <location filename="qdjviewinfodialog.ui" line="154"/>
        <source>&amp;View Page</source>
        <translation>&amp;Просмотр страницы</translation>
    </message>
    <message>
        <location filename="qdjviewdialogs.cpp" line="321"/>
        <source>Waiting for data...</source>
        <translation>Ожидание данных...</translation>
    </message>
    <message>
        <location filename="qdjviewdialogs.cpp" line="475"/>
        <source>1 file</source>
        <translation>1 файл</translation>
    </message>
    <message numerus="yes">
        <location filename="qdjviewdialogs.cpp" line="475"/>
        <source>%n files</source>
        <translation>
            <numerusform>%n файл</numerusform>
            <numerusform>%n файла</numerusform>
            <numerusform>%n файлов</numerusform>
        </translation>
    </message>
    <message>
        <location filename="qdjviewdialogs.cpp" line="476"/>
        <source>1 page</source>
        <translation>1 страница</translation>
    </message>
    <message numerus="yes">
        <location filename="qdjviewdialogs.cpp" line="476"/>
        <source>%n pages</source>
        <translation>
            <numerusform>%n страница</numerusform>
            <numerusform>%n страницы</numerusform>
            <numerusform>%n страниц</numerusform>
        </translation>
    </message>
</context>
<context>
    <name>QDjViewMetaDialog</name>
    <message>
        <location filename="qdjviewmetadialog.ui" line="168"/>
        <source>&amp;Close</source>
        <translation>&amp;Закрыть</translation>
    </message>
    <message>
        <location filename="qdjviewdialogs.cpp" line="594"/>
        <source>Ctrl+C</source>
        <comment>copy</comment>
        <translation>Ctrl+C</translation>
    </message>
    <message>
        <location filename="qdjviewmetadialog.ui" line="16"/>
        <source>Dialog</source>
        <translation>Диалог</translation>
    </message>
    <message>
        <location filename="qdjviewmetadialog.ui" line="29"/>
        <source>&amp;Document Metadata</source>
        <translation>&amp;Метаданные документа</translation>
    </message>
    <message>
        <location filename="qdjviewdialogs.cpp" line="633"/>
        <source>&lt;html&gt;&lt;b&gt;Document metadata&lt;/b&gt;&lt;br&gt;This panel displays metadata pertaining to the document, such as author, title, references, etc. This information can be saved into the document with program &lt;tt&gt;djvused&lt;/tt&gt;: use the commands &lt;tt&gt;create-shared-ant&lt;/tt&gt; and &lt;tt&gt;set-meta&lt;/tt&gt;.&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;b&gt;Метаданные документа&lt;/b&gt;&lt;br&gt;Эта панель отображает метаданные, относящиеся к документу, такие как автор, заголовок, ссылки и т.д. Эта информация может быть записага в документ программой &lt;tt&gt;djvused&lt;/tt&gt;: используйте команды &lt;tt&gt;create-shared-ant&lt;/tt&gt; и &lt;tt&gt;set-meta&lt;/tt&gt;.&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="qdjviewdialogs.cpp" line="642"/>
        <source>&lt;html&gt;&lt;b&gt;Page metadata&lt;/b&gt;&lt;br&gt;This panel displays metadata pertaining to a specific page. Page specific metadata override document metadata. This information can be saved into the document with program &lt;tt&gt;djvused&lt;/tt&gt;: use command &lt;tt&gt;select&lt;/tt&gt; to select the page and command &lt;tt&gt;set-meta&lt;/tt&gt; to specify the metadata entries.&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;b&gt;Метаданные страницы&lt;/b&gt;&lt;br&gt;Эта панель отображает метаданные, относящиеся к отдельной странице. Метаданные страницы переопределяют метаданные документа. Эта информация может быть записана в документ программой &lt;tt&gt;djvused&lt;/tt&gt;: используйте соманду &lt;tt&gt;select&lt;/tt&gt; для выбора страницы и команду &lt;tt&gt;set-meta&lt;/tt&gt; для задания метаданных.&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="qdjviewdialogs.cpp" line="597"/>
        <source> Key </source>
        <translation> Ключ </translation>
    </message>
    <message>
        <location filename="qdjviewmetadialog.ui" line="75"/>
        <source>Page:</source>
        <translation>Страница:</translation>
    </message>
    <message>
        <location filename="qdjviewmetadialog.ui" line="55"/>
        <source>&amp;Page Metadata</source>
        <translation>Метаданные &amp;страницы</translation>
    </message>
    <message>
        <location filename="qdjviewdialogs.cpp" line="597"/>
        <source> Value </source>
        <translation> Значение </translation>
    </message>
    <message>
        <location filename="qdjviewmetadialog.ui" line="121"/>
        <source>&amp;View Page</source>
        <translation>&amp;Просмотр страницы</translation>
    </message>
</context>
<context>
    <name>QDjViewOutline</name>
    <message>
        <location filename="qdjviewsidebar.cpp" line="175"/>
        <location filename="qdjviewsidebar.cpp" line="215"/>
        <source>Go to page %1</source>
        <translation>Перейти к странице %1</translation>
    </message>
    <message>
        <location filename="qdjviewsidebar.cpp" line="113"/>
        <source>&lt;html&gt;&lt;b&gt;Document outline.&lt;/b&gt;&lt;br/&gt; This panel display the document outline, or the page names when the outline is not available, Double-click any entry to jump to the selected page.&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;b&gt;Эскиз документа.&lt;/b&gt;&lt;br/&gt; Эта панель отображает эскиз документа или названия страниц, если эскиз недоступен. Двойным щелчком можно перейти к выбранной странице.&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="qdjviewsidebar.cpp" line="146"/>
        <source>Outline data is corrupted</source>
        <translation>Данные эскиза испорчены</translation>
    </message>
    <message>
        <location filename="qdjviewsidebar.cpp" line="172"/>
        <location filename="qdjviewsidebar.cpp" line="207"/>
        <source>Page %1</source>
        <translation>Страница %1</translation>
    </message>
    <message>
        <location filename="qdjviewsidebar.cpp" line="165"/>
        <source>Pages</source>
        <translation>Страницы</translation>
    </message>
</context>
<context>
    <name>QDjViewPSExporter</name>
    <message>
        <location filename="qdjviewexporters.cpp" line="600"/>
        <source>Booklet</source>
        <comment>tab caption</comment>
        <translation>Буклет</translation>
    </message>
    <message>
        <location filename="qdjviewexporters.cpp" line="548"/>
        <source>Encapsulated PostScript</source>
        <translation>Инкапсулированный PostScript</translation>
    </message>
    <message>
        <location filename="qdjviewexporters.cpp" line="618"/>
        <source>&lt;html&gt;&lt;b&gt;Position and scaling.&lt;/b&gt;&lt;br&gt;Option &lt;tt&gt;Scale to fit&lt;/tt&gt; accommodates whatever paper size your printer uses. Zoom factor &lt;tt&gt;100%&lt;/tt&gt; reproduces the initial document size. Orientation &lt;tt&gt;Automatic&lt;/tt&gt; chooses portrait or landscape on a page per page basis.&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;b&gt;Положение и масштаб.&lt;/b&gt;&lt;br&gt;Опция &lt;tt&gt;Масштабировать по размеру страницы&lt;/tt&gt; приспосабливает документ к размерам листов бумаги, используемой вашим принтером. Масштаб &lt;tt&gt;100%&lt;/tt&gt; воспроизводит исходный размер документа. Опция ориентации &lt;tt&gt;Автоматически&lt;/tt&gt; выбирает книжную или альбомну ориентацию по размерам каждой страницы.&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="qdjviewexporters.cpp" line="610"/>
        <source>&lt;html&gt;&lt;b&gt;PostScript options.&lt;/b&gt;&lt;br&gt;Option &lt;tt&gt;Color&lt;/tt&gt; enables color printing. Document pages can be decorated with frame and crop marks. PostScript language level 1 is only useful with very old printers. Level 2 works with most printers. Level 3 print color document faster on recent printers.&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;b&gt;Настройки PostScript.&lt;/b&gt;&lt;br&gt;Опция &lt;tt&gt;Цветной&lt;/tt&gt; включает цветную печать. К страницам может быть добавлена рамка и разметка. Уровень языка PostScript 1 (PostScript Level 1) полезен только при работе с очень старыми принтерами. PostScript Level 2 работает с большинством принтеров. PostScript Level 3 работает быстрее при цветной печати на современных принтерах.&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="qdjviewexporters.cpp" line="625"/>
        <source>&lt;html&gt;&lt;b&gt;Producing booklets.&lt;/b&gt;&lt;br&gt;The booklet mode prints the selected pages as sheets suitable for folding one or several booklets. Several booklets might be produced when a maximum number of sheets per booklet is specified. You can either use a duplex printer or print rectos and versos separately.&lt;p&gt; Shifting rectos and versos is useful with poorly aligned duplex printers. The center margins determine how much space is left between the pages to fold the sheets. This space slowly increases from the inner sheet to the outer sheet.&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;b&gt;Изготовление буклетов.&lt;/b&gt;&lt;br&gt;В режиме буклета печатаются выбранные страницы в виде листов, готовых для составления одного или нескольких буклетов. Вы можете либо пользоваться двухсторонним принтером, либо печатать отдельно четные и нечетные листы.&lt;p&gt; Смещение четных листов от нечетных полезно при печати на плохо совмещенных двухсторонних принтерах. Центральное поле определяет размер пустого пространства между страницами на одном листе. Этот размер медленно увеличивается от внутреннего листа к внешнему.&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="qdjviewexporters.cpp" line="599"/>
        <source>Position</source>
        <comment>tab caption</comment>
        <translation>Позиция</translation>
    </message>
    <message>
        <location filename="qdjviewexporters.cpp" line="544"/>
        <source>PostScript</source>
        <translation>PostScript</translation>
    </message>
    <message>
        <location filename="qdjviewexporters.cpp" line="598"/>
        <source>PostScript</source>
        <comment>tab caption</comment>
        <translation>PostScript</translation>
    </message>
    <message>
        <location filename="qdjviewexporters.cpp" line="545"/>
        <location filename="qdjviewexporters.cpp" line="549"/>
        <source>PostScript Files (*.ps *.eps)</source>
        <translation>Файлы PostScript (*.ps *.eps)</translation>
    </message>
    <message>
        <location filename="qdjviewexporters.cpp" line="1116"/>
        <source>Save job creation failed!</source>
        <translation>Не удалось запустить процесс сохранения!</translation>
    </message>
</context>
<context>
    <name>QDjViewPdfExporter</name>
    <message>
        <location filename="qdjviewexporters.cpp" line="1747"/>
        <source>Error while creating pdf file.</source>
        <translation>Ошибка создания файла PDF.</translation>
    </message>
    <message>
        <location filename="qdjviewexporters.cpp" line="1703"/>
        <source>&lt;html&gt;&lt;b&gt;PDF options.&lt;/b&gt;&lt;br&gt;These options control the characteristics of the images embedded in the exported PDF files. The resolution box limits their maximal resolution. Forcing bitonal G4 compression encodes all pages in black and white using the CCITT Group 4 compression. Allowing JPEG compression uses lossy JPEG for all non bitonal or subsampled images. Otherwise, allowing deflate compression produces more compact files. &lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;b&gt;Настройки TIFF.&lt;/b&gt;&lt;br&gt;Эти опции управляют характеристиками изображений, встраиваемых в экспортируемые файлы PDF. Элемент &quot;Разрешение&quot; задает верхнюю границу разрешения изображений. При применении черно-белого сжатия G4 все страницы кодируются с помощью сжатия CCITT Group 4. Сжатие JPEG будет применяться ко всем цветным изображениям.&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="qdjviewexporters.cpp" line="1692"/>
        <source>PDF Document</source>
        <translation>Документ PDF</translation>
    </message>
    <message>
        <location filename="qdjviewexporters.cpp" line="1769"/>
        <source>PDF export was not compiled.</source>
        <translation>Экспорт в PDF не был произведен.</translation>
    </message>
    <message>
        <location filename="qdjviewexporters.cpp" line="1693"/>
        <source>PDF Files (*.pdf)</source>
        <translation>Файлы PDF (*.pdf)</translation>
    </message>
    <message>
        <location filename="qdjviewexporters.cpp" line="1702"/>
        <source>PDF Options</source>
        <comment>tab caption</comment>
        <translation>Настройки PDF</translation>
    </message>
    <message>
        <location filename="qdjviewexporters.cpp" line="1753"/>
        <location filename="qdjviewexporters.cpp" line="1795"/>
        <source>System error: %1.</source>
        <translation>Системная ошибка: %1.</translation>
    </message>
    <message>
        <location filename="qdjviewexporters.cpp" line="1751"/>
        <source>Unable to create output file.</source>
        <translation>Не удается создать выходной файл.</translation>
    </message>
    <message>
        <location filename="qdjviewexporters.cpp" line="1793"/>
        <source>Unable to create temporary file.</source>
        <translation>Не удается создать временный файл.</translation>
    </message>
    <message>
        <location filename="qdjviewexporters.cpp" line="1756"/>
        <source>Unable to reopen temporary file.</source>
        <translation>Невозможно заново открыть временный файл.</translation>
    </message>
</context>
<context>
    <name>QDjViewPlugin::Document</name>
    <message>
        <location filename="qdjviewplugin.cpp" line="301"/>
        <source>Requesting %1.</source>
        <translation>Запрашивается %1.</translation>
    </message>
</context>
<context>
    <name>QDjViewPrefsDialog</name>
    <message>
        <location filename="qdjviewprefsdialog.ui" line="1170"/>
        <source>&amp;Apply</source>
        <translation>&amp;Применить</translation>
    </message>
    <message>
        <location filename="qdjviewprefs.cpp" line="814"/>
        <source>&lt;html&gt;&lt;b&gt;Caches.&lt;/b&gt;&lt;br&gt;The &lt;i&gt;pixel cache&lt;/i&gt; stores image data located outside the visible area. This cache makes panning smoother. The &lt;i&gt;decoded page cache&lt;/i&gt; contains partially decoded pages. It provides faster response times when navigating a multipage document or when returning to a previously viewed page. Clearing this cache might be useful to reflect a change in the page data without restarting the DjVu viewer.&lt;p&gt;&lt;b&gt;Miscellaneous.&lt;/b&gt;&lt;br&gt;Forcing a manual color correction can be useful when using ancient printers. The advanced features check box enables a small number of additional menu entries useful for authoring DjVu files.&lt;/html&gt;</source>
        <translation>&lt;b&gt;Кэши.&lt;/b&gt;&lt;br&gt;&lt;i&gt;Пиксельный кэш&lt;/i&gt; хранит данные изображения вне видимой области. Этот кэш обеспечивает плавную прокрутку. &lt;i&gt;Кэш декодированных страниц&lt;/i&gt; содержит частично декодированные страницы. Он обеспечивает более короткое время отклика при при повторонм просмотре страниц многостраничного документа. Очистка кэша может быть полезна для отражения изменений страницы без перезапуска просмотрщика.&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="qdjviewprefsdialog.ui" line="970"/>
        <source>Cache</source>
        <translation>Кэш</translation>
    </message>
    <message>
        <location filename="qdjviewprefsdialog.ui" line="1184"/>
        <source>Cancel</source>
        <translation>Отмена</translation>
    </message>
    <message>
        <location filename="qdjviewprefsdialog.ui" line="982"/>
        <source>&amp;Clear</source>
        <translation>&amp;Очистить</translation>
    </message>
    <message>
        <location filename="qdjviewprefsdialog.ui" line="109"/>
        <source>Darker</source>
        <translation>Темнее</translation>
    </message>
    <message>
        <location filename="qdjviewprefsdialog.ui" line="1021"/>
        <source>&amp;Decoded page cache: </source>
        <translation>Кэш &amp;декодированных страниц: </translation>
    </message>
    <message>
        <location filename="qdjviewprefsdialog.ui" line="1144"/>
        <source>&amp;Defaults</source>
        <translation>&amp;По умолчанию</translation>
    </message>
    <message>
        <location filename="qdjviewprefsdialog.ui" line="13"/>
        <source>Dialog</source>
        <translation>Диалог</translation>
    </message>
    <message>
        <location filename="qdjviewprefsdialog.ui" line="377"/>
        <source>Display</source>
        <translation>Отображение</translation>
    </message>
    <message>
        <location filename="qdjviewprefsdialog.ui" line="459"/>
        <source>Display &amp;annotations</source>
        <translation>Показывать &amp;примечания</translation>
    </message>
    <message>
        <location filename="qdjviewprefsdialog.ui" line="452"/>
        <source>Display page &amp;frames</source>
        <translation>Показывать &amp;рамки страниц</translation>
    </message>
    <message>
        <location filename="qdjviewprefsdialog.ui" line="301"/>
        <source>Embedded Plugin</source>
        <translation>встраиваемого плагина</translation>
    </message>
    <message>
        <location filename="qdjviewprefsdialog.ui" line="740"/>
        <source>Enable magnifying &amp;lens</source>
        <translation>Включить &amp;лупу</translation>
    </message>
    <message>
        <location filename="qdjviewprefsdialog.ui" line="652"/>
        <source>For displaying hyper&amp;links: </source>
        <translation>Для отображения &amp;гиперссылок: </translation>
    </message>
    <message>
        <location filename="qdjviewprefsdialog.ui" line="672"/>
        <source>For displaying the &amp;lens: </source>
        <translation>Для показа &amp;лупы: </translation>
    </message>
    <message>
        <location filename="qdjviewprefsdialog.ui" line="662"/>
        <source>For &amp;selecting text or images: </source>
        <translation>Для &amp;выделения текста и изображений: </translation>
    </message>
    <message>
        <location filename="qdjviewprefsdialog.ui" line="296"/>
        <source>Full Page Plugin</source>
        <translation>полностраничного плагина</translation>
    </message>
    <message>
        <location filename="qdjviewprefsdialog.ui" line="291"/>
        <source>Full Screen Standalone Viewer</source>
        <translation>самостоятельного приложения (полноэкранный режим)</translation>
    </message>
    <message>
        <location filename="qdjviewprefsdialog.ui" line="40"/>
        <source>Gamma</source>
        <translation>Гамма</translation>
    </message>
    <message>
        <location filename="qdjviewprefsdialog.ui" line="916"/>
        <source>&amp;Host: </source>
        <translation>&amp;Хост: </translation>
    </message>
    <message>
        <location filename="qdjviewprefs.cpp" line="789"/>
        <source>&lt;html&gt;&lt;b&gt;Initial interface setup.&lt;/b&gt;&lt;br&gt;DjView can run as a standalone viewer, as a full screen viewer, as a full page browser plugin, or as a plugin embedded inside a html page. For each case, check the &lt;tt&gt;Remember&lt;/tt&gt; box to automatically save and restore the interface setup. Otherwise, specify an initial configuration.&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;b&gt;Исходные настройки интерфейса.&lt;/b&gt;&lt;br&gt;DjView может быть запущен как самостоятельное приложение, в полноэкранном режиме, в виде плагина браузера или плагина, встроенного в страницу HTML. Для каждого случая установите опцию &lt;tt&gt;Запоминать настройки&lt;/tt&gt; для автоматического сохранения и восстановления настроек интерфейса. Иначе, определите начальную конфигурацию.&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="qdjviewprefs.cpp" line="799"/>
        <source>&lt;html&gt;&lt;b&gt;Modifiers keys.&lt;/b&gt;&lt;br&gt;Define which combination of modifier keys will show the manifying lens, temporarily enable the selection mode, or highlight the hyperlinks.&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;b&gt;Служебные клавиши.&lt;/b&gt;&lt;br&gt;Определите, какие комбинации служебных клавиш будут показывать лупу, временно включать режим выделения или подсвечивать гиперссылки.&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="qdjviewprefs.cpp" line="833"/>
        <source>&lt;html&gt;&lt;b&gt;Network proxy settings.&lt;/b&gt;&lt;br&gt;These proxy settings are used when the standalone djview viewer accesses a djvu document through a http url. The djview plugin always uses the proxy settings of the web browser.&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;b&gt;Настройки прокси.&lt;/b&gt;&lt;br&gt;Эти настройки прокси используются, когда просмотрщик в качестве самостоятельного приложения обращается к документу DjVu по протоколу http. Если просмотрщик запускается к виде плагина, всегда используются настройки браузера для прокси.&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="qdjviewprefsdialog.ui" line="60"/>
        <source>&lt;html&gt;Screen color correction.&lt;br&gt;Adjust slider until gray shades look similar.&lt;/html&gt;</source>
        <translation>&lt;html&gt;Цветовая коррекция для отображения.&lt;br&gt;Установите ползунок так, чтобы оттенки серого выглядели максимально похожими.&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="qdjviewprefsdialog.ui" line="261"/>
        <source>&amp;Interface</source>
        <translation>&amp;Интерфейс</translation>
    </message>
    <message>
        <location filename="qdjviewprefsdialog.ui" line="628"/>
        <source>&amp;Keys</source>
        <translation>&amp;Комбинации клавиш</translation>
    </message>
    <message>
        <location filename="qdjviewprefsdialog.ui" line="640"/>
        <source>Keys</source>
        <translation>Комбинации клавиш</translation>
    </message>
    <message>
        <location filename="qdjviewprefsdialog.ui" line="728"/>
        <source>&amp;Lens</source>
        <translation>&amp;Лупа</translation>
    </message>
    <message>
        <location filename="qdjviewprefsdialog.ui" line="750"/>
        <source>Lens options</source>
        <translation>Настройки лупы</translation>
    </message>
    <message>
        <location filename="qdjviewprefsdialog.ui" line="762"/>
        <source>Lens &amp;size: </source>
        <translation>&amp;Размер: </translation>
    </message>
    <message>
        <location filename="qdjviewprefsdialog.ui" line="129"/>
        <source>Lighter</source>
        <translation>Светлее</translation>
    </message>
    <message>
        <location filename="qdjviewprefsdialog.ui" line="772"/>
        <source>Magnifying &amp;power: </source>
        <translation>&amp;Увеличение: </translation>
    </message>
    <message>
        <location filename="qdjviewprefsdialog.ui" line="339"/>
        <source>&amp;Menu bar</source>
        <translation>&amp;Меню</translation>
    </message>
    <message>
        <location filename="qdjviewprefsdialog.ui" line="842"/>
        <source>&amp;Network</source>
        <translation>&amp;Сеть</translation>
    </message>
    <message>
        <location filename="qdjviewprefsdialog.ui" line="1177"/>
        <source>&amp;Ok</source>
        <translation>&amp;OK</translation>
    </message>
    <message>
        <location filename="qdjviewprefsdialog.ui" line="275"/>
        <source>Options for</source>
        <translation>Настройки для</translation>
    </message>
    <message>
        <location filename="qdjviewprefsdialog.ui" line="876"/>
        <source>Pass&amp;word: </source>
        <translation>&amp;Пароль: </translation>
    </message>
    <message>
        <location filename="qdjviewprefsdialog.ui" line="1031"/>
        <source>&amp;Pixel cache: </source>
        <translation>&amp;Пиксельный кэш: </translation>
    </message>
    <message>
        <location filename="qdjviewprefsdialog.ui" line="798"/>
        <source> pixels</source>
        <translation> пикселей</translation>
    </message>
    <message>
        <location filename="qdjviewprefsdialog.ui" line="906"/>
        <source>&amp;Port: </source>
        <translation>Пор&amp;т: </translation>
    </message>
    <message>
        <location filename="qdjviewprefs.cpp" line="706"/>
        <source>Preferences[*] - DjView</source>
        <translation>Настройки[*] - DjView</translation>
    </message>
    <message>
        <location filename="qdjviewprefsdialog.ui" line="864"/>
        <source>Proxy settings</source>
        <translation>Настройки прокси</translation>
    </message>
    <message>
        <location filename="qdjviewprefsdialog.ui" line="324"/>
        <source>&amp;Remember initial state from last invocation</source>
        <translation>&amp;Запоминать настройки, которые использовались в последний раз</translation>
    </message>
    <message>
        <location filename="qdjviewprefsdialog.ui" line="353"/>
        <source>Sc&amp;rollbars</source>
        <translation>&amp;Полосы прокрутки</translation>
    </message>
    <message>
        <location filename="qdjviewprefsdialog.ui" line="333"/>
        <source>Show</source>
        <translation>Показывать</translation>
    </message>
    <message>
        <location filename="qdjviewprefsdialog.ui" line="367"/>
        <source>&amp;Side bar</source>
        <translation>&amp;Боковая панель</translation>
    </message>
    <message>
        <location filename="qdjviewprefsdialog.ui" line="286"/>
        <source>Standalone Viewer</source>
        <translation>самостоятельного приложения</translation>
    </message>
    <message>
        <location filename="qdjviewprefsdialog.ui" line="360"/>
        <source>Stat&amp;us bar</source>
        <translation>Строка &amp;состояния</translation>
    </message>
    <message>
        <location filename="qdjviewprefsdialog.ui" line="346"/>
        <source>&amp;Tool bar</source>
        <translation>&amp;Панель инструментов</translation>
    </message>
    <message>
        <location filename="qdjviewprefsdialog.ui" line="854"/>
        <source>Use pro&amp;xy to access the network</source>
        <translation>Использовать &amp;прокси для доступа к сети</translation>
    </message>
    <message>
        <location filename="qdjviewprefsdialog.ui" line="886"/>
        <source>&amp;User: </source>
        <translation>&amp;Пользователь: </translation>
    </message>
    <message>
        <location filename="qdjviewprefsdialog.ui" line="782"/>
        <source>x</source>
        <translation>x</translation>
    </message>
    <message>
        <location filename="qdjviewprefsdialog.ui" line="385"/>
        <source>&amp;Zoom: </source>
        <translation>&amp;Масштаб: </translation>
    </message>
    <message>
        <location filename="qdjviewprefs.cpp" line="776"/>
        <source>&lt;html&gt;&lt;b&gt;Screen gamma correction.&lt;/b&gt;&lt;br&gt;The best color rendition is achieved by adjusting the gamma correction slider and choosing the position that makes the gray square as uniform as possible.&lt;p&gt;&lt;b&gt;Screen resolution.&lt;/b&gt;&lt;br&gt;This option forces a particular resolution instead of using the unreliable resolution advertised by the operating system. Forcing the resolution to 100 dpi matches the behavior of the djvulibre command line tools.&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;b&gt;Экранная гамма-коррекция.&lt;/b&gt;&lt;br&gt;Лучшая цветопередача достигается регулированием ползунка гамма-коррекции и выбором позиции, при которой серый квадрат кажется максимально однородным.&lt;p&gt;&lt;b&gt;Разрешение экрана.&lt;/b&gt;&lt;br&gt;Эта опция меняет разрешение на выбранное, опция полезна при неверном определении разрешения операционной системой. При выборе разрешения 100 dpi поведение совпадает с поведением утилит командной строки djvulibre.&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="qdjviewprefs.cpp" line="806"/>
        <source>&lt;html&gt;&lt;b&gt;Magnifying lens.&lt;/b&gt;&lt;br&gt;The magnifying lens appears when you depress the modifier keys specified in tab &lt;tt&gt;Keys&lt;/tt&gt;. This panel lets you choose the power and the size of the magnifying lens.&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;b&gt;Лупа.&lt;/b&gt;&lt;br&gt;Лупа появляется, когда Вы нажимаете комбинацию клавиш, определенную на вкладке &lt;tt&gt;Комбинации клавиш&lt;/tt&gt;. Эта панель позволяет выбрать размер и увеличение лупы.&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="qdjviewprefsdialog.ui" line="34"/>
        <source>&amp;Screen</source>
        <translation>&amp;Экран</translation>
    </message>
    <message>
        <location filename="qdjviewprefsdialog.ui" line="198"/>
        <source>Resolution</source>
        <translation>Разрешение</translation>
    </message>
    <message>
        <location filename="qdjviewprefsdialog.ui" line="204"/>
        <source>Force screen &amp;resolution</source>
        <translation>Переопределить &amp;разрешение экрана</translation>
    </message>
    <message>
        <location filename="qdjviewprefsdialog.ui" line="233"/>
        <source> dpi</source>
        <translation> dpi</translation>
    </message>
    <message>
        <location filename="qdjviewprefsdialog.ui" line="964"/>
        <source>&amp;Advanced</source>
        <translation>&amp;Продвинутые</translation>
    </message>
    <message>
        <location filename="qdjviewprefsdialog.ui" line="992"/>
        <location filename="qdjviewprefsdialog.ui" line="1005"/>
        <source> MB</source>
        <translation> Мб</translation>
    </message>
    <message>
        <location filename="qdjviewprefsdialog.ui" line="1044"/>
        <source>Miscellaneous</source>
        <translation>Разное</translation>
    </message>
    <message>
        <location filename="qdjviewprefsdialog.ui" line="1052"/>
        <source>Manual printer color correction</source>
        <translation>Настроить цветовую коррекцию принтера вручную</translation>
    </message>
    <message>
        <location filename="qdjviewprefsdialog.ui" line="1065"/>
        <source>gamma=</source>
        <translation>гамма=</translation>
    </message>
    <message>
        <location filename="qdjviewprefsdialog.ui" line="1102"/>
        <source>Enable advanced features in menus</source>
        <translation>Добавить в меню дополнительные возможности</translation>
    </message>
    <message>
        <location filename="qdjviewprefsdialog.ui" line="1109"/>
        <source>Show hidden text in status bar</source>
        <translation>Показывать скрытый текст в строке состояния</translation>
    </message>
    <message>
        <location filename="qdjviewprefsdialog.ui" line="495"/>
        <source>Layout</source>
        <translation>Вид </translation>
    </message>
    <message>
        <location filename="qdjviewprefsdialog.ui" line="501"/>
        <source>Continuous</source>
        <translation>Непрерывный</translation>
    </message>
    <message>
        <location filename="qdjviewprefsdialog.ui" line="553"/>
        <source>Side-by-side</source>
        <translation>Пары страниц</translation>
    </message>
    <message>
        <location filename="qdjviewprefsdialog.ui" line="583"/>
        <source>Cover Page</source>
        <translation>Титульный лист</translation>
    </message>
    <message>
        <location filename="qdjviewprefsdialog.ui" line="590"/>
        <source>Right-to-Left</source>
        <translation>Справа налево</translation>
    </message>
</context>
<context>
    <name>QDjViewPrintDialog</name>
    <message>
        <location filename="qdjviewdialogs.cpp" line="1737"/>
        <source>A file with this name already exists.
Do you want to replace it?</source>
        <translation>Файл с таким именем уже существует.
Заменить его?</translation>
    </message>
    <message>
        <location filename="qdjviewdialogs.cpp" line="1651"/>
        <source>All files</source>
        <comment>save filter</comment>
        <translation>Все файлы</translation>
    </message>
    <message>
        <location filename="qdjviewprintdialog.ui" line="280"/>
        <location filename="qdjviewdialogs.cpp" line="1740"/>
        <source>&amp;Cancel</source>
        <translation>&amp;Отмена</translation>
    </message>
    <message>
        <location filename="qdjviewprintdialog.ui" line="207"/>
        <source>Choose</source>
        <translation>Выбрать</translation>
    </message>
    <message>
        <location filename="qdjviewprintdialog.ui" line="63"/>
        <source>C&amp;urrent page</source>
        <translation>&amp;Текущую страницу</translation>
    </message>
    <message>
        <location filename="qdjviewprintdialog.ui" line="238"/>
        <source>&amp;Defaults</source>
        <translation>&amp;По умолчанию</translation>
    </message>
    <message>
        <location filename="qdjviewprintdialog.ui" line="122"/>
        <source>Destination</source>
        <translation>Результат</translation>
    </message>
    <message>
        <location filename="qdjviewprintdialog.ui" line="13"/>
        <source>Dialog</source>
        <translation>Диалог</translation>
    </message>
    <message>
        <location filename="qdjviewprintdialog.ui" line="53"/>
        <source>&amp;Document</source>
        <translation>&amp;Документ</translation>
    </message>
    <message>
        <location filename="qdjviewdialogs.cpp" line="1525"/>
        <source>&lt;html&gt;&lt;b&gt;Printing.&lt;/b&gt;&lt;br/&gt; You can print the whole document or a page range. Use the &lt;tt&gt;Choose&lt;/tt&gt; button to select a print destination and specify printer options. Additional dialog tabs might appear to specify conversion options.&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;b&gt;Печать.&lt;/b&gt;&lt;br/&gt; Вы можете напечатать весь документ или диапазон страниц. Используйте кнопку &lt;tt&gt;Выбрать&lt;/tt&gt;, чтобы выбрать и настроить принтер. Используйте другие вкладки для настройки конвертации.&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="qdjviewprintdialog.ui" line="264"/>
        <source>&amp;Ok</source>
        <translation>&amp;OK</translation>
    </message>
    <message>
        <location filename="qdjviewprintdialog.ui" line="78"/>
        <source>&amp;Pages</source>
        <translation>&amp;Страницы от</translation>
    </message>
    <message>
        <location filename="qdjviewprintdialog.ui" line="29"/>
        <location filename="qdjviewprintdialog.ui" line="41"/>
        <source>Print</source>
        <translation>Печатать</translation>
    </message>
    <message>
        <location filename="qdjviewprintdialog.ui" line="182"/>
        <source>Printer name: </source>
        <translation>Имя принтера:</translation>
    </message>
    <message>
        <location filename="qdjviewprintdialog.ui" line="134"/>
        <source>Print to file</source>
        <translation>Печать в файл</translation>
    </message>
    <message>
        <location filename="qdjviewdialogs.cpp" line="1658"/>
        <source>Print To File - DjView</source>
        <comment>dialog caption</comment>
        <translation>Печать в файл - DjView</translation>
    </message>
    <message>
        <location filename="qdjviewdialogs.cpp" line="1736"/>
        <source>Question - DjView</source>
        <comment>dialog caption</comment>
        <translation>Вопрос - DjView</translation>
    </message>
    <message>
        <location filename="qdjviewdialogs.cpp" line="1739"/>
        <source>&amp;Replace</source>
        <translation>&amp;Заменить</translation>
    </message>
    <message>
        <location filename="qdjviewprintdialog.ui" line="307"/>
        <source>Stop</source>
        <translation>Остановить</translation>
    </message>
    <message>
        <location filename="qdjviewdialogs.cpp" line="1783"/>
        <source>This operation has been interrupted.</source>
        <translation>Операция была прервана.</translation>
    </message>
    <message>
        <location filename="qdjviewdialogs.cpp" line="1779"/>
        <source>This operation has failed.</source>
        <translation>Не удалось выполнить операцию.</translation>
    </message>
    <message>
        <location filename="qdjviewprintdialog.ui" line="88"/>
        <source>to</source>
        <translation>до</translation>
    </message>
    <message>
        <location filename="qdjviewdialogs.cpp" line="1619"/>
        <source>(invalid printer)</source>
        <translation>(неверный принтер)</translation>
    </message>
</context>
<context>
    <name>QDjViewPrnExporter</name>
    <message>
        <location filename="qdjviewexporters.cpp" line="2266"/>
        <source>Cannot render page %1.</source>
        <translation>Невозможно преобразовать страницу %1.</translation>
    </message>
    <message>
        <location filename="qdjviewexporters.cpp" line="2014"/>
        <source>&lt;html&gt;&lt;b&gt;Printing options.&lt;/b&gt;&lt;br&gt;Option &lt;tt&gt;Color&lt;/tt&gt; enables color printing. Document pages can be decorated with a frame. Option &lt;tt&gt;Scale to fit&lt;/tt&gt; accommodates whatever paper size your printer uses. Zoom factor &lt;tt&gt;100%&lt;/tt&gt; reproduces the initial document size. Orientation &lt;tt&gt;Automatic&lt;/tt&gt; chooses portrait or landscape on a page per page basis.&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;b&gt;Настройки печати.&lt;/b&gt;&lt;br&gt;Опция &lt;tt&gt;Цвет&lt;/tt&gt; включает цветную печать. К страницам документа может быть добавлена рамка. Опция &lt;tt&gt;Масштабировать по размеру страницы&lt;/tt&gt; приспосабливает документ к размерам листов бумаги, используемой вашим принтером. Масштаб &lt;tt&gt;100%&lt;/tt&gt; воспроизводит исходный размер документа. Опция ориентации &lt;tt&gt;Автоматически&lt;/tt&gt; выбирает книжную или альбомну ориентацию по размерам каждой страницы.&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="qdjviewexporters.cpp" line="1991"/>
        <source>Printer data</source>
        <translation>Данные принтера</translation>
    </message>
    <message>
        <location filename="qdjviewexporters.cpp" line="2012"/>
        <source>Printing Options</source>
        <comment>tab caption</comment>
        <translation>Настройки печати</translation>
    </message>
    <message>
        <location filename="qdjviewexporters.cpp" line="1992"/>
        <source>PRN Files (*.prn)</source>
        <translation>Файлы PRN (*.prn)</translation>
    </message>
</context>
<context>
    <name>QDjViewSaveDialog</name>
    <message>
        <location filename="qdjviewdialogs.cpp" line="983"/>
        <source>A file with this name already exists.
Do you want to replace it?</source>
        <translation>Файл с таким именем уже существует.Заменить его?</translation>
    </message>
    <message>
        <location filename="qdjviewdialogs.cpp" line="1054"/>
        <source>All files</source>
        <comment>save filter</comment>
        <translation>Все файлы</translation>
    </message>
    <message>
        <location filename="qdjviewsavedialog.ui" line="142"/>
        <source>Bundled DjVu Document</source>
        <translation>Связанный документ DjVu (в одном файле)</translation>
    </message>
    <message>
        <location filename="qdjviewsavedialog.ui" line="222"/>
        <location filename="qdjviewdialogs.cpp" line="986"/>
        <source>&amp;Cancel</source>
        <translation>&amp;Отмена</translation>
    </message>
    <message>
        <location filename="qdjviewsavedialog.ui" line="47"/>
        <source>C&amp;urrent page</source>
        <translation>&amp;Текущую страницу</translation>
    </message>
    <message>
        <location filename="qdjviewsavedialog.ui" line="106"/>
        <source>Destination</source>
        <translation>Результат</translation>
    </message>
    <message>
        <location filename="qdjviewsavedialog.ui" line="13"/>
        <source>Dialog</source>
        <translation>Диалог</translation>
    </message>
    <message>
        <location filename="qdjviewsavedialog.ui" line="37"/>
        <source>&amp;Document</source>
        <translation>&amp;Документ</translation>
    </message>
    <message>
        <location filename="qdjviewdialogs.cpp" line="976"/>
        <source>Error - DjView</source>
        <comment>dialog caption</comment>
        <translation>Ошибка - DjView</translation>
    </message>
    <message>
        <location filename="qdjviewsavedialog.ui" line="134"/>
        <source>Format:</source>
        <translation>Формат:</translation>
    </message>
    <message>
        <location filename="qdjviewdialogs.cpp" line="887"/>
        <source>&lt;html&gt;&lt;b&gt;Saving.&lt;/b&gt;&lt;br/&gt; You can save the whole document or a page range. The bundled format creates a single file. The indirect format creates multiple files suitable for web serving.&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;b&gt;Сохранение.&lt;/b&gt;&lt;br/&gt; Вы можете сохранить весь документ или диапазон страниц.&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="qdjviewsavedialog.ui" line="147"/>
        <source>Indirect DjVu Document</source>
        <translation>Непрямой документ DjVu (страницы в отдельных файлах)</translation>
    </message>
    <message>
        <location filename="qdjviewsavedialog.ui" line="206"/>
        <source>&amp;Ok</source>
        <translation>&amp;OK</translation>
    </message>
    <message>
        <location filename="qdjviewdialogs.cpp" line="977"/>
        <source>Overwriting the current file is not allowed!</source>
        <translation>Замена текущего файла не позволяется!</translation>
    </message>
    <message>
        <location filename="qdjviewsavedialog.ui" line="62"/>
        <source>&amp;Pages</source>
        <translation>&amp;Страницы от</translation>
    </message>
    <message>
        <location filename="qdjviewdialogs.cpp" line="982"/>
        <source>Question - DjView</source>
        <comment>dialog caption</comment>
        <translation>Вопрос - DjView</translation>
    </message>
    <message>
        <location filename="qdjviewdialogs.cpp" line="985"/>
        <source>&amp;Replace</source>
        <translation>&amp;Заменить</translation>
    </message>
    <message>
        <location filename="qdjviewsavedialog.ui" line="25"/>
        <source>Save</source>
        <translation>Сохранить</translation>
    </message>
    <message>
        <location filename="qdjviewdialogs.cpp" line="1061"/>
        <source>Save - DjView</source>
        <comment>dialog caption</comment>
        <translation>Сохранить - DjView</translation>
    </message>
    <message>
        <location filename="qdjviewsavedialog.ui" line="249"/>
        <source>Stop</source>
        <translation>Остановить</translation>
    </message>
    <message>
        <location filename="qdjviewdialogs.cpp" line="1027"/>
        <source>This operation has been interrupted.</source>
        <translation>Операция была прервана.</translation>
    </message>
    <message>
        <location filename="qdjviewdialogs.cpp" line="1023"/>
        <source>This operation has failed.</source>
        <translation>Не удалось выполнить операцию.</translation>
    </message>
    <message>
        <location filename="qdjviewsavedialog.ui" line="72"/>
        <source>to</source>
        <translation>до</translation>
    </message>
</context>
<context>
    <name>QDjViewThumbnails</name>
    <message>
        <location filename="qdjviewsidebar.cpp" line="719"/>
        <source>Control Left Mouse Button</source>
        <translation>Control + левую кнопку мыши</translation>
    </message>
    <message>
        <location filename="qdjviewsidebar.cpp" line="723"/>
        <source>&lt;html&gt;&lt;b&gt;Document thumbnails.&lt;/b&gt;&lt;br/&gt; This panel display thumbnails for the document pages. Double click a thumbnail to jump to the selected page. %1 to change the thumbnail size or the refresh mode. The smart refresh mode only computes thumbnails when the page data is present (displayed or cached.)&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;b&gt;Макеты страниц документа.&lt;/b&gt;&lt;br/&gt; Эта панель отображает макеты страниц документа. Двойной щелчок перемещает к выбранной странице. Используйте %1, чтобы изменить размер макетов или режим обновления. В умном режиме обновления макеты показываются только для тех страниц, которые либо отображаются, либо кэшированы.&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="qdjviewsidebar.cpp" line="706"/>
        <source>Large</source>
        <comment>thumbnail menu</comment>
        <translation>Большие</translation>
    </message>
    <message>
        <location filename="qdjviewsidebar.cpp" line="701"/>
        <source>Medium</source>
        <comment>thumbnail menu</comment>
        <translation>Средние</translation>
    </message>
    <message>
        <location filename="qdjviewsidebar.cpp" line="721"/>
        <source>Right Mouse Button</source>
        <translation>правую кнопку мыши</translation>
    </message>
    <message>
        <location filename="qdjviewsidebar.cpp" line="696"/>
        <source>Small</source>
        <comment>thumbnail menu</comment>
        <translation>Маленькие</translation>
    </message>
    <message>
        <location filename="qdjviewsidebar.cpp" line="712"/>
        <source>Smart</source>
        <comment>thumbnail menu</comment>
        <translation>Умные</translation>
    </message>
    <message>
        <location filename="qdjviewsidebar.cpp" line="691"/>
        <source>Tiny</source>
        <comment>thumbnail menu</comment>
        <translation>Крошечные</translation>
    </message>
</context>
<context>
    <name>QDjViewTiffExporter</name>
    <message>
        <location filename="qdjviewexporters.cpp" line="1516"/>
        <source>Cannot open output file.</source>
        <translation>Не удается открыть выходной файл.</translation>
    </message>
    <message>
        <location filename="qdjviewexporters.cpp" line="1360"/>
        <source>&lt;html&gt;&lt;b&gt;TIFF options.&lt;/b&gt;&lt;br&gt;The resolution box specifies an upper limit for the resolution of the TIFF images. Forcing bitonal G4 compression encodes all pages in black and white using the CCITT Group 4 compression. Allowing JPEG compression uses lossy JPEG for all non bitonal or subsampled images. Otherwise, allowing deflate compression produces more compact (but less portable) files than the default packbits compression.&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;b&gt;Настройки TIFF.&lt;/b&gt;&lt;br&gt;Элемент &quot;Разрешение&quot; задает верхнюю границу разрешения изображений TIFF. При применении черно-белого сжатия G4 все страницы кодируются с помощью сжатия CCITT Group 4. Сжатие JPEG будет применяться ко всем цветным изображениям.&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="qdjviewexporters.cpp" line="1630"/>
        <source>Internal error.</source>
        <translation>Внутренняя ошибка.</translation>
    </message>
    <message>
        <location filename="qdjviewexporters.cpp" line="1628"/>
        <source>Out of memory.</source>
        <translation>Недостаточно памяти.</translation>
    </message>
    <message>
        <location filename="qdjviewexporters.cpp" line="1338"/>
        <source>TIFF Document</source>
        <translation>Документ TIFF</translation>
    </message>
    <message>
        <location filename="qdjviewexporters.cpp" line="1642"/>
        <source>TIFF export has not been compiled.</source>
        <translation>Экспорт в TIFF не был произведен.</translation>
    </message>
    <message>
        <location filename="qdjviewexporters.cpp" line="1339"/>
        <source>TIFF Files (*.tiff *.tif)</source>
        <translation>Файлы TIFF (*.tiff *.tif)</translation>
    </message>
    <message>
        <location filename="qdjviewexporters.cpp" line="1358"/>
        <source>TIFF Options</source>
        <comment>tab caption</comment>
        <translation>Настройки TIFF</translation>
    </message>
</context>
<context>
    <name>QDjVuHttpDocument</name>
    <message>
        <location filename="qdjvuhttp.cpp" line="274"/>
        <source>%1 while retrieving &apos;%2&apos;.</source>
        <translation>%1 при запросе &apos;%2&apos;.</translation>
    </message>
    <message>
        <location filename="qdjvuhttp.cpp" line="234"/>
        <source>Received %1 data while retrieving %2.</source>
        <comment>%1 is a mime type</comment>
        <translation>Получены данные %1 при запросе %2.</translation>
    </message>
    <message>
        <location filename="qdjvuhttp.cpp" line="240"/>
        <source>Received http status %1 while retrieving %2.</source>
        <comment>%1 is an http status code</comment>
        <translation>Получен статус http %1 при запросе %2.</translation>
    </message>
    <message>
        <location filename="qdjvuhttp.cpp" line="196"/>
        <source>Requesting &apos;%1&apos;</source>
        <translation>Запрашивается &apos;%1&apos;</translation>
    </message>
    <message>
        <location filename="qdjvuhttp.cpp" line="140"/>
        <source>Unsupported url scheme &apos;%1:&apos;.</source>
        <translation>Неподдерживаемая схема URL &apos;%1:&apos;.</translation>
    </message>
</context>
</TS>
