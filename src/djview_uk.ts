<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.0" language="uk">
<context>
    <name>QApplication</name>
    <message>
        <location filename="djview.cpp" line="415"/>
        <source>Option &apos;-fix&apos; is deprecated.</source>
        <translation>Параметр &apos;-fix&apos; є застарілим.</translation>
    </message>
</context>
<context>
    <name>QDjView</name>
    <message>
        <location filename="qdjview.cpp" line="140"/>
        <source>FitWidth</source>
        <comment>zoomCombo</comment>
        <translation>Підібрати ширину</translation>
    </message>
    <message>
        <location filename="qdjview.cpp" line="141"/>
        <source>FitPage</source>
        <comment>zoomCombo</comment>
        <translation>Підібрати висоту</translation>
    </message>
    <message>
        <location filename="qdjview.cpp" line="142"/>
        <source>Stretch</source>
        <comment>zoomCombo</comment>
        <translation>Розтягнути</translation>
    </message>
    <message>
        <location filename="qdjview.cpp" line="143"/>
        <source>1:1</source>
        <comment>zoomCombo</comment>
        <translation>1:1</translation>
    </message>
    <message>
        <location filename="qdjview.cpp" line="144"/>
        <source>300%</source>
        <comment>zoomCombo</comment>
        <translation>300%</translation>
    </message>
    <message>
        <location filename="qdjview.cpp" line="145"/>
        <source>200%</source>
        <comment>zoomCombo</comment>
        <translation>200%</translation>
    </message>
    <message>
        <location filename="qdjview.cpp" line="146"/>
        <source>150%</source>
        <comment>zoomCombo</comment>
        <translation>150%</translation>
    </message>
    <message>
        <location filename="qdjview.cpp" line="147"/>
        <source>100%</source>
        <comment>zoomCombo</comment>
        <translation>100%</translation>
    </message>
    <message>
        <location filename="qdjview.cpp" line="148"/>
        <source>75%</source>
        <comment>zoomCombo</comment>
        <translation>75%</translation>
    </message>
    <message>
        <location filename="qdjview.cpp" line="149"/>
        <source>50%</source>
        <comment>zoomCombo</comment>
        <translation>50%</translation>
    </message>
    <message>
        <location filename="qdjview.cpp" line="157"/>
        <source>Color</source>
        <comment>modeCombo</comment>
        <translation>Колір</translation>
    </message>
    <message>
        <location filename="qdjview.cpp" line="158"/>
        <source>Stencil</source>
        <comment>modeCombo</comment>
        <translation>Шаблон</translation>
    </message>
    <message>
        <location filename="qdjview.cpp" line="159"/>
        <source>Foreground</source>
        <comment>modeCombo</comment>
        <translation>Передній план</translation>
    </message>
    <message>
        <location filename="qdjview.cpp" line="160"/>
        <source>Background</source>
        <comment>modeCombo</comment>
        <translation>Тло</translation>
    </message>
    <message>
        <location filename="qdjview.cpp" line="367"/>
        <source>&amp;New</source>
        <comment>File|</comment>
        <translation>С&amp;творити</translation>
    </message>
    <message>
        <location filename="qdjview.cpp" line="368"/>
        <source>Ctrl+N</source>
        <comment>File|New</comment>
        <translation>Ctrl+N</translation>
    </message>
    <message>
        <location filename="qdjview.cpp" line="370"/>
        <source>Create a new DjView window.</source>
        <translation>Створити нове вікно DjView.</translation>
    </message>
    <message>
        <location filename="qdjview.cpp" line="373"/>
        <source>&amp;Open</source>
        <comment>File|</comment>
        <translation>&amp;Відкрити</translation>
    </message>
    <message>
        <location filename="qdjview.cpp" line="376"/>
        <source>Open a DjVu document.</source>
        <translation>Відкрити документ DjVu.</translation>
    </message>
    <message>
        <location filename="qdjview.cpp" line="387"/>
        <source>Close this window.</source>
        <translation>Закрити це вікно.</translation>
    </message>
    <message>
        <location filename="qdjview.cpp" line="393"/>
        <source>Close all windows and quit the application.</source>
        <translation>Закрити всі вікна та вийти з програми.</translation>
    </message>
    <message>
        <location filename="qdjview.cpp" line="399"/>
        <source>Save the DjVu document.</source>
        <translation>Зберегти документ DjVu.</translation>
    </message>
    <message>
        <location filename="qdjview.cpp" line="411"/>
        <source>Print the DjVu document.</source>
        <translation>Друкувати документ DjVu.</translation>
    </message>
    <message>
        <location filename="qdjview.cpp" line="417"/>
        <source>Find text in the document.</source>
        <translation>Знайти текст у документі.</translation>
    </message>
    <message>
        <location filename="qdjview.cpp" line="423"/>
        <source>Find next occurence of search text in the document.</source>
        <translation>Шукати наступне входження тексту до документа.</translation>
    </message>
    <message>
        <location filename="qdjview.cpp" line="428"/>
        <source>Find previous occurence of search text in the document.</source>
        <translation>Шукати попереднє входження тексту до документа.</translation>
    </message>
    <message>
        <location filename="qdjview.cpp" line="435"/>
        <source>Select a rectangle in the document.</source>
        <translation>Обрати прямокутну область у документі.</translation>
    </message>
    <message>
        <location filename="qdjview.cpp" line="440"/>
        <source>Increase the magnification.</source>
        <translation>Збільшити область перегляду.</translation>
    </message>
    <message>
        <location filename="qdjview.cpp" line="445"/>
        <source>Decrease the magnification.</source>
        <translation>Зменшити область перегляду.</translation>
    </message>
    <message>
        <location filename="qdjview.cpp" line="449"/>
        <source>Set magnification to fit page width.</source>
        <translation>Встановити масштаб так, щоб було видно всю висоту сторінки.</translation>
    </message>
    <message>
        <location filename="qdjview.cpp" line="455"/>
        <source>Set magnification to fit page.</source>
        <translation>Встановити масштаб так, щоб було видно всю ширину сторінки.</translation>
    </message>
    <message>
        <location filename="qdjview.cpp" line="461"/>
        <source>Set full resolution magnification.</source>
        <translation>Встановити відображення у натуральному розмірі.</translation>
    </message>
    <message>
        <location filename="qdjview.cpp" line="467"/>
        <source>Magnify 300%</source>
        <translation>Масштаб у 300%</translation>
    </message>
    <message>
        <location filename="qdjview.cpp" line="473"/>
        <source>Magnify 20%</source>
        <translation>Масштаб у 200%</translation>
    </message>
    <message>
        <location filename="qdjview.cpp" line="479"/>
        <source>Magnify 150%</source>
        <translation>Масштаб у 150%</translation>
    </message>
    <message>
        <location filename="qdjview.cpp" line="485"/>
        <source>Magnify 100%</source>
        <translation>Масштаб у 100%</translation>
    </message>
    <message>
        <location filename="qdjview.cpp" line="491"/>
        <source>Magnify 75%</source>
        <translation>Масштаб у 75%</translation>
    </message>
    <message>
        <location filename="qdjview.cpp" line="497"/>
        <source>Magnify 50%</source>
        <translation>Масштаб у 50%</translation>
    </message>
    <message>
        <location filename="qdjview.cpp" line="504"/>
        <source>Jump to first document page.</source>
        <translation>Перейти до першої сторінки документу.</translation>
    </message>
    <message>
        <location filename="qdjview.cpp" line="510"/>
        <source>Jump to next document page.</source>
        <translation>Перейти до наступної сторінки документу.</translation>
    </message>
    <message>
        <location filename="qdjview.cpp" line="516"/>
        <source>Jump to previous document page.</source>
        <translation>Перейти до попередньої сторінки документу.</translation>
    </message>
    <message>
        <location filename="qdjview.cpp" line="522"/>
        <source>Jump to last document page.</source>
        <translation>Перейти до останньої сторінки документу.</translation>
    </message>
    <message>
        <location filename="qdjview.cpp" line="528"/>
        <source>Backward in history.</source>
        <translation>Назад за журналом.</translation>
    </message>
    <message>
        <location filename="qdjview.cpp" line="534"/>
        <source>Forward in history.</source>
        <translation>Вперед за журналом.</translation>
    </message>
    <message>
        <location filename="qdjview.cpp" line="540"/>
        <source>Rotate page image counter-clockwise.</source>
        <translation>Обертання картинки сторінки проти годинникової стрілки.</translation>
    </message>
    <message>
        <location filename="qdjview.cpp" line="546"/>
        <source>Rotate page image clockwise.</source>
        <translation>Обертання картинки сторінки за годинниковою стрілкою.</translation>
    </message>
    <message>
        <location filename="qdjview.cpp" line="551"/>
        <source>Set natural page orientation.</source>
        <translation>Зберегти початкову орієнтацію сторінки.</translation>
    </message>
    <message>
        <location filename="qdjview.cpp" line="557"/>
        <source>Turn page on its left side.</source>
        <translation>Повернути сторінку на її лівий бік.</translation>
    </message>
    <message>
        <location filename="qdjview.cpp" line="563"/>
        <source>Turn page upside-down.</source>
        <translation>Перевернути сторінку догори низом.</translation>
    </message>
    <message>
        <location filename="qdjview.cpp" line="569"/>
        <source>Turn page on its right side.</source>
        <translation>Повернути сторінку на її правий бік.</translation>
    </message>
    <message>
        <location filename="qdjview.cpp" line="576"/>
        <source>Show information about the document encoding and structure.</source>
        <translation>Показати інформацію про кодування документу та його структуру.</translation>
    </message>
    <message>
        <location filename="qdjview.cpp" line="583"/>
        <source>Show the document and page meta data.</source>
        <translation>Показати метадані документу та сторінки.</translation>
    </message>
    <message>
        <location filename="qdjview.cpp" line="588"/>
        <source>&amp;About DjView...</source>
        <translation>&amp;Про DjView...</translation>
    </message>
    <message>
        <location filename="qdjview.cpp" line="592"/>
        <source>Show information about this program.</source>
        <translation>Показати інформацію про цю програму.</translation>
    </message>
    <message>
        <location filename="qdjview.cpp" line="596"/>
        <source>Display everything.</source>
        <translation>Показувати все.</translation>
    </message>
    <message>
        <location filename="qdjview.cpp" line="602"/>
        <source>Only display the document bitonal stencil.</source>
        <translation>Показати тільки двокольоровий шаблон документу.</translation>
    </message>
    <message>
        <location filename="qdjview.cpp" line="609"/>
        <source>Only display the foreground layer.</source>
        <translation>Показувати лише передній план.</translation>
    </message>
    <message>
        <location filename="qdjview.cpp" line="616"/>
        <source>Only display the background layer.</source>
        <translation>Показувати лише задній план.</translation>
    </message>
    <message>
        <location filename="qdjview.cpp" line="630"/>
        <source>Show the preferences dialog.</source>
        <translation>Показати діалог налаштування.</translation>
    </message>
    <message>
        <location filename="qdjview.cpp" line="636"/>
        <source>Show/hide the side bar.</source>
        <translation>Показати або сховати бічний пенал.</translation>
    </message>
    <message>
        <location filename="qdjview.cpp" line="643"/>
        <source>Show/hide the standard tool bar.</source>
        <translation>Показати або сховати панель інструментів.</translation>
    </message>
    <message>
        <location filename="qdjview.cpp" line="647"/>
        <source>Show/hide the status bar.</source>
        <translation>Показати або сховати панель стану.</translation>
    </message>
    <message>
        <location filename="qdjview.cpp" line="656"/>
        <source>Toggle full screen mode.</source>
        <translation>Перемикач повноекранного режиму.</translation>
    </message>
    <message>
        <location filename="qdjview.cpp" line="663"/>
        <source>Toggle continuous layout mode.</source>
        <translation>Перемикач режиму неперервного перегляду сторінок.</translation>
    </message>
    <message>
        <location filename="qdjview.cpp" line="671"/>
        <source>Toggle side-by-side layout mode.</source>
        <translation>Перемикач режиму відображення сторінок поруч.</translation>
    </message>
    <message>
        <location filename="qdjview.cpp" line="1031"/>
        <source>Control Left Mouse Button</source>
        <translation>Контроль правою кнопкою миші</translation>
    </message>
    <message>
        <location filename="qdjview.cpp" line="1033"/>
        <source>Right Mouse Button</source>
        <translation>Права кнопка миші</translation>
    </message>
    <message>
        <location filename="qdjview.cpp" line="1040"/>
        <source>&lt;html&gt;&lt;b&gt;Selecting a rectangle.&lt;/b&gt;&lt;br/&gt; Once a rectangular area is selected, a popup menu lets you copy the corresponding text or image. Instead of using this tool, you can also hold %1 and use the Left Mouse Button.&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;b&gt;Вибір прямокутної області.&lt;/b&gt;&lt;br/&gt; Тільки-но обрано прямокутну область, спадне меню надасть вам змогу скопіювати відповідний текст чи картинку. Замість використання цього інструменту Ви можете натиснути %1 та використати ліву кнопку миші.&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="qdjview.cpp" line="1048"/>
        <source>&lt;html&gt;&lt;b&gt;Zooming.&lt;/b&gt;&lt;br/&gt; Choose a zoom level for viewing the document. Zoom level 100% displays the document for a 100 dpi screen. Zoom levels &lt;tt&gt;Fit Page&lt;/tt&gt; and &lt;tt&gt;Fit Width&lt;/tt&gt; ensure that the full page or the page width fit in the window. &lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;b&gt;Масштабування.&lt;/b&gt;&lt;br/&gt; Оберіть масштаб для перегляду документа. Масштаб у 100% покаже документ у роздільній здатності екрана 100 т/д. Масштаб &lt;tt&gt;Підібрати під сторінку&lt;/tt&gt; та &lt;tt&gt;Підібрати ширину&lt;/tt&gt; надасть змогу бачити на екрані всю сторінку або всю ширину сторінки. &lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="qdjview.cpp" line="1060"/>
        <source>&lt;html&gt;&lt;b&gt;Rotating the pages.&lt;/b&gt;&lt;br/&gt; Choose to display pages in portrait or landscape mode. You can also turn them upside down.&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;b&gt;Поворот сторінок.&lt;/b&gt;&lt;br/&gt; Оберіть, щоб переглядати сторінки у книжковому чи альбомному режимі. Ви також можете перевернути їх догори низом.&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="qdjview.cpp" line="1067"/>
        <source>&lt;html&gt;&lt;b&gt;Display mode.&lt;/b&gt;&lt;br/&gt; DjVu images compose a background layer and a foreground layer using a stencil. The display mode specifies with layers should be displayed.&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;b&gt;Режим відображення.&lt;/b&gt;&lt;br/&gt; Картинки DjVu складаються з шару тла та шару переднього плану за допомогою шаблону. Режим відображення визначає шари, що буде відображено.&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="qdjview.cpp" line="1075"/>
        <source>&lt;html&gt;&lt;b&gt;Navigating the document.&lt;/b&gt;&lt;br/&gt; The page selector lets you jump to any page by name. The navigation buttons jump to the first page, the previous page, the next page, or the last page. &lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;b&gt;Навігація документом.&lt;/b&gt;&lt;br/&gt; Вибір сторінки дозволить Вам перейти до будь-якої сторінки за назвою. Кнопки навігації дозволять перейти до першої, попередньої, наступної, останньої сторінки. &lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="qdjview.cpp" line="1083"/>
        <source>&lt;html&gt;&lt;b&gt;Document and page information.&lt;/b&gt;&lt;br&gt; Display a dialog window for viewing encoding information pertaining to the document or to a specific page.&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;b&gt;Інформація про документ та сторінку.&lt;/b&gt;&lt;br&gt; Показує вікно діалогу для перегляду інформації про кодування, що стосується документу або певної сторінки.&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="qdjview.cpp" line="1089"/>
        <source>&lt;html&gt;&lt;b&gt;Document and page metadata.&lt;/b&gt;&lt;br&gt; Display a dialog window for viewing metadata pertaining to the document or to a specific page.&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;b&gt;Метадані документу та сторінки.&lt;/b&gt;&lt;br&gt; Показує вікно діалогу для перегляду метаданих, що стосуються документу або певної сторінки.&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="qdjview.cpp" line="1095"/>
        <source>&lt;html&gt;&lt;b&gt;Continuous layout.&lt;/b&gt;&lt;br/&gt; Display all the document pages arranged vertically inside the scrollable document viewing area.&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;b&gt;Неперервний показ.&lt;/b&gt;&lt;br/&gt; Показує всі сторінки документу розташовані вертикально всередині області перегляду, придатної для гортання.&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="qdjview.cpp" line="1100"/>
        <source>&lt;html&gt;&lt;b&gt;Side by side layout.&lt;/b&gt;&lt;br/&gt; Display pairs of pages side by side inside the scrollable document viewing area.&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;b&gt;Розташування поруч.&lt;/b&gt;&lt;br/&gt; Показує пари сторінок поруч одна з одною всередині області перегляду документа, придатної для гортання.&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="qdjview.cpp" line="1105"/>
        <source>&lt;html&gt;&lt;b&gt;Page information.&lt;/b&gt;&lt;br/&gt; Display information about the page located under the cursor: the sequential page number, the page size in pixels, and the page resolution in dots per inch. &lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;b&gt;Інформація про сторінку.&lt;/b&gt;&lt;br/&gt; Показує інформацію про сторінку розташовану під вказівником: номер сторінки по порядку, розмір сторінки у точках та роздільну здатність сторінки у точках на дюйм. &lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="qdjview.cpp" line="1111"/>
        <source>&lt;html&gt;&lt;b&gt;Cursor information.&lt;/b&gt;&lt;br/&gt; Display the position of the mouse cursor expressed in page coordinates. &lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;b&gt;Інформація про вказівник.&lt;/b&gt;&lt;br/&gt; Показує позицію вказівника миші виражену у координатах сторінки. &lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="qdjview.cpp" line="1116"/>
        <source>&lt;html&gt;&lt;b&gt;Document viewing area.&lt;/b&gt;&lt;br/&gt; This is the main display area for the DjVu document. &lt;ul&gt;&lt;li&gt;Arrows and page keys to navigate the document.&lt;/li&gt;&lt;li&gt;Space and BackSpace to read the document.&lt;/li&gt;&lt;li&gt;Keys &lt;tt&gt;+&lt;/tt&gt; &lt;tt&gt;-&lt;/tt&gt; &lt;tt&gt;[&lt;/tt&gt; &lt;tt&gt;]&lt;/tt&gt; to zoom or rotate the document.&lt;/li&gt;&lt;li&gt;Left Mouse Button for panning and selecting links.&lt;/li&gt;&lt;li&gt;%3 for displaying the contextual menu.&lt;/li&gt;&lt;li&gt;%1 Left Mouse Button for selecting text or images.&lt;/li&gt;&lt;li&gt;%2 for popping the magnification lens.&lt;/li&gt;&lt;/ul&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;b&gt;Область перегляду документа.&lt;/b&gt;&lt;br/&gt; Це головна область відображення документа DjVu. &lt;ul&gt;&lt;li&gt;Стрілки та клавіші переміщення сторінки може бути використано для навігації документом.&lt;/li&gt;&lt;li&gt;Пробіл та BackSpace - для читання документа.&lt;/li&gt;&lt;li&gt;Клавіші &lt;tt&gt;+&lt;/tt&gt; &lt;tt&gt;-&lt;/tt&gt; &lt;tt&gt;[&lt;/tt&gt; &lt;tt&gt;]&lt;/tt&gt; - для масштабування та повороту документа.&lt;/li&gt;&lt;li&gt;Ліва кнопка миші - для панорамування та вибору посилань.&lt;/li&gt;&lt;li&gt;%3 - для відображення контекстного меню.&lt;/li&gt;&lt;li&gt;%1 ліва кнопка миші - для вибору тексту або картинок.&lt;/li&gt;&lt;li&gt;%2 - для виклику збільшувальної лінзи.&lt;/li&gt;&lt;/ul&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="qdjview.cpp" line="1129"/>
        <source>&lt;html&gt;&lt;b&gt;Document viewing area.&lt;/b&gt;&lt;br/&gt; This is the main display area for the DjVu document. But you must first open a DjVu document to see anything.&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;b&gt;Область перегляду документа.&lt;/b&gt;&lt;br/&gt; Це головна область перегляду для документа DjVu. Але перед цим Вам, звичайно, слід відкрити документ DjVu, щоб щось побачити.&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="qdjview.cpp" line="1438"/>
        <source>Option &apos;%1&apos; requires boolean argument.</source>
        <translation>Параметр &apos;%1&apos; потребує булівського аргументу.</translation>
    </message>
    <message>
        <location filename="qdjview.cpp" line="1445"/>
        <source>Illegal value &apos;%2&apos; for option &apos;%1&apos;.</source>
        <translation>Некоректне значення &apos;%2&apos; для параметра&apos;%1&apos;.</translation>
    </message>
    <message>
        <location filename="qdjview.cpp" line="1563"/>
        <source>Toolbar option &apos;%1&apos; is not implemented.</source>
        <translation>Параметр панелі інструментів &apos;%1&apos; не може бути застосований.</translation>
    </message>
    <message>
        <location filename="qdjview.cpp" line="1576"/>
        <source>Toolbar option &apos;%1&apos; is not recognized.</source>
        <translation>Параметр панелі інструментів &apos;%1&apos; неможливо розпізнати.</translation>
    </message>
    <message>
        <location filename="qdjview.cpp" line="1622"/>
        <source>Option &apos;%1&apos; requires a standalone viewer.</source>
        <translation>Параметр &apos;%1&apos; потребує самодостатнього переглядача.</translation>
    </message>
    <message>
        <location filename="qdjview.cpp" line="1875"/>
        <source>Deprecated option &apos;%1&apos;</source>
        <translation>Небажаний параметр &apos;%1&apos;</translation>
    </message>
    <message>
        <location filename="qdjview.cpp" line="1921"/>
        <source>Option &apos;%1&apos; is not implemented.</source>
        <translation>Параметр &apos;%1&apos; не може бути застосований.</translation>
    </message>
    <message>
        <location filename="qdjview.cpp" line="1926"/>
        <source>Option &apos;%1&apos; is not recognized.</source>
        <translation>Параметр &apos;%1&apos; неможливо розпізнати.</translation>
    </message>
    <message>
        <location filename="qdjview.cpp" line="2196"/>
        <location filename="qdjview.cpp" line="2463"/>
        <source>DjView</source>
        <translation>DjView</translation>
    </message>
    <message>
        <location filename="qdjview.cpp" line="2483"/>
        <source>Cannot open file &apos;%1&apos;.</source>
        <translation>Неможливо відкрити файл &apos;%1&apos;.</translation>
    </message>
    <message>
        <location filename="qdjview.cpp" line="2484"/>
        <source>Opening DjVu file</source>
        <translation>Відкриття файла DjVu</translation>
    </message>
    <message>
        <location filename="qdjview.cpp" line="2522"/>
        <source>Cannot open URL &apos;%1&apos;.</source>
        <translation>Неможливо відкрити URL &apos;%1&apos;.</translation>
    </message>
    <message>
        <location filename="qdjview.cpp" line="2523"/>
        <source>Opening DjVu document</source>
        <translation>Відкриття документу DjVu</translation>
    </message>
    <message>
        <location filename="qdjview.cpp" line="2550"/>
        <source>Cannot find page numbered: %1</source>
        <translation>Не вдалося знайти сторінку з номером: %1</translation>
    </message>
    <message>
        <location filename="qdjview.cpp" line="2582"/>
        <location filename="qdjview.cpp" line="2612"/>
        <source>Cannot find page named: %1</source>
        <translation>Не вдалося знайти сторінку з назвою: %1</translation>
    </message>
    <message>
        <location filename="qdjview.cpp" line="2825"/>
        <source>Unrecognized sidebar options &apos;%1&apos;.</source>
        <translation>Нерозпізнаний параметри бічної панелі &apos;%1&apos;.</translation>
    </message>
    <message>
        <location filename="qdjview.cpp" line="3203"/>
        <location filename="qdjview.cpp" line="3268"/>
        <source>Cannot write file &apos;%1&apos;.
%2.</source>
        <translation>Файл &apos;%1&apos; неможливо записати.
%2.</translation>
    </message>
    <message>
        <location filename="qdjview.cpp" line="3248"/>
        <source>Cannot determine file format.
Filename &apos;%1&apos; has no suffix.</source>
        <translation>Неможливо визначити формат файла.
Назва файла &apos;%1&apos; не має суфіксу.</translation>
    </message>
    <message>
        <location filename="qdjview.cpp" line="3261"/>
        <source>Image format %1 not supported.</source>
        <translation>Формат картинки %1 не підтримується.</translation>
    </message>
    <message>
        <location filename="qdjview.cpp" line="3484"/>
        <source>Cannot decode page %1.</source>
        <translation>Неможливо розкодувати сторінку %1.</translation>
    </message>
    <message>
        <location filename="qdjview.cpp" line="3486"/>
        <source>Cannot decode document.</source>
        <translation>Неможливо розкодувати документ.</translation>
    </message>
    <message>
        <location filename="qdjview.cpp" line="3488"/>
        <source>Decoding DjVu document</source>
        <translation>Розкодування документу DjVu</translation>
    </message>
    <message>
        <source> P%1 %2x%3 %4dpi </source>
        <translation type="obsolete"> С%1 %2x%3 %4т/д </translation>
    </message>
    <message>
        <location filename="qdjview.cpp" line="3625"/>
        <source> x=%1 y=%2 </source>
        <translation> x=%1 y=%2 </translation>
    </message>
    <message numerus="yes">
        <location filename="qdjview.cpp" line="3702"/>
        <source>Go: %n pages forward.</source>
        <translation>
            <numerusform>Перехід на %n сторінку вперед.</numerusform>
            <numerusform>Перехід на %n сторінки вперед.</numerusform>
            <numerusform>Перехід на %n сторінок вперед.</numerusform>
        </translation>
    </message>
    <message numerus="yes">
        <location filename="qdjview.cpp" line="3705"/>
        <source>Go: %n pages backward.</source>
        <translation>
            <numerusform>Перехід на %n сторінку назад.</numerusform>
            <numerusform>Перехід на %n сторінки назад.</numerusform>
            <numerusform>Перехід на %n сторінок назад.</numerusform>
        </translation>
    </message>
    <message>
        <location filename="qdjview.cpp" line="3708"/>
        <location filename="qdjview.cpp" line="3710"/>
        <source>Go: page %1.</source>
        <translation>Перехід: сторінка %1.</translation>
    </message>
    <message>
        <location filename="qdjview.cpp" line="3712"/>
        <source>Link: %1</source>
        <translation>Посилання: %1</translation>
    </message>
    <message>
        <location filename="qdjview.cpp" line="3714"/>
        <source> (in other window.)</source>
        <translation> (у іншому вікні.)</translation>
    </message>
    <message>
        <location filename="qdjview.cpp" line="3790"/>
        <source>Cannot resolve link &apos;%1&apos;</source>
        <translation>Неможливо розпізнати посилання &apos;%1&apos;</translation>
    </message>
    <message>
        <location filename="qdjview.cpp" line="3818"/>
        <source>Cannot spawn a browser for url &apos;%1&apos;</source>
        <translation>Неможливо викликати переглядач для url &apos;%1&apos;</translation>
    </message>
    <message>
        <location filename="qdjview.cpp" line="3838"/>
        <source>Copy text (%1)</source>
        <translation>Копіювання тексту (%1)</translation>
    </message>
    <message>
        <location filename="qdjview.cpp" line="3839"/>
        <source>Save text as...</source>
        <translation>Збереження тексту як...</translation>
    </message>
    <message>
        <location filename="qdjview.cpp" line="3845"/>
        <source>Copy image (%1x%2 pixels)</source>
        <translation>Копіювання картинки (%1x%2 пікселів)</translation>
    </message>
    <message>
        <location filename="qdjview.cpp" line="3847"/>
        <source>Save image as...</source>
        <translation>Збереження картинки як...</translation>
    </message>
    <message>
        <location filename="qdjview.cpp" line="3851"/>
        <source>Zoom to rectangle</source>
        <translation>Масштабувати до прямокутника</translation>
    </message>
    <message>
        <location filename="qdjview.cpp" line="4018"/>
        <source>&lt;html&gt;&lt;h2&gt;DjVuLibre DjView %1&lt;/h2&gt;%2&lt;p&gt;Viewer for DjVu documents&lt;br&gt;&lt;a href=http://djvulibre.djvuzone.org&gt;http://djvulibre.djvuzone.org&lt;/a&gt;&lt;br&gt;Copyright © 2006-- Léon Bottou.&lt;/p&gt;&lt;p align=justify&gt;&lt;small&gt;This program is free software. You can redistribute or modify it under the terms of the GNU General Public License as published by the Free Software Foundation. This program is distributed &lt;i&gt;without any warranty&lt;/i&gt;. See the GNU General Public License for more details.&lt;/small&gt;&lt;/p&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;h2&gt;DjVuLibre DjView %1&lt;/h2&gt;%2&lt;p&gt;Переглядач документів DjVu&lt;br&gt;&lt;a href=http://djvulibre.djvuzone.org&gt;http://djvulibre.djvuzone.org&lt;/a&gt;&lt;br&gt;Авторські права© 2006 — Léon Bottou.&lt;/p&gt;&lt;p align=justify&gt;&lt;small&gt;Ця програма є вільним програмним забезпеченням. Ви можете поширювати або змінювати її за умов дотримання GNU General Public License у тому вигляді, у якому її оприлюднено Free Software Foundation. Ця програма поширюється &lt;i&gt;без жодних гарантій&lt;/i&gt;. Щоб дізнатися більше, прочитайте GNU General Public License.&lt;/small&gt;&lt;/p&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="qdjview.cpp" line="4038"/>
        <source>About DjView</source>
        <translation>Про DjView</translation>
    </message>
    <message>
        <location filename="qdjview.cpp" line="4058"/>
        <source>DjVu files</source>
        <translation>Файли DjVu</translation>
    </message>
    <message>
        <location filename="djview.cpp" line="435"/>
        <source>cannot open &apos;%1&apos;.</source>
        <translation>неможливо відкрити &apos;%1&apos;.</translation>
    </message>
    <message>
        <location filename="qdjview.cpp" line="402"/>
        <source>&amp;Export as...</source>
        <comment>File|</comment>
        <translation>&amp;Експортувати як...</translation>
    </message>
    <message>
        <location filename="qdjview.cpp" line="405"/>
        <source>Export DjVu page or document to other formats.</source>
        <translation>Експортувати сторінку або документ DjVu у інші формати.</translation>
    </message>
    <message>
        <location filename="qdjview.cpp" line="2919"/>
        <source>Export - DjView</source>
        <comment>dialog caption</comment>
        <translation>Експорт - DjView</translation>
    </message>
    <message>
        <location filename="qdjview.cpp" line="403"/>
        <source>Ctrl+E</source>
        <comment>File|ExportAs</comment>
        <translation>Ctrl+E</translation>
    </message>
    <message>
        <location filename="qdjview.cpp" line="374"/>
        <source>Ctrl+O</source>
        <comment>File|Open</comment>
        <translation>Ctrl+O</translation>
    </message>
    <message>
        <location filename="qdjview.cpp" line="384"/>
        <source>&amp;Close</source>
        <comment>File|</comment>
        <translation>&amp;Закрити</translation>
    </message>
    <message>
        <location filename="qdjview.cpp" line="385"/>
        <source>Ctrl+W</source>
        <comment>File|Close</comment>
        <translation>Ctrl+W</translation>
    </message>
    <message>
        <location filename="qdjview.cpp" line="390"/>
        <source>&amp;Quit</source>
        <comment>File|</comment>
        <translation>Ви&amp;йти</translation>
    </message>
    <message>
        <location filename="qdjview.cpp" line="391"/>
        <source>Ctrl+Q</source>
        <comment>File|Quit</comment>
        <translation>Ctrl+Q</translation>
    </message>
    <message>
        <location filename="qdjview.cpp" line="396"/>
        <source>Save &amp;as...</source>
        <comment>File|</comment>
        <translation>Зберегти &amp;як...</translation>
    </message>
    <message>
        <location filename="qdjview.cpp" line="397"/>
        <source>Ctrl+S</source>
        <comment>File|SaveAs</comment>
        <translation>Ctrl+S</translation>
    </message>
    <message>
        <location filename="qdjview.cpp" line="408"/>
        <source>&amp;Print...</source>
        <comment>File|</comment>
        <translation>&amp;Друкувати...</translation>
    </message>
    <message>
        <location filename="qdjview.cpp" line="409"/>
        <source>Ctrl+P</source>
        <comment>File|Print</comment>
        <translation>Ctrl+P</translation>
    </message>
    <message>
        <location filename="qdjview.cpp" line="414"/>
        <source>&amp;Find...</source>
        <comment>Edit|</comment>
        <translation>Шук&amp;ати...</translation>
    </message>
    <message>
        <location filename="qdjview.cpp" line="415"/>
        <source>Ctrl+F</source>
        <comment>Edit|Find</comment>
        <translation>Ctrl+F</translation>
    </message>
    <message>
        <location filename="qdjview.cpp" line="420"/>
        <source>Find &amp;Next</source>
        <comment>Edit|</comment>
        <translation>Шукати &amp;наступне</translation>
    </message>
    <message>
        <location filename="qdjview.cpp" line="422"/>
        <source>F3</source>
        <comment>Edit|Find Next</comment>
        <translation>F3</translation>
    </message>
    <message>
        <location filename="qdjview.cpp" line="426"/>
        <source>Find &amp;Previous</source>
        <comment>Edit|</comment>
        <translation>Шукати &amp;попереднє</translation>
    </message>
    <message>
        <location filename="qdjview.cpp" line="427"/>
        <source>Shift+F3</source>
        <comment>Edit|Find Previous</comment>
        <translation>Shift+F3</translation>
    </message>
    <message>
        <location filename="qdjview.cpp" line="431"/>
        <source>&amp;Select</source>
        <comment>Edit|</comment>
        <translation>&amp;Обрати</translation>
    </message>
    <message>
        <location filename="qdjview.cpp" line="433"/>
        <source>F2</source>
        <comment>Edit|Select</comment>
        <translation>F2</translation>
    </message>
    <message>
        <location filename="qdjview.cpp" line="438"/>
        <source>Zoom &amp;In</source>
        <comment>Zoom|</comment>
        <translation>З&amp;більшити</translation>
    </message>
    <message>
        <location filename="qdjview.cpp" line="443"/>
        <source>Zoom &amp;Out</source>
        <comment>Zoom|</comment>
        <translation>З&amp;меншити</translation>
    </message>
    <message>
        <location filename="qdjview.cpp" line="448"/>
        <source>Fit &amp;Width</source>
        <comment>Zoom|</comment>
        <translation>Підібрати за &amp;шириною</translation>
    </message>
    <message>
        <location filename="qdjview.cpp" line="454"/>
        <source>Fit &amp;Page</source>
        <comment>Zoom|</comment>
        <translation>Підібрати за &amp;висотою</translation>
    </message>
    <message>
        <location filename="qdjview.cpp" line="460"/>
        <source>One &amp;to one</source>
        <comment>Zoom|</comment>
        <translation>Один до &amp;одного</translation>
    </message>
    <message>
        <location filename="qdjview.cpp" line="466"/>
        <source>&amp;300%</source>
        <comment>Zoom|</comment>
        <translation>&amp;300%</translation>
    </message>
    <message>
        <location filename="qdjview.cpp" line="472"/>
        <source>&amp;200%</source>
        <comment>Zoom|</comment>
        <translation>&amp;200%</translation>
    </message>
    <message>
        <location filename="qdjview.cpp" line="478"/>
        <source>150%</source>
        <comment>Zoom|</comment>
        <translation>150%</translation>
    </message>
    <message>
        <location filename="qdjview.cpp" line="484"/>
        <source>&amp;100%</source>
        <comment>Zoom|</comment>
        <translation>&amp;100%</translation>
    </message>
    <message>
        <location filename="qdjview.cpp" line="490"/>
        <source>&amp;75%</source>
        <comment>Zoom|</comment>
        <translation>&amp;75%</translation>
    </message>
    <message>
        <location filename="qdjview.cpp" line="496"/>
        <source>&amp;50%</source>
        <comment>Zoom|</comment>
        <translation>&amp;50%</translation>
    </message>
    <message>
        <location filename="qdjview.cpp" line="502"/>
        <source>&amp;First Page</source>
        <comment>Go|</comment>
        <translation>&amp;Перша сторінка</translation>
    </message>
    <message>
        <location filename="qdjview.cpp" line="508"/>
        <source>&amp;Next Page</source>
        <comment>Go|</comment>
        <translation>&amp;Наступна сторінка</translation>
    </message>
    <message>
        <location filename="qdjview.cpp" line="514"/>
        <source>&amp;Previous Page</source>
        <comment>Go|</comment>
        <translation>&amp;Попередня сторінка</translation>
    </message>
    <message>
        <location filename="qdjview.cpp" line="520"/>
        <source>&amp;Last Page</source>
        <comment>Go|</comment>
        <translation>&amp;Остання сторінка</translation>
    </message>
    <message>
        <location filename="qdjview.cpp" line="526"/>
        <source>&amp;Backward</source>
        <comment>Go|</comment>
        <translation>&amp;Назад</translation>
    </message>
    <message>
        <location filename="qdjview.cpp" line="532"/>
        <source>&amp;Forward</source>
        <comment>Go|</comment>
        <translation>&amp;Вперед</translation>
    </message>
    <message>
        <location filename="qdjview.cpp" line="538"/>
        <source>Rotate &amp;Left</source>
        <comment>Rotate|</comment>
        <translation>Повернути &amp;ліворуч</translation>
    </message>
    <message>
        <location filename="qdjview.cpp" line="544"/>
        <source>Rotate &amp;Right</source>
        <comment>Rotate|</comment>
        <translation>Повернути &amp;праворуч</translation>
    </message>
    <message>
        <location filename="qdjview.cpp" line="550"/>
        <source>Rotate &amp;0°</source>
        <comment>Rotate|</comment>
        <translation>Поворот у &amp;0°</translation>
    </message>
    <message>
        <location filename="qdjview.cpp" line="556"/>
        <source>Rotate &amp;90°</source>
        <comment>Rotate|</comment>
        <translation>Поворот у &amp;90°</translation>
    </message>
    <message>
        <location filename="qdjview.cpp" line="562"/>
        <source>Rotate &amp;180°</source>
        <comment>Rotate|</comment>
        <translation>Поворот у &amp;180°</translation>
    </message>
    <message>
        <location filename="qdjview.cpp" line="568"/>
        <source>Rotate &amp;270°</source>
        <comment>Rotate|</comment>
        <translation>Поворот у &amp;270°</translation>
    </message>
    <message>
        <location filename="qdjview.cpp" line="574"/>
        <source>&amp;Information...</source>
        <comment>Edit|</comment>
        <translation>&amp;Інформація...</translation>
    </message>
    <message>
        <location filename="qdjview.cpp" line="575"/>
        <source>Ctrl+I</source>
        <comment>Edit|Information</comment>
        <translation>Ctrl+I</translation>
    </message>
    <message>
        <location filename="qdjview.cpp" line="579"/>
        <source>&amp;Metadata...</source>
        <comment>Edit|</comment>
        <translation>&amp;Метадані...</translation>
    </message>
    <message>
        <location filename="qdjview.cpp" line="581"/>
        <source>Ctrl+M</source>
        <comment>Edit|Metadata</comment>
        <translation>Ctrl+M</translation>
    </message>
    <message>
        <location filename="qdjview.cpp" line="595"/>
        <source>&amp;Color</source>
        <comment>Display|</comment>
        <translation>&amp;Колір</translation>
    </message>
    <message>
        <location filename="qdjview.cpp" line="601"/>
        <source>&amp;Stencil</source>
        <comment>Display|</comment>
        <translation>&amp;Шаблон</translation>
    </message>
    <message>
        <location filename="qdjview.cpp" line="608"/>
        <source>&amp;Foreground</source>
        <comment>Display|</comment>
        <translation>&amp;Передній план</translation>
    </message>
    <message>
        <location filename="qdjview.cpp" line="615"/>
        <source>&amp;Background</source>
        <comment>Display|</comment>
        <translation>&amp;Тло</translation>
    </message>
    <message>
        <location filename="qdjview.cpp" line="628"/>
        <source>Prefere&amp;nces...</source>
        <comment>Settings|</comment>
        <translation>Нала&amp;штування...</translation>
    </message>
    <message>
        <location filename="qdjview.cpp" line="635"/>
        <source>F9</source>
        <comment>Settings|Show sidebar</comment>
        <translation>F9</translation>
    </message>
    <message>
        <location filename="qdjview.cpp" line="642"/>
        <source>F10</source>
        <comment>Settings|Show toolbar</comment>
        <translation>F10</translation>
    </message>
    <message>
        <location filename="qdjview.cpp" line="652"/>
        <source>F&amp;ull Screen</source>
        <comment>View|</comment>
        <translation>На весь &amp;екран</translation>
    </message>
    <message>
        <location filename="qdjview.cpp" line="654"/>
        <source>F11</source>
        <comment>View|FullScreen</comment>
        <translation>F11</translation>
    </message>
    <message>
        <location filename="qdjview.cpp" line="659"/>
        <source>&amp;Continuous</source>
        <comment>Layout|</comment>
        <translation>&amp;Неперервно</translation>
    </message>
    <message>
        <location filename="qdjview.cpp" line="662"/>
        <source>F4</source>
        <comment>Layout|Continuous</comment>
        <translation>F4</translation>
    </message>
    <message>
        <location filename="qdjview.cpp" line="667"/>
        <source>Side &amp;by Side</source>
        <comment>Layout|</comment>
        <translation>Сторінки &amp;поруч</translation>
    </message>
    <message>
        <location filename="qdjview.cpp" line="670"/>
        <source>F5</source>
        <comment>Layout|SideBySide</comment>
        <translation>F5</translation>
    </message>
    <message>
        <location filename="qdjview.cpp" line="718"/>
        <source>&amp;File</source>
        <comment>File|</comment>
        <translation>&amp;Файл</translation>
    </message>
    <message>
        <location filename="qdjview.cpp" line="738"/>
        <source>&amp;Edit</source>
        <comment>Edit|</comment>
        <translation>&amp;Редагування</translation>
    </message>
    <message>
        <location filename="qdjview.cpp" line="747"/>
        <source>&amp;View</source>
        <comment>View|</comment>
        <translation>&amp;Перегляд</translation>
    </message>
    <message>
        <location filename="qdjview.cpp" line="748"/>
        <location filename="qdjview.cpp" line="813"/>
        <source>&amp;Zoom</source>
        <comment>View|Zoom</comment>
        <translation>&amp;Масштаб</translation>
    </message>
    <message>
        <location filename="qdjview.cpp" line="762"/>
        <location filename="qdjview.cpp" line="827"/>
        <source>&amp;Rotate</source>
        <comment>View|Rotate</comment>
        <translation>&amp;Обертати</translation>
    </message>
    <message>
        <location filename="qdjview.cpp" line="770"/>
        <location filename="qdjview.cpp" line="835"/>
        <source>&amp;Display</source>
        <comment>View|Display</comment>
        <translation>&amp;Вигляд</translation>
    </message>
    <message>
        <location filename="qdjview.cpp" line="788"/>
        <location filename="qdjview.cpp" line="808"/>
        <source>&amp;Go</source>
        <comment>Go|</comment>
        <translation>Пере&amp;хід</translation>
    </message>
    <message>
        <location filename="qdjview.cpp" line="796"/>
        <source>&amp;Settings</source>
        <comment>Settings|</comment>
        <translation>Пара&amp;метри</translation>
    </message>
    <message>
        <location filename="qdjview.cpp" line="802"/>
        <source>&amp;Help</source>
        <comment>Help|</comment>
        <translation>&amp;Довідка</translation>
    </message>
    <message>
        <location filename="qdjview.cpp" line="2358"/>
        <source>Thumbnails</source>
        <translation>Піктограми</translation>
    </message>
    <message>
        <location filename="qdjview.cpp" line="2365"/>
        <source>Outline</source>
        <translation>Ескіз</translation>
    </message>
    <message>
        <location filename="qdjview.cpp" line="2372"/>
        <source>Find</source>
        <translation>Пошук</translation>
    </message>
    <message>
        <location filename="qdjview.cpp" line="2886"/>
        <source>Print - DjView</source>
        <comment>dialog caption</comment>
        <translation>Друк - DjView</translation>
    </message>
    <message>
        <location filename="qdjview.cpp" line="2903"/>
        <source>Save - DjView</source>
        <comment>dialog caption</comment>
        <translation>Збереження - DjView</translation>
    </message>
    <message>
        <location filename="qdjview.cpp" line="3184"/>
        <source>Text files</source>
        <comment>save filter</comment>
        <translation>Текстові файли</translation>
    </message>
    <message>
        <location filename="qdjview.cpp" line="3185"/>
        <location filename="qdjview.cpp" line="3232"/>
        <source>All files</source>
        <comment>save filter</comment>
        <translation>Всі файли</translation>
    </message>
    <message>
        <location filename="qdjview.cpp" line="3186"/>
        <source>Save Text - DjView</source>
        <comment>dialog caption</comment>
        <translation>Збереження тексту - DjView</translation>
    </message>
    <message>
        <location filename="qdjview.cpp" line="3202"/>
        <location filename="qdjview.cpp" line="3247"/>
        <location filename="qdjview.cpp" line="3267"/>
        <source>Error - DjView</source>
        <comment>dialog caption</comment>
        <translation>Помилка - DjView</translation>
    </message>
    <message>
        <location filename="qdjview.cpp" line="3233"/>
        <source>Save Image - DjView</source>
        <comment>dialog caption</comment>
        <translation>Зберегти картинку - DjView</translation>
    </message>
    <message>
        <location filename="qdjview.cpp" line="3618"/>
        <source> P%1/%2 %3x%4 %5dpi </source>
        <translation> P%1/%2 %3x%4 %5dpi </translation>
    </message>
    <message>
        <location filename="qdjview.cpp" line="4078"/>
        <source>Enter the URL of a DjVu document:</source>
        <translation>Введіть адресу документа DjVu:</translation>
    </message>
    <message>
        <location filename="qdjview.cpp" line="4095"/>
        <source>Information - DjView</source>
        <comment>dialog caption</comment>
        <translation>Інформація - DjView</translation>
    </message>
    <message>
        <location filename="qdjview.cpp" line="4110"/>
        <source>Metadata - DjView</source>
        <comment>dialog caption</comment>
        <translation>Метадані - DjView</translation>
    </message>
    <message>
        <location filename="qdjview.cpp" line="724"/>
        <source>Open &amp;Recent</source>
        <translation>Відкрити &amp;нещодавній</translation>
    </message>
    <message>
        <location filename="qdjview.cpp" line="4251"/>
        <source>&amp;Clear History</source>
        <translation>&amp;Очистити журнал</translation>
    </message>
    <message>
        <location filename="qdjview.cpp" line="379"/>
        <source>Open &amp;Location...</source>
        <comment>File|</comment>
        <translation>Відкрити &amp;адресу...</translation>
    </message>
    <message>
        <location filename="qdjview.cpp" line="380"/>
        <source>Open a remote DjVu document.</source>
        <translation>Відкрити нелокальний документ DjVu.</translation>
    </message>
    <message>
        <location filename="qdjview.cpp" line="421"/>
        <source>Ctrl+F3</source>
        <comment>Edit|Find Next</comment>
        <translation>Ctrl+F3</translation>
    </message>
    <message>
        <location filename="qdjview.cpp" line="432"/>
        <source>Ctrl+F2</source>
        <comment>Edit|Select</comment>
        <translation>Ctrl+F2</translation>
    </message>
    <message>
        <location filename="qdjview.cpp" line="634"/>
        <source>Ctrl+F9</source>
        <comment>Settings|Show sidebar</comment>
        <translation>Ctrl+F9</translation>
    </message>
    <message>
        <location filename="qdjview.cpp" line="641"/>
        <source>Ctrl+F10</source>
        <comment>Settings|Show toolbar</comment>
        <translation>Ctrl+F10</translation>
    </message>
    <message>
        <location filename="qdjview.cpp" line="653"/>
        <source>Ctrl+F11</source>
        <comment>View|FullScreen</comment>
        <translation>Ctrl+F11</translation>
    </message>
    <message>
        <location filename="qdjview.cpp" line="661"/>
        <source>Ctrl+F4</source>
        <comment>Layout|Continuous</comment>
        <translation>Ctrl+F4</translation>
    </message>
    <message>
        <location filename="qdjview.cpp" line="669"/>
        <source>Ctrl+F5</source>
        <comment>Layout|SideBySide</comment>
        <translation>Ctrl+F5</translation>
    </message>
    <message>
        <location filename="qdjview.cpp" line="4077"/>
        <source>Open Location - DjView</source>
        <comment>dialog caption</comment>
        <translation>Відкриття адреси - DjView</translation>
    </message>
    <message>
        <location filename="qdjview.cpp" line="4060"/>
        <source>Open - DjView</source>
        <comment>dialog caption</comment>
        <translation>Відкрити - DjView</translation>
    </message>
    <message>
        <location filename="qdjview.cpp" line="633"/>
        <source>Show &amp;Sidebar</source>
        <comment>Settings|</comment>
        <translation>Показ &amp;бічного пеналу</translation>
    </message>
    <message>
        <location filename="qdjview.cpp" line="640"/>
        <source>Show &amp;Toolbar</source>
        <comment>Settings|</comment>
        <translation>Показ панелі &amp;інструментів</translation>
    </message>
    <message>
        <location filename="qdjview.cpp" line="646"/>
        <source>Show Stat&amp;usbar</source>
        <comment>Settings|</comment>
        <translation>Показ панелі &amp;стану</translation>
    </message>
    <message>
        <location filename="qdjview.cpp" line="3229"/>
        <source>%1 files (*.%2);;</source>
        <comment>save image filter</comment>
        <translation>%1 Файлів (*.%2)</translation>
    </message>
    <message numerus="yes">
        <location filename="qdjview.cpp" line="3832"/>
        <source>%n characters</source>
        <translation>
            <numerusform>%n символ</numerusform>
            <numerusform>%n символи</numerusform>
            <numerusform>%n символів</numerusform>
        </translation>
    </message>
    <message>
        <location filename="qdjview.cpp" line="3701"/>
        <source>Go: 1 page forward.</source>
        <translation>Перехід: 1 сторінка вперед.</translation>
    </message>
    <message>
        <location filename="qdjview.cpp" line="3704"/>
        <source>Go: 1 page backward.</source>
        <translation>Перехід: 1 сторінка назад.</translation>
    </message>
    <message>
        <location filename="qdjview.cpp" line="695"/>
        <source>Copy &amp;URL</source>
        <comment>Edit|</comment>
        <translation>Копіювати &amp;URL</translation>
    </message>
    <message>
        <location filename="qdjview.cpp" line="696"/>
        <source>Save an URL for the current page into the clipboard.</source>
        <translation>Зберегти адресу URL поточної сторінки у буфері.</translation>
    </message>
    <message>
        <location filename="qdjview.cpp" line="700"/>
        <source>Copy &amp;Outline</source>
        <comment>Edit|</comment>
        <translation>Копіювати &amp;ескіз</translation>
    </message>
    <message>
        <location filename="qdjview.cpp" line="704"/>
        <source>Copy &amp;Annotations</source>
        <comment>Edit|</comment>
        <translation>Копіювати &amp;анотації</translation>
    </message>
    <message>
        <location filename="qdjview.cpp" line="3842"/>
        <source>Copy text into the clipboard.</source>
        <translation>Копіювати текст до буфера.</translation>
    </message>
    <message>
        <location filename="qdjview.cpp" line="3843"/>
        <source>Save text into a file.</source>
        <translation>Зберегти текст до файла.</translation>
    </message>
    <message>
        <location filename="qdjview.cpp" line="3849"/>
        <source>Save image into a file.</source>
        <translation>Зберегти зображення до файла.</translation>
    </message>
    <message>
        <location filename="qdjview.cpp" line="3852"/>
        <source>Zoom the selection to fit the window.</source>
        <translation>Підібрати область перегляду під виділене.</translation>
    </message>
    <message>
        <location filename="qdjview.cpp" line="3858"/>
        <source>Copy URL</source>
        <translation>Копіювати адресу URL</translation>
    </message>
    <message>
        <location filename="qdjview.cpp" line="3859"/>
        <source>Save into the clipboard an URL that highlights the selection.</source>
        <translation>Зберегти до буфера адресу URL, яку виділено у вибраному.</translation>
    </message>
    <message>
        <location filename="qdjview.cpp" line="3861"/>
        <source>Copy Maparea</source>
        <translation>Копіювати область карти</translation>
    </message>
    <message>
        <location filename="qdjview.cpp" line="3862"/>
        <source>Save into the clipboard a maparea annotation expression for program djvused.</source>
        <translation>Зберегти до буфера вираз анотації карти області для програми djvused.</translation>
    </message>
    <message>
        <location filename="qdjview.cpp" line="697"/>
        <source>Ctrl+C</source>
        <comment>Edit|CopyURL</comment>
        <translation>Ctrl+C</translation>
    </message>
    <message>
        <location filename="qdjview.cpp" line="701"/>
        <source>Save the djvused code for the outline into the clipboard.</source>
        <translation>Зберегти код djvused ескіза до буфера.</translation>
    </message>
    <message>
        <location filename="qdjview.cpp" line="705"/>
        <source>Save the djvused code for the page annotations into the clipboard.</source>
        <translation>Зберегти код djvused анотації сторінки до буфера.</translation>
    </message>
    <message>
        <location filename="qdjview.cpp" line="3848"/>
        <source>Copy image into the clipboard.</source>
        <translation>Копіювати зображення до буфера.</translation>
    </message>
    <message>
        <location filename="qdjview.cpp" line="675"/>
        <source>Co&amp;ver Page</source>
        <comment>Layout|</comment>
        <translation>&amp;Обкладинка</translation>
    </message>
    <message>
        <location filename="qdjview.cpp" line="679"/>
        <source>Ctrl+F6</source>
        <comment>Layout|CoverPage</comment>
        <translation>Ctrl+F6</translation>
    </message>
    <message>
        <location filename="qdjview.cpp" line="680"/>
        <source>F6</source>
        <comment>Layout|CoverPage</comment>
        <translation>F6</translation>
    </message>
    <message>
        <location filename="qdjview.cpp" line="681"/>
        <source>Show the cover page alone in side-by-side mode.</source>
        <translation>У режимі зі сторінками, розташованими поруч, показувати першу сторінку окремо.</translation>
    </message>
    <message>
        <location filename="qdjview.cpp" line="685"/>
        <source>&amp;Right to Left</source>
        <comment>Layout|</comment>
        <translation>&amp;Справа ліворуч</translation>
    </message>
    <message>
        <location filename="qdjview.cpp" line="689"/>
        <source>Ctrl+Shift+F6</source>
        <comment>Layout|RightToLeft</comment>
        <translation></translation>
    </message>
    <message>
        <location filename="qdjview.cpp" line="690"/>
        <source>Shift+F6</source>
        <comment>Layout|RightToLeft</comment>
        <translation>Shift+F6</translation>
    </message>
    <message>
        <location filename="qdjview.cpp" line="691"/>
        <source>Show pages right-to-left in side-by-side mode.</source>
        <translation>Показувати сторінки лівописних мов у режимі сторінок поруч.</translation>
    </message>
    <message>
        <location filename="qdjview.cpp" line="4059"/>
        <source>All files</source>
        <translation>Всі файли</translation>
    </message>
    <message>
        <location filename="qdjview.cpp" line="622"/>
        <source>&amp;Hidden Text</source>
        <comment>Display|</comment>
        <translation>При&amp;хований текст</translation>
    </message>
    <message>
        <location filename="qdjview.cpp" line="623"/>
        <source>Overlay a representation of the hidden text layer.</source>
        <translation>Накласти на зображення представлення шару прихованого тексту.</translation>
    </message>
    <message>
        <location filename="qdjview.cpp" line="161"/>
        <source>Hidden Text</source>
        <comment>modeCombo</comment>
        <translation>Прихований текст</translation>
    </message>
</context>
<context>
    <name>QDjViewApplication</name>
    <message>
        <location filename="djview.cpp" line="304"/>
        <source>Usage: djview [options] [filename-or-url]
Common options include:
-help~~~Prints this message.
-verbose~~~Prints all warning messages.
-display &lt;xdpy&gt;~~~Select the X11 display &lt;xdpy&gt;.
-geometry &lt;xgeom&gt;~~~Select the initial window geometry.
-font &lt;xlfd&gt;~~~Select the X11 name of the main font.
-style &lt;qtstyle&gt;~~~Select the QT user interface style.
-fullscreen, -fs~~~Start djview in full screen mode.
-page=&lt;page&gt;~~~Jump to page &lt;page&gt;.
-zoom=&lt;zoom&gt;~~~Set zoom factor.
-continuous=&lt;yn&gt;~~~Set continuous layout.
-sidebyside=&lt;yn&gt;~~~Set side-by-side layout.
</source>
        <translation>Використання: djview [параметри] [назва-файла-або адреса]
Звичайні параметри такі:
-help~~~Надрукувати це повідомлення
-verbose~~~Друкувати всі попереджувальні повідомлення.
-display &lt;xdpy&gt;~~~Обрати дисплей X11 &lt;xdpy&gt;.
-geometry &lt;xgeom&gt;~~~Обрати початкову геометрію вікна.
-font &lt;xlfd&gt;~~~Обрати назву основного шрифту X11.
-style &lt;qt-стиль&gt;~~~Обрати стиль відображення QT.
-fullscreen, -fs~~~Виконати djview у повноекранному режимі.
-page=&lt;сторінка&gt;~~~Перейти до сторінки &lt;сторінка&gt;.
-zoom=&lt;масштаб&gt;~~~Встановити масштаб.
-continuous=&lt;yn&gt;~~~Встановити режим неперервного перегляду.
-sidebyside=&lt;yn&gt;~~~Встановити розміщення поряд.
</translation>
    </message>
    <message>
        <location filename="djview.cpp" line="238"/>
        <source>cannot open &apos;%1&apos;.</source>
        <translation>неможливо відкрити &apos;%1&apos;.</translation>
    </message>
</context>
<context>
    <name>QDjViewDjVuExporter</name>
    <message>
        <location filename="qdjviewexporters.cpp" line="384"/>
        <source>&lt;html&gt; This file belongs to a non empty directory. Saving an indirect document creates many files in this directory. Do you want to continue and risk overwriting files in this directory?&lt;/html&gt;</source>
        <translation>&lt;html&gt; Цей файл належить до не порожньої теки. Збереження непрямого документу створить багато файлів у цій теці. Бажаєте продовжувати з ризиком перезапису файлів у цій теці?&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="qdjviewexporters.cpp" line="390"/>
        <source>Con&amp;tinue</source>
        <translation>Про&amp;довжувати</translation>
    </message>
    <message>
        <location filename="qdjviewexporters.cpp" line="391"/>
        <source>&amp;Cancel</source>
        <translation>&amp;Скасувати</translation>
    </message>
    <message>
        <location filename="qdjviewexporters.cpp" line="435"/>
        <source>Save job creation failed!</source>
        <translation>Створення процесу збереження зазнало невдачі!</translation>
    </message>
    <message>
        <location filename="qdjviewexporters.cpp" line="327"/>
        <source>DjVu Bundled Document</source>
        <translation>Зв&apos;язаний документ DjVu</translation>
    </message>
    <message>
        <location filename="qdjviewexporters.cpp" line="328"/>
        <location filename="qdjviewexporters.cpp" line="332"/>
        <source>DjVu Files (*.djvu *.djv)</source>
        <translation>Файли DjVu (*.djvu *.djv)</translation>
    </message>
    <message>
        <location filename="qdjviewexporters.cpp" line="331"/>
        <source>DjVu Indirect Document</source>
        <translation>Непрямий документ DjVu</translation>
    </message>
    <message>
        <location filename="qdjviewexporters.cpp" line="418"/>
        <source>Unknown error.</source>
        <translation>Невідома помилка.</translation>
    </message>
    <message>
        <location filename="qdjviewexporters.cpp" line="420"/>
        <source>System error: %1.</source>
        <translation>Системна помилка: %1.</translation>
    </message>
    <message>
        <location filename="qdjviewexporters.cpp" line="383"/>
        <source>Question - DjView</source>
        <comment>dialog caption</comment>
        <translation>Запитання - DjView</translation>
    </message>
</context>
<context>
    <name>QDjViewErrorDialog</name>
    <message>
        <location filename="qdjviewerrordialog.ui" line="16"/>
        <location filename="qdjviewdialogs.cpp" line="125"/>
        <source>DjView Error</source>
        <translation>Помилка DjView</translation>
    </message>
    <message>
        <location filename="qdjviewerrordialog.ui" line="110"/>
        <source>&amp;Ok</source>
        <translation>&amp;Гаразд</translation>
    </message>
    <message>
        <location filename="qdjviewdialogs.cpp" line="162"/>
        <source>Error - DjView</source>
        <comment>dialog caption</comment>
        <translation>Помилка - DjView</translation>
    </message>
</context>
<context>
    <name>QDjViewExportDialog</name>
    <message>
        <location filename="qdjviewexportdialog.ui" line="13"/>
        <source>Dialog</source>
        <translation>Діалог</translation>
    </message>
    <message>
        <location filename="qdjviewexportdialog.ui" line="26"/>
        <location filename="qdjviewexportdialog.ui" line="38"/>
        <source>Export</source>
        <translation>Експортувати</translation>
    </message>
    <message>
        <location filename="qdjviewexportdialog.ui" line="139"/>
        <source>Format:</source>
        <translation>Формат:</translation>
    </message>
    <message>
        <location filename="qdjviewexportdialog.ui" line="50"/>
        <source>&amp;Document</source>
        <translation>&amp;Документ</translation>
    </message>
    <message>
        <location filename="qdjviewexportdialog.ui" line="60"/>
        <source>C&amp;urrent page</source>
        <translation>П&amp;оточна сторінка</translation>
    </message>
    <message>
        <location filename="qdjviewexportdialog.ui" line="75"/>
        <source>&amp;Pages</source>
        <translation>&amp;Сторінки</translation>
    </message>
    <message>
        <location filename="qdjviewexportdialog.ui" line="85"/>
        <source>to</source>
        <translation>до</translation>
    </message>
    <message>
        <location filename="qdjviewexportdialog.ui" line="119"/>
        <source>Destination</source>
        <translation>Призначення</translation>
    </message>
    <message>
        <location filename="qdjviewexportdialog.ui" line="214"/>
        <source>&amp;Defaults</source>
        <translation>&amp;Типове</translation>
    </message>
    <message>
        <location filename="qdjviewexportdialog.ui" line="240"/>
        <source>&amp;Ok</source>
        <translation>&amp;Гаразд</translation>
    </message>
    <message>
        <location filename="qdjviewexportdialog.ui" line="256"/>
        <location filename="qdjviewdialogs.cpp" line="1333"/>
        <source>&amp;Cancel</source>
        <translation>&amp;Скасувати</translation>
    </message>
    <message>
        <location filename="qdjviewexportdialog.ui" line="283"/>
        <source>Stop</source>
        <translation>Зупинити</translation>
    </message>
    <message>
        <location filename="qdjviewdialogs.cpp" line="1170"/>
        <source>&lt;html&gt;&lt;b&gt;Saving.&lt;/b&gt;&lt;br/&gt; You can save the whole document or a page range under a variety of formats. Selecting certain formats creates additional dialog pages for specifying format options.&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;b&gt;Збереження.&lt;/b&gt;&lt;br/&gt; Ви можете зберегти весь документ або проміжок сторінок у цілій низці форматів. Вибір певних форматів викличе додаткові сторінки діалогів для визначення параметрів формата.&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="qdjviewdialogs.cpp" line="1324"/>
        <source>Overwriting the current file is not allowed!</source>
        <translation>Перезапис поточного файла заборонено!</translation>
    </message>
    <message>
        <location filename="qdjviewdialogs.cpp" line="1330"/>
        <source>A file with this name already exists.
Do you want to replace it?</source>
        <translation>Файл з такою назвою вже існує.
Бажаєте замінити його?</translation>
    </message>
    <message>
        <location filename="qdjviewdialogs.cpp" line="1332"/>
        <source>&amp;Replace</source>
        <translation>&amp;Замінити</translation>
    </message>
    <message>
        <location filename="qdjviewdialogs.cpp" line="1370"/>
        <source>This operation has failed.</source>
        <translation>Ця операція зазнала невдачі.</translation>
    </message>
    <message>
        <location filename="qdjviewdialogs.cpp" line="1374"/>
        <source>This operation has been interrupted.</source>
        <translation>Цю операцію було перервано.</translation>
    </message>
    <message>
        <location filename="qdjviewdialogs.cpp" line="1410"/>
        <source>Export - DjView</source>
        <comment>dialog caption</comment>
        <translation>Експорт - DjView</translation>
    </message>
    <message>
        <location filename="qdjviewdialogs.cpp" line="1323"/>
        <source>Error - DjView</source>
        <comment>dialog caption</comment>
        <translation>Помилка - DjView</translation>
    </message>
    <message>
        <location filename="qdjviewdialogs.cpp" line="1329"/>
        <source>Question - DjView</source>
        <comment>dialog caption</comment>
        <translation>Запитання - DjView</translation>
    </message>
    <message>
        <location filename="qdjviewdialogs.cpp" line="1403"/>
        <source>All files</source>
        <comment>save filter</comment>
        <translation>Всі файли</translation>
    </message>
</context>
<context>
    <name>QDjViewExportPS1</name>
    <message>
        <location filename="qdjviewexportps1.ui" line="16"/>
        <source>Form</source>
        <translation>Форма</translation>
    </message>
    <message>
        <location filename="qdjviewexportps1.ui" line="36"/>
        <source>Color</source>
        <translation>Колір</translation>
    </message>
    <message>
        <location filename="qdjviewexportps1.ui" line="48"/>
        <source>&amp;Color</source>
        <translation>&amp;Колір</translation>
    </message>
    <message>
        <location filename="qdjviewexportps1.ui" line="58"/>
        <source>&amp;GrayScale</source>
        <translation>&amp;Сірі півтони</translation>
    </message>
    <message>
        <location filename="qdjviewexportps1.ui" line="68"/>
        <source>Marks</source>
        <translation>Позначки</translation>
    </message>
    <message>
        <location filename="qdjviewexportps1.ui" line="80"/>
        <source>Print image &amp;frame</source>
        <translation>Друкувати &amp;рамку картинки</translation>
    </message>
    <message>
        <location filename="qdjviewexportps1.ui" line="87"/>
        <source>Print crop &amp;marks</source>
        <translation>Друкувати обмежувальні &amp;мітки</translation>
    </message>
    <message>
        <location filename="qdjviewexportps1.ui" line="99"/>
        <source>PostScript</source>
        <translation>PostScript</translation>
    </message>
    <message>
        <location filename="qdjviewexportps1.ui" line="111"/>
        <source>Language Level</source>
        <translation>Рівень мови</translation>
    </message>
</context>
<context>
    <name>QDjViewExportPS2</name>
    <message>
        <location filename="qdjviewexportps2.ui" line="16"/>
        <source>Form</source>
        <translation>Форма</translation>
    </message>
    <message>
        <location filename="qdjviewexportps2.ui" line="36"/>
        <source>Scaling</source>
        <translation>Масштабування</translation>
    </message>
    <message>
        <location filename="qdjviewexportps2.ui" line="48"/>
        <source>Scale to &amp;fit the page</source>
        <translation>Масштабувати, щоб &amp;вмістити сторінку</translation>
    </message>
    <message>
        <location filename="qdjviewexportps2.ui" line="66"/>
        <source>&amp;Zoom</source>
        <translation>&amp;Масштаб</translation>
    </message>
    <message>
        <location filename="qdjviewexportps2.ui" line="76"/>
        <source> %</source>
        <translation> %</translation>
    </message>
    <message>
        <location filename="qdjviewexportps2.ui" line="100"/>
        <source>Orientation</source>
        <translation>Орієнтація</translation>
    </message>
    <message>
        <location filename="qdjviewexportps2.ui" line="112"/>
        <source>Automatic</source>
        <translation>Автоматично</translation>
    </message>
    <message>
        <location filename="qdjviewexportps2.ui" line="122"/>
        <source>&amp;Portrait</source>
        <translation>П&amp;ортрет</translation>
    </message>
    <message>
        <location filename="qdjviewexportps2.ui" line="129"/>
        <source>&amp;Landscape</source>
        <translation>&amp;Альбом</translation>
    </message>
</context>
<context>
    <name>QDjViewExportPS3</name>
    <message>
        <location filename="qdjviewexportps3.ui" line="16"/>
        <source>Form</source>
        <translation>Форма</translation>
    </message>
    <message>
        <location filename="qdjviewexportps3.ui" line="28"/>
        <source>Print sheets suitable for folding a booklet.</source>
        <translation>Друкувати листки, придатні для створення буклету.</translation>
    </message>
    <message>
        <location filename="qdjviewexportps3.ui" line="35"/>
        <source>Advanced</source>
        <translation>Додатково</translation>
    </message>
    <message>
        <location filename="qdjviewexportps3.ui" line="55"/>
        <source>Sheets per booklet: </source>
        <translation>Листків на буклет: </translation>
    </message>
    <message>
        <location filename="qdjviewexportps3.ui" line="62"/>
        <source>Unlimited</source>
        <translation>Не обмежувати</translation>
    </message>
    <message>
        <location filename="qdjviewexportps3.ui" line="65"/>
        <source>at most </source>
        <translation>не більше </translation>
    </message>
    <message>
        <location filename="qdjviewexportps3.ui" line="98"/>
        <source>Print </source>
        <translation>Друк </translation>
    </message>
    <message>
        <location filename="qdjviewexportps3.ui" line="112"/>
        <source>rectos and versos.</source>
        <translation>верхні та нижні.</translation>
    </message>
    <message>
        <location filename="qdjviewexportps3.ui" line="117"/>
        <source>rectos only.</source>
        <translation>тільки верхні.</translation>
    </message>
    <message>
        <location filename="qdjviewexportps3.ui" line="122"/>
        <source>versos only.</source>
        <translation>тільки нижні.</translation>
    </message>
    <message>
        <location filename="qdjviewexportps3.ui" line="153"/>
        <source>Shift rectos and versos by </source>
        <translation>Змістити верхні та нижні на </translation>
    </message>
    <message>
        <location filename="qdjviewexportps3.ui" line="160"/>
        <source> points.</source>
        <translation> точок.</translation>
    </message>
    <message>
        <location filename="qdjviewexportps3.ui" line="196"/>
        <source>Center:</source>
        <translation>Посередині:</translation>
    </message>
    <message>
        <location filename="qdjviewexportps3.ui" line="203"/>
        <source> points</source>
        <translation> точок</translation>
    </message>
    <message>
        <location filename="qdjviewexportps3.ui" line="216"/>
        <source>plus</source>
        <translation>плюс</translation>
    </message>
    <message>
        <location filename="qdjviewexportps3.ui" line="223"/>
        <source>/100</source>
        <translation>/100</translation>
    </message>
    <message>
        <location filename="qdjviewexportps3.ui" line="239"/>
        <source>per page.</source>
        <translation>на сторінку.</translation>
    </message>
</context>
<context>
    <name>QDjViewExportPrn</name>
    <message>
        <location filename="qdjviewexportprn.ui" line="13"/>
        <source>Form</source>
        <translation>Форма</translation>
    </message>
    <message>
        <location filename="qdjviewexportprn.ui" line="21"/>
        <source>Color</source>
        <translation>Колір</translation>
    </message>
    <message>
        <location filename="qdjviewexportprn.ui" line="30"/>
        <source>&amp;Color</source>
        <translation>&amp;Колір</translation>
    </message>
    <message>
        <location filename="qdjviewexportprn.ui" line="40"/>
        <source>&amp;GrayScale</source>
        <translation>&amp;Сірі півтони</translation>
    </message>
    <message>
        <location filename="qdjviewexportprn.ui" line="50"/>
        <source>Marks</source>
        <translation>Позначки</translation>
    </message>
    <message>
        <location filename="qdjviewexportprn.ui" line="59"/>
        <source>Print image &amp;frame</source>
        <translation>Друкувати &amp;рамку картинки</translation>
    </message>
    <message>
        <location filename="qdjviewexportprn.ui" line="69"/>
        <source>Print crop &amp;marks</source>
        <translation>Друкувати обмежувальні &amp;мітки</translation>
    </message>
    <message>
        <location filename="qdjviewexportprn.ui" line="83"/>
        <source>Scaling</source>
        <translation>Масштабування</translation>
    </message>
    <message>
        <location filename="qdjviewexportprn.ui" line="89"/>
        <source>Scale to &amp;fit the page</source>
        <translation>Масштабувати, щоб &amp;вмістити сторінку</translation>
    </message>
    <message>
        <location filename="qdjviewexportprn.ui" line="104"/>
        <source>&amp;Zoom</source>
        <translation>&amp;Масштаб</translation>
    </message>
    <message>
        <location filename="qdjviewexportprn.ui" line="117"/>
        <source> %</source>
        <translation> %</translation>
    </message>
    <message>
        <location filename="qdjviewexportprn.ui" line="141"/>
        <source>Orientation</source>
        <translation>Орієнтація</translation>
    </message>
    <message>
        <location filename="qdjviewexportprn.ui" line="150"/>
        <source>Automatic</source>
        <translation>Автоматично</translation>
    </message>
    <message>
        <location filename="qdjviewexportprn.ui" line="163"/>
        <source>&amp;Portrait</source>
        <translation>П&amp;ортрет</translation>
    </message>
    <message>
        <location filename="qdjviewexportprn.ui" line="173"/>
        <source>&amp;Landscape</source>
        <translation>&amp;Альбом</translation>
    </message>
</context>
<context>
    <name>QDjViewExportTiff</name>
    <message>
        <location filename="qdjviewexporttiff.ui" line="13"/>
        <source>Form</source>
        <translation>Форма</translation>
    </message>
    <message>
        <location filename="qdjviewexporttiff.ui" line="25"/>
        <source>Resolution</source>
        <translation>Роздільна здатність</translation>
    </message>
    <message>
        <location filename="qdjviewexporttiff.ui" line="37"/>
        <source>Maximum image resolution </source>
        <translation>Максимальна роздільна здатність картинки </translation>
    </message>
    <message>
        <location filename="qdjviewexporttiff.ui" line="44"/>
        <source> dpi</source>
        <translation> т/д</translation>
    </message>
    <message>
        <location filename="qdjviewexporttiff.ui" line="76"/>
        <source>Compression</source>
        <translation>Стиснення</translation>
    </message>
    <message>
        <location filename="qdjviewexporttiff.ui" line="88"/>
        <source>Force &amp;bitonal G4 compression.</source>
        <translation>Примусове &amp;двотональне стиснення G4.</translation>
    </message>
    <message>
        <location filename="qdjviewexporttiff.ui" line="95"/>
        <source>Allow &amp;lossy JPEG compression.</source>
        <translation>Використовувати JPEG стиснення з &amp;втратами.</translation>
    </message>
    <message>
        <location filename="qdjviewexporttiff.ui" line="129"/>
        <source>JPEG &amp;quality</source>
        <translation>&amp;Якість JPEG</translation>
    </message>
    <message>
        <location filename="qdjviewexporttiff.ui" line="167"/>
        <source>Allow &amp;deflate compression.</source>
        <translation>Дозволити &amp;стиснення.</translation>
    </message>
</context>
<context>
    <name>QDjViewFind</name>
    <message>
        <location filename="qdjviewsidebar.cpp" line="1555"/>
        <source>Case sensitive</source>
        <translation>З врахуванням регістру</translation>
    </message>
    <message>
        <location filename="qdjviewsidebar.cpp" line="1558"/>
        <source>Words only</source>
        <translation>Тільки слова</translation>
    </message>
    <message>
        <location filename="qdjviewsidebar.cpp" line="1580"/>
        <source>Find Previous (Shift+F3) </source>
        <translation>Шукати попереднє (Shift+F3) </translation>
    </message>
    <message>
        <location filename="qdjviewsidebar.cpp" line="1585"/>
        <source>Find Next (F3) </source>
        <translation>Шукати наступне (F3) </translation>
    </message>
    <message>
        <location filename="qdjviewsidebar.cpp" line="1595"/>
        <source>Options</source>
        <translation>Параметри</translation>
    </message>
    <message>
        <location filename="qdjviewsidebar.cpp" line="1642"/>
        <source>&lt;html&gt;&lt;b&gt;Finding text.&lt;/b&gt;&lt;br/&gt; Search hits appear progressively as soon as you type a search string. Typing enter jumps to the next hit. To move to the previous or next hit, you can also use the arrow buttons or the shortcuts &lt;tt&gt;F3&lt;/tt&gt; or &lt;tt&gt;Shift-F3&lt;/tt&gt;. You can also double click a page name. Use the &lt;tt&gt;Options&lt;/tt&gt; menu to search words only or to specify the case sensitivity.&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;b&gt;Пошук тексту.&lt;/b&gt;&lt;br/&gt; Під час пошуку відповідники з&apos;являються поступово, паралельно з введенням вами рядка для пошуку. Натиснення клавіші вводу задає перехід до наступного відповідника. Щоб перейти до попереднього чи наступного відповідника, Ви також можете використовувати стрілки та гарячі клавіші &lt;tt&gt;F3&lt;/tt&gt; або &lt;tt&gt;Shift-F3&lt;/tt&gt;. Також можна двічі клацнути на назві сторінки. Щоб шукати лише цілі слова або задати чутливість до регістру, скористайтеся меню &lt;tt&gt;Параметри&lt;/tt&gt;, &lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="qdjviewsidebar.cpp" line="1561"/>
        <source>Regular expression</source>
        <translation>Формальний вираз</translation>
    </message>
    <message>
        <location filename="qdjviewsidebar.cpp" line="1591"/>
        <source>Reset search options to default values.</source>
        <translation>Відновити типові значення параметрів пошуку.</translation>
    </message>
    <message>
        <location filename="qdjviewsidebar.cpp" line="1652"/>
        <source>Specify whether search hits must begin on a word boundary.</source>
        <translation>Визначіть, чи має шуканий рядок бути розташованим на межі слова.</translation>
    </message>
    <message>
        <location filename="qdjviewsidebar.cpp" line="1654"/>
        <source>Specify whether searches are case sensitive.</source>
        <translation>Вкажіть, чи слід враховувати регістр символів під час пошуку.</translation>
    </message>
    <message>
        <location filename="qdjviewsidebar.cpp" line="1656"/>
        <source>Regular expressions describe complex string matching patterns.</source>
        <translation>За допомогою формальних виразів можна описати складні шаблони відповідності.</translation>
    </message>
    <message>
        <location filename="qdjviewsidebar.cpp" line="1658"/>
        <source>&lt;html&gt;&lt;b&gt;Regular Expression Quick Guide&lt;/b&gt;&lt;ul&gt;&lt;li&gt;The dot &lt;tt&gt;.&lt;/tt&gt; matches any character.&lt;/li&gt;&lt;li&gt;Most characters match themselves.&lt;/li&gt;&lt;li&gt;Prepend a backslash &lt;tt&gt;\&lt;/tt&gt; to match special    characters &lt;tt&gt;()[]{}|*+.?!^$\&lt;/tt&gt;.&lt;/li&gt;&lt;li&gt;&lt;tt&gt;\b&lt;/tt&gt; matches a word boundary.&lt;/li&gt;&lt;li&gt;&lt;tt&gt;\w&lt;/tt&gt; matches a word character.&lt;/li&gt;&lt;li&gt;&lt;tt&gt;\d&lt;/tt&gt; matches a digit character.&lt;/li&gt;&lt;li&gt;&lt;tt&gt;\s&lt;/tt&gt; matches a blank character.&lt;/li&gt;&lt;li&gt;&lt;tt&gt;\n&lt;/tt&gt; matches a newline character.&lt;/li&gt;&lt;li&gt;&lt;tt&gt;[&lt;i&gt;a&lt;/i&gt;-&lt;i&gt;b&lt;/i&gt;]&lt;/tt&gt; matches characters in range    &lt;tt&gt;&lt;i&gt;a&lt;/i&gt;&lt;/tt&gt;-&lt;tt&gt;&lt;i&gt;b&lt;/i&gt;&lt;/tt&gt;.&lt;/li&gt;&lt;li&gt;&lt;tt&gt;[^&lt;i&gt;a&lt;/i&gt;-&lt;i&gt;b&lt;/i&gt;]&lt;/tt&gt; matches characters outside range    &lt;tt&gt;&lt;i&gt;a&lt;/i&gt;&lt;/tt&gt;-&lt;tt&gt;&lt;i&gt;b&lt;/i&gt;&lt;/tt&gt;.&lt;/li&gt;&lt;li&gt;&lt;tt&gt;&lt;i&gt;a&lt;/i&gt;|&lt;i&gt;b&lt;/i&gt;&lt;/tt&gt; matches either regular expression    &lt;tt&gt;&lt;i&gt;a&lt;/i&gt;&lt;/tt&gt; or regular expression &lt;tt&gt;&lt;i&gt;b&lt;/i&gt;&lt;/tt&gt;.&lt;/li&gt;&lt;li&gt;&lt;tt&gt;&lt;i&gt;a&lt;/i&gt;{&lt;i&gt;n&lt;/i&gt;,&lt;i&gt;m&lt;/i&gt;}&lt;/tt&gt; matches regular expression    &lt;tt&gt;&lt;i&gt;a&lt;/i&gt;&lt;/tt&gt; repeated &lt;tt&gt;&lt;i&gt;n&lt;/i&gt;&lt;/tt&gt; to &lt;tt&gt;&lt;i&gt;m&lt;/i&gt;&lt;/tt&gt;    times.&lt;/li&gt;&lt;li&gt;&lt;tt&gt;&lt;i&gt;a&lt;/i&gt;?&lt;/tt&gt;, &lt;tt&gt;&lt;i&gt;a&lt;/i&gt;*&lt;/tt&gt;, and &lt;tt&gt;&lt;i&gt;a&lt;/i&gt;+&lt;/tt&gt;    are shorthands for &lt;tt&gt;&lt;i&gt;a&lt;/i&gt;{0,1}&lt;/tt&gt;, &lt;tt&gt;&lt;i&gt;a&lt;/i&gt;{0,}&lt;/tt&gt;,     and &lt;tt&gt;&lt;i&gt;a&lt;/i&gt;{1,}&lt;/tt&gt;.&lt;/li&gt;&lt;li&gt;Use parentheses &lt;tt&gt;()&lt;/tt&gt; to group regular expressions     before &lt;tt&gt;?+*{&lt;/tt&gt;.&lt;/li&gt;&lt;/ul&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;b&gt;Коротка інструкція з формальних виразів&lt;/b&gt;&lt;ul&gt;&lt;li&gt;Крапка &lt;tt&gt;.&lt;/tt&gt; відповідає довільному символу.&lt;/li&gt;&lt;li&gt;Більшість символів відповідаються самим собі.&lt;/li&gt;&lt;li&gt;До спеціальних символів, &lt;tt&gt;()[]{}|*+.?!^$\&lt;/tt&gt;, слід додавати префікс &lt;tt&gt;\&lt;/tt&gt;.&lt;/li&gt;&lt;li&gt;&lt;tt&gt;\b&lt;/tt&gt; відповідає межі слова.&lt;/li&gt;&lt;li&gt;&lt;tt&gt;\w&lt;/tt&gt; відповідає літері.&lt;/li&gt;&lt;li&gt;&lt;tt&gt;\d&lt;/tt&gt; відповідає числовому символу.&lt;/li&gt;&lt;li&gt;&lt;tt&gt;\s&lt;/tt&gt; відповідає пробілу.&lt;/li&gt;&lt;li&gt;&lt;tt&gt;\n&lt;/tt&gt; відповідає символу нового рядка.&lt;/li&gt;&lt;li&gt;&lt;tt&gt;[&lt;i&gt;a&lt;/i&gt;-&lt;i&gt;b&lt;/i&gt;]&lt;/tt&gt; відповідає діапазону символів    &lt;tt&gt;&lt;i&gt;a&lt;/i&gt;&lt;/tt&gt;-&lt;tt&gt;&lt;i&gt;b&lt;/i&gt;&lt;/tt&gt;.&lt;/li&gt;&lt;li&gt;&lt;tt&gt;[^&lt;i&gt;a&lt;/i&gt;-&lt;i&gt;b&lt;/i&gt;]&lt;/tt&gt; відповідає символам поза діапазоном    &lt;tt&gt;&lt;i&gt;a&lt;/i&gt;&lt;/tt&gt;-&lt;tt&gt;&lt;i&gt;b&lt;/i&gt;&lt;/tt&gt;.&lt;/li&gt;&lt;li&gt;&lt;tt&gt;&lt;i&gt;a&lt;/i&gt;|&lt;i&gt;b&lt;/i&gt;&lt;/tt&gt; відповідає або формальному виразу    &lt;tt&gt;&lt;i&gt;a&lt;/i&gt;&lt;/tt&gt;, або формальному виразу &lt;tt&gt;&lt;i&gt;b&lt;/i&gt;&lt;/tt&gt;.&lt;/li&gt;&lt;li&gt;&lt;tt&gt;&lt;i&gt;a&lt;/i&gt;{&lt;i&gt;n&lt;/i&gt;,&lt;i&gt;m&lt;/i&gt;}&lt;/tt&gt; відповідає формальному виразу    &lt;tt&gt;&lt;i&gt;a&lt;/i&gt;&lt;/tt&gt;, повтореному від &lt;tt&gt;&lt;i&gt;n&lt;/i&gt;&lt;/tt&gt; до &lt;tt&gt;&lt;i&gt;m&lt;/i&gt;&lt;/tt&gt;    разів.&lt;/li&gt;&lt;li&gt;&lt;tt&gt;&lt;i&gt;a&lt;/i&gt;?&lt;/tt&gt;, &lt;tt&gt;&lt;i&gt;a&lt;/i&gt;*&lt;/tt&gt; і &lt;tt&gt;&lt;i&gt;a&lt;/i&gt;+&lt;/tt&gt;  — скорочення шаблонів &lt;tt&gt;&lt;i&gt;a&lt;/i&gt;{0,1}&lt;/tt&gt;, &lt;tt&gt;&lt;i&gt;a&lt;/i&gt;{0,}&lt;/tt&gt;     і &lt;tt&gt;&lt;i&gt;a&lt;/i&gt;{1,}&lt;/tt&gt;.&lt;/li&gt;&lt;li&gt;Для групування формальних виразів додайте &lt;tt&gt;()&lt;/tt&gt; перед &lt;tt&gt;?+*{&lt;/tt&gt;.&lt;/li&gt;&lt;/ul&gt;&lt;/html&gt;</translation>
    </message>
</context>
<context>
    <name>QDjViewFind::Model</name>
    <message>
        <location filename="qdjviewsidebar.cpp" line="946"/>
        <source>1 hit</source>
        <translation>1 збіг</translation>
    </message>
    <message>
        <location filename="qdjviewsidebar.cpp" line="1015"/>
        <source>Page %1 (1 hit)</source>
        <translation>Сторінка %1 (1 збіг)</translation>
    </message>
    <message>
        <location filename="qdjviewsidebar.cpp" line="1320"/>
        <source>Searching page %1 (waiting for data.)</source>
        <translation>Пошук сторінки %1 (очікування на дані.)</translation>
    </message>
    <message>
        <location filename="qdjviewsidebar.cpp" line="1333"/>
        <source>Searching page %1.</source>
        <translation>Пошук сторінки %1.</translation>
    </message>
    <message>
        <location filename="qdjviewsidebar.cpp" line="1368"/>
        <source>No hits!</source>
        <translation>Не знайдено!</translation>
    </message>
    <message>
        <location filename="qdjviewsidebar.cpp" line="1373"/>
        <source>&lt;html&gt;Document is not searchable. No page contains information about its textual content.&lt;/html&gt;</source>
        <translation>&lt;html&gt;Пошук у документі неможливий. Жодна сторінка не містить інформації про свій текстовий вміст.&lt;/html&gt;</translation>
    </message>
    <message numerus="yes">
        <location filename="qdjviewsidebar.cpp" line="947"/>
        <source>%n hits</source>
        <translation>
            <numerusform>%n збіг</numerusform>
            <numerusform>%n збіги</numerusform>
            <numerusform>%n збігів</numerusform>
        </translation>
    </message>
    <message numerus="yes">
        <location filename="qdjviewsidebar.cpp" line="1017"/>
        <source>Page %1 (%n hits)</source>
        <translation>
            <numerusform>Сторінка %1 (%n збіг)</numerusform>
            <numerusform>Сторінка %1 (%n збіги)</numerusform>
            <numerusform>Сторінка %1 (%n збігів)</numerusform>
        </translation>
    </message>
    <message>
        <location filename="qdjviewsidebar.cpp" line="1379"/>
        <source>&lt;html&gt;Invalid regular expression.&lt;/html&gt;</source>
        <translation>&lt;html&gt;Некоректний формальний вираз.&lt;/html&gt;</translation>
    </message>
</context>
<context>
    <name>QDjViewImgExporter</name>
    <message>
        <location filename="qdjviewexporters.cpp" line="1914"/>
        <source>Cannot render page.</source>
        <translation>Відображення сторінки неможливе.</translation>
    </message>
    <message>
        <location filename="qdjviewexporters.cpp" line="1924"/>
        <source>Image format %1 not supported.</source>
        <translation>Формат картинки %1 не підтримується.</translation>
    </message>
    <message>
        <location filename="qdjviewexporters.cpp" line="1847"/>
        <source>%1 Image</source>
        <comment>JPG Image</comment>
        <translation>%1 Зображення</translation>
    </message>
    <message>
        <location filename="qdjviewexporters.cpp" line="1848"/>
        <source>%1 Files (*.%2)</source>
        <comment>JPG Files</comment>
        <translation>%1 Файлів (*.%2)</translation>
    </message>
</context>
<context>
    <name>QDjViewInfoDialog</name>
    <message>
        <location filename="qdjviewinfodialog.ui" line="16"/>
        <source>Dialog</source>
        <translation>Діалог</translation>
    </message>
    <message>
        <location filename="qdjviewinfodialog.ui" line="29"/>
        <source>&amp;Document</source>
        <translation>&amp;Документ</translation>
    </message>
    <message>
        <location filename="qdjviewinfodialog.ui" line="88"/>
        <source>&amp;File</source>
        <translation>&amp;Файл</translation>
    </message>
    <message>
        <location filename="qdjviewinfodialog.ui" line="108"/>
        <source>File: </source>
        <translation>Файл: </translation>
    </message>
    <message>
        <location filename="qdjviewinfodialog.ui" line="154"/>
        <source>&amp;View Page</source>
        <translation>&amp;Переглянути сторінку</translation>
    </message>
    <message>
        <location filename="qdjviewinfodialog.ui" line="204"/>
        <source>&amp;Close</source>
        <translation>&amp;Закрити</translation>
    </message>
    <message>
        <location filename="qdjviewdialogs.cpp" line="225"/>
        <source>File #</source>
        <translation>Файл №</translation>
    </message>
    <message>
        <location filename="qdjviewdialogs.cpp" line="225"/>
        <source>File Name</source>
        <translation>Назва файла</translation>
    </message>
    <message>
        <location filename="qdjviewdialogs.cpp" line="226"/>
        <source>File Size</source>
        <translation>Розмір файла</translation>
    </message>
    <message>
        <location filename="qdjviewdialogs.cpp" line="226"/>
        <source>File Type</source>
        <translation>Тип файла</translation>
    </message>
    <message>
        <location filename="qdjviewdialogs.cpp" line="227"/>
        <source>Page #</source>
        <translation>Сторінка №</translation>
    </message>
    <message>
        <location filename="qdjviewdialogs.cpp" line="227"/>
        <source>Page Title</source>
        <translation>Заголовок сторінки</translation>
    </message>
    <message>
        <location filename="qdjviewdialogs.cpp" line="259"/>
        <source>&lt;html&gt;&lt;b&gt;Document information&lt;/b&gt;&lt;br&gt;This panel shows information about the document and its component files. Select a component file to display detailled information in the &lt;tt&gt;File&lt;/tt&gt; tab. Double click a component file to show the corresponding page in the main window. &lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;b&gt;Інформація про документ&lt;/b&gt;&lt;br&gt;Ця панель показує інформацію про документ та файли, з яких його складено. Оберіть файл-компонент, щоб побачити детальну інформацію на сторінці &lt;tt&gt;Файл&lt;/tt&gt;. Двічі клацніть на файлі-компоненті, щоб побачити відповідну сторінку у головному вікні. &lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="qdjviewdialogs.cpp" line="267"/>
        <source>&lt;html&gt;&lt;b&gt;File and page information&lt;/b&gt;&lt;br&gt;This panel shows the structure of the DjVu data corresponding to the component file or the page selected in the &lt;tt&gt;Document&lt;/tt&gt; tab. The arrow buttons jump to the previous or next component file.&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;b&gt;Інформація про файл та сторінку&lt;/b&gt;&lt;br&gt;Ця панель показує структуру даних DjVu, що відповідають файлу-компонентові або сторінці, обраній на сторінці &lt;tt&gt;Документ&lt;/tt&gt;. Кнопки зі стрілками дозволять перейти до попереднього чи наступного файла-компонента.&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="qdjviewdialogs.cpp" line="321"/>
        <source>Waiting for data...</source>
        <translation>Очікування на дані...</translation>
    </message>
    <message>
        <location filename="qdjviewdialogs.cpp" line="437"/>
        <source>Page #%1 - « %2 »</source>
        <translation>Сторінка №%1 - « %2 »</translation>
    </message>
    <message>
        <location filename="qdjviewdialogs.cpp" line="440"/>
        <source>Page #%1</source>
        <translation>Сторінка №%1</translation>
    </message>
    <message>
        <location filename="qdjviewdialogs.cpp" line="443"/>
        <source>Thumbnails</source>
        <translation>Піктограми</translation>
    </message>
    <message>
        <location filename="qdjviewdialogs.cpp" line="445"/>
        <source>Shared annotations</source>
        <translation>Спільні анотації</translation>
    </message>
    <message>
        <location filename="qdjviewdialogs.cpp" line="447"/>
        <source>Shared data</source>
        <translation>Спільні дані</translation>
    </message>
    <message>
        <location filename="qdjviewdialogs.cpp" line="448"/>
        <source>File #%1 - </source>
        <translation>Файл №%1 - </translation>
    </message>
    <message>
        <location filename="qdjviewdialogs.cpp" line="461"/>
        <source>Single DjVu page</source>
        <translation>Одна сторінка DjVu</translation>
    </message>
    <message>
        <location filename="qdjviewdialogs.cpp" line="465"/>
        <source>Bundled DjVu document</source>
        <translation>З&apos;єднаний документ DjVu</translation>
    </message>
    <message>
        <location filename="qdjviewdialogs.cpp" line="467"/>
        <source>Indirect DjVu document</source>
        <translation>Непрямий документ DjVu</translation>
    </message>
    <message>
        <location filename="qdjviewdialogs.cpp" line="469"/>
        <source>Obsolete bundled DjVu document</source>
        <translation>Застарілий з&apos;єднаний документ DjVu</translation>
    </message>
    <message>
        <location filename="qdjviewdialogs.cpp" line="471"/>
        <source>Obsolete indexed DjVu document</source>
        <translation>Застарілий індексований документ DjVu</translation>
    </message>
    <message>
        <location filename="qdjviewdialogs.cpp" line="509"/>
        <location filename="qdjviewdialogs.cpp" line="516"/>
        <source>n/a</source>
        <translation>недоступне</translation>
    </message>
    <message>
        <location filename="qdjviewdialogs.cpp" line="523"/>
        <source> Page </source>
        <translation> Сторінка </translation>
    </message>
    <message>
        <location filename="qdjviewdialogs.cpp" line="525"/>
        <source> Thumbnails </source>
        <translation> Мініатюри </translation>
    </message>
    <message>
        <location filename="qdjviewdialogs.cpp" line="527"/>
        <source> Shared </source>
        <translation> Спільні </translation>
    </message>
    <message>
        <location filename="qdjviewdialogs.cpp" line="475"/>
        <source>1 file</source>
        <translation>1 файл</translation>
    </message>
    <message numerus="yes">
        <location filename="qdjviewdialogs.cpp" line="475"/>
        <source>%n files</source>
        <translation>
            <numerusform>%n файл</numerusform>
            <numerusform>%n файли</numerusform>
            <numerusform>%n файлів</numerusform>
        </translation>
    </message>
    <message>
        <location filename="qdjviewdialogs.cpp" line="476"/>
        <source>1 page</source>
        <translation>1 сторінка</translation>
    </message>
    <message numerus="yes">
        <location filename="qdjviewdialogs.cpp" line="476"/>
        <source>%n pages</source>
        <translation>
            <numerusform>%n сторінка</numerusform>
            <numerusform>%n сторінки</numerusform>
            <numerusform>%n сторінок</numerusform>
        </translation>
    </message>
</context>
<context>
    <name>QDjViewMetaDialog</name>
    <message>
        <location filename="qdjviewmetadialog.ui" line="16"/>
        <source>Dialog</source>
        <translation>Діалог</translation>
    </message>
    <message>
        <location filename="qdjviewmetadialog.ui" line="29"/>
        <source>&amp;Document Metadata</source>
        <translation>Метадані &amp;документу</translation>
    </message>
    <message>
        <location filename="qdjviewmetadialog.ui" line="55"/>
        <source>&amp;Page Metadata</source>
        <translation>Метадані &amp;сторінки</translation>
    </message>
    <message>
        <location filename="qdjviewmetadialog.ui" line="75"/>
        <source>Page:</source>
        <translation>Сторінка:</translation>
    </message>
    <message>
        <location filename="qdjviewmetadialog.ui" line="121"/>
        <source>&amp;View Page</source>
        <translation>&amp;Переглянути сторінку</translation>
    </message>
    <message>
        <location filename="qdjviewmetadialog.ui" line="168"/>
        <source>&amp;Close</source>
        <translation>&amp;Закрити</translation>
    </message>
    <message>
        <location filename="qdjviewdialogs.cpp" line="597"/>
        <source> Key </source>
        <translation> Ключ </translation>
    </message>
    <message>
        <location filename="qdjviewdialogs.cpp" line="597"/>
        <source> Value </source>
        <translation> Значення </translation>
    </message>
    <message>
        <location filename="qdjviewdialogs.cpp" line="633"/>
        <source>&lt;html&gt;&lt;b&gt;Document metadata&lt;/b&gt;&lt;br&gt;This panel displays metadata pertaining to the document, such as author, title, references, etc. This information can be saved into the document with program &lt;tt&gt;djvused&lt;/tt&gt;: use the commands &lt;tt&gt;create-shared-ant&lt;/tt&gt; and &lt;tt&gt;set-meta&lt;/tt&gt;.&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;b&gt;Метадані документа&lt;/b&gt;&lt;br&gt;Ця панель відображає метадані, що стосуються документа, такі як автор, назва, посилання та інші. Цю інформацію може бути збережено у документі за допомогою програми &lt;tt&gt;djvused&lt;/tt&gt;: використовуйте команди &lt;tt&gt;create-shared-ant&lt;/tt&gt; та &lt;tt&gt;set-meta&lt;/tt&gt;.&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="qdjviewdialogs.cpp" line="642"/>
        <source>&lt;html&gt;&lt;b&gt;Page metadata&lt;/b&gt;&lt;br&gt;This panel displays metadata pertaining to a specific page. Page specific metadata override document metadata. This information can be saved into the document with program &lt;tt&gt;djvused&lt;/tt&gt;: use command &lt;tt&gt;select&lt;/tt&gt; to select the page and command &lt;tt&gt;set-meta&lt;/tt&gt; to specify the metadata entries.&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;b&gt;Метадані сторінки&lt;/b&gt;&lt;br&gt;Ця панель відображає метадані, що стосуються обраної сторінки. Особливі метадані сторінки заміщують метадані документа. Цю інформацію може бути додано до документа за допомогою програми &lt;tt&gt;djvused&lt;/tt&gt;: використовуйте команду &lt;tt&gt;select&lt;/tt&gt; для вибору сторінки та команду &lt;tt&gt;set-meta&lt;/tt&gt; для визначення пунктів метаданих.&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="qdjviewdialogs.cpp" line="594"/>
        <source>Ctrl+C</source>
        <comment>copy</comment>
        <translation>Ctrl+C</translation>
    </message>
</context>
<context>
    <name>QDjViewOutline</name>
    <message>
        <location filename="qdjviewsidebar.cpp" line="113"/>
        <source>&lt;html&gt;&lt;b&gt;Document outline.&lt;/b&gt;&lt;br/&gt; This panel display the document outline, or the page names when the outline is not available, Double-click any entry to jump to the selected page.&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;b&gt;Ескіз документа.&lt;/b&gt;&lt;br/&gt; Ця панель показує ескіз документа або назви сторінок, коли ескіз недоступний. Двічі клацніть по будь-якому пункту для переходу до обраної сторінки.&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="qdjviewsidebar.cpp" line="146"/>
        <source>Outline data is corrupted</source>
        <translation>Дані ескізу зіпсовано</translation>
    </message>
    <message>
        <location filename="qdjviewsidebar.cpp" line="165"/>
        <source>Pages</source>
        <translation>Сторінки</translation>
    </message>
    <message>
        <location filename="qdjviewsidebar.cpp" line="172"/>
        <location filename="qdjviewsidebar.cpp" line="207"/>
        <source>Page %1</source>
        <translation>Сторінка %1</translation>
    </message>
    <message>
        <location filename="qdjviewsidebar.cpp" line="175"/>
        <location filename="qdjviewsidebar.cpp" line="215"/>
        <source>Go to page %1</source>
        <translation>Перехід до сторінки %1</translation>
    </message>
</context>
<context>
    <name>QDjViewPSExporter</name>
    <message>
        <location filename="qdjviewexporters.cpp" line="610"/>
        <source>&lt;html&gt;&lt;b&gt;PostScript options.&lt;/b&gt;&lt;br&gt;Option &lt;tt&gt;Color&lt;/tt&gt; enables color printing. Document pages can be decorated with frame and crop marks. PostScript language level 1 is only useful with very old printers. Level 2 works with most printers. Level 3 print color document faster on recent printers.&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;b&gt;Параметри PostScript.&lt;/b&gt;&lt;br&gt;Параметр &lt;tt&gt;Колір&lt;/tt&gt; дозволяє друк у кольорі. Сторінки документа можна прикрасити рамкою та обмежувальними мітками. Рівень мови PostScript 1 корисний лише для дуже старих друкарок. Рівень 2 працює для більшості друкарок. Рівень 3 дозволяє пришвидшити друк на сучасних друкаркам.&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="qdjviewexporters.cpp" line="618"/>
        <source>&lt;html&gt;&lt;b&gt;Position and scaling.&lt;/b&gt;&lt;br&gt;Option &lt;tt&gt;Scale to fit&lt;/tt&gt; accommodates whatever paper size your printer uses. Zoom factor &lt;tt&gt;100%&lt;/tt&gt; reproduces the initial document size. Orientation &lt;tt&gt;Automatic&lt;/tt&gt; chooses portrait or landscape on a page per page basis.&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;b&gt;Розміщення та масштаб.&lt;/b&gt;&lt;br&gt;Параметр &lt;tt&gt;Масшатабувати для відповідності&lt;/tt&gt; пристосовує документ до того розміру паперу, який використовує Ваша друкарка. Множник масштабу &lt;tt&gt;100%&lt;/tt&gt; відтворює початковий розмір сторінок документа. &lt;tt&gt;Автоматична&lt;/tt&gt; орієнтація дозволяє програмі обрати книжкову чи альбомну орієнтацію паперу на основі припущення одна сторінка - один листок.&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="qdjviewexporters.cpp" line="625"/>
        <source>&lt;html&gt;&lt;b&gt;Producing booklets.&lt;/b&gt;&lt;br&gt;The booklet mode prints the selected pages as sheets suitable for folding one or several booklets. Several booklets might be produced when a maximum number of sheets per booklet is specified. You can either use a duplex printer or print rectos and versos separately.&lt;p&gt; Shifting rectos and versos is useful with poorly aligned duplex printers. The center margins determine how much space is left between the pages to fold the sheets. This space slowly increases from the inner sheet to the outer sheet.&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;b&gt;Виготовлення буклетів.&lt;/b&gt;&lt;br&gt;Режим буклету дозволяє друк обраних сторінок у вигляді листів придатних для виготовлення одного чи декількох буклетів. Декілька буклетів буде виготовлено, якщо зазначена максимальна кількість листів у буклеті. Ви можете використовувати двобічну друкарку або друкувати верхні та нижні боки сторінки окремо.&lt;p&gt; Зсув верхніх та нижніх боків може бути корисний для двобічних друкарок з неякісним вирівнюванням. Центральне поле визначає як багато місця буде залишено між сторінками для наступного згортання у буклет. Ця відстань повільно зростає від внутрішніх листів до зовнішніх.&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="qdjviewexporters.cpp" line="1116"/>
        <source>Save job creation failed!</source>
        <translation>Створення процесу збереження зазнало невдачі!</translation>
    </message>
    <message>
        <location filename="qdjviewexporters.cpp" line="544"/>
        <source>PostScript</source>
        <translation>PostScript</translation>
    </message>
    <message>
        <location filename="qdjviewexporters.cpp" line="545"/>
        <location filename="qdjviewexporters.cpp" line="549"/>
        <source>PostScript Files (*.ps *.eps)</source>
        <translation>Файлів PostScript (*.ps)</translation>
    </message>
    <message>
        <location filename="qdjviewexporters.cpp" line="548"/>
        <source>Encapsulated PostScript</source>
        <translation>Інкапсульований PostScript</translation>
    </message>
    <message>
        <location filename="qdjviewexporters.cpp" line="598"/>
        <source>PostScript</source>
        <comment>tab caption</comment>
        <translation>PostScript</translation>
    </message>
    <message>
        <location filename="qdjviewexporters.cpp" line="599"/>
        <source>Position</source>
        <comment>tab caption</comment>
        <translation>Розміщення</translation>
    </message>
    <message>
        <location filename="qdjviewexporters.cpp" line="600"/>
        <source>Booklet</source>
        <comment>tab caption</comment>
        <translation>Буклет</translation>
    </message>
</context>
<context>
    <name>QDjViewPdfExporter</name>
    <message>
        <location filename="qdjviewexporters.cpp" line="1692"/>
        <source>PDF Document</source>
        <translation>Документ PDF</translation>
    </message>
    <message>
        <location filename="qdjviewexporters.cpp" line="1693"/>
        <source>PDF Files (*.pdf)</source>
        <translation>Файли PDF (*.pdf)</translation>
    </message>
    <message>
        <location filename="qdjviewexporters.cpp" line="1702"/>
        <source>PDF Options</source>
        <comment>tab caption</comment>
        <translation>Параметри PDF</translation>
    </message>
    <message>
        <location filename="qdjviewexporters.cpp" line="1703"/>
        <source>&lt;html&gt;&lt;b&gt;PDF options.&lt;/b&gt;&lt;br&gt;These options control the characteristics of the images embedded in the exported PDF files. The resolution box limits their maximal resolution. Forcing bitonal G4 compression encodes all pages in black and white using the CCITT Group 4 compression. Allowing JPEG compression uses lossy JPEG for all non bitonal or subsampled images. Otherwise, allowing deflate compression produces more compact files. &lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;b&gt;Параметри PDF.&lt;/b&gt;&lt;br&gt;Ці параметри впливають на характеристики зображень, вбудованих до файлів PDF, що експортуються. Поле \&quot;роздільна здатність\&quot; обмежує максимальну роздільну здатність. Примусове двотональне стиснення G4 кодує всі сторінки у чорно-білому вигляді за допомогою методу стиснення CCITT Group 4. Використання стиснення JPEG дозволяє стискати за допомогою стиснення з втратами JPEG всі не чорно-білі зображення або зображення з підвибірками. Або ж дозвіл на стиснення призводить до отримання компактніших файлів. &lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="qdjviewexporters.cpp" line="1747"/>
        <source>Error while creating pdf file.</source>
        <translation>Помилка під час створення файла pdf.</translation>
    </message>
    <message>
        <location filename="qdjviewexporters.cpp" line="1769"/>
        <source>PDF export was not compiled.</source>
        <translation>Експорт до PDF не було використано під час збирання програми.</translation>
    </message>
    <message>
        <location filename="qdjviewexporters.cpp" line="1751"/>
        <source>Unable to create output file.</source>
        <translation>Неможливо створити файл виведення.</translation>
    </message>
    <message>
        <location filename="qdjviewexporters.cpp" line="1753"/>
        <location filename="qdjviewexporters.cpp" line="1795"/>
        <source>System error: %1.</source>
        <translation>Системна помилка: %1.</translation>
    </message>
    <message>
        <location filename="qdjviewexporters.cpp" line="1756"/>
        <source>Unable to reopen temporary file.</source>
        <translation>Неможливо знову відкрити тимчасовий файл.</translation>
    </message>
    <message>
        <location filename="qdjviewexporters.cpp" line="1793"/>
        <source>Unable to create temporary file.</source>
        <translation>Неможливо створити тимчасовий файл.</translation>
    </message>
</context>
<context>
    <name>QDjViewPlugin::Document</name>
    <message>
        <location filename="qdjviewplugin.cpp" line="301"/>
        <source>Requesting %1.</source>
        <translation>Запит %1.</translation>
    </message>
</context>
<context>
    <name>QDjViewPrefsDialog</name>
    <message>
        <location filename="qdjviewprefsdialog.ui" line="13"/>
        <source>Dialog</source>
        <translation>Діалог</translation>
    </message>
    <message>
        <location filename="qdjviewprefsdialog.ui" line="40"/>
        <source>Gamma</source>
        <translation>Гама</translation>
    </message>
    <message>
        <location filename="qdjviewprefsdialog.ui" line="109"/>
        <source>Darker</source>
        <translation>Темніше</translation>
    </message>
    <message>
        <location filename="qdjviewprefsdialog.ui" line="129"/>
        <source>Lighter</source>
        <translation>Світліше</translation>
    </message>
    <message>
        <location filename="qdjviewprefsdialog.ui" line="60"/>
        <source>&lt;html&gt;Screen color correction.&lt;br&gt;Adjust slider until gray shades look similar.&lt;/html&gt;</source>
        <translation>&lt;html&gt;Коригування екранного кольору.&lt;br&gt;Пересуньте повзунок так, щоб сірі області виглядали однаково.&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="qdjviewprefsdialog.ui" line="261"/>
        <source>&amp;Interface</source>
        <translation>&amp;Вигляд</translation>
    </message>
    <message>
        <location filename="qdjviewprefsdialog.ui" line="275"/>
        <source>Options for</source>
        <translation>Параметри для</translation>
    </message>
    <message>
        <location filename="qdjviewprefsdialog.ui" line="286"/>
        <source>Standalone Viewer</source>
        <translation>Самодостатнього переглядача</translation>
    </message>
    <message>
        <location filename="qdjviewprefsdialog.ui" line="291"/>
        <source>Full Screen Standalone Viewer</source>
        <translation>Повноекранного самодостатнього переглядача</translation>
    </message>
    <message>
        <location filename="qdjviewprefsdialog.ui" line="296"/>
        <source>Full Page Plugin</source>
        <translation>Додатку повної сторінки</translation>
    </message>
    <message>
        <location filename="qdjviewprefsdialog.ui" line="301"/>
        <source>Embedded Plugin</source>
        <translation>Вбудованого додатку</translation>
    </message>
    <message>
        <location filename="qdjviewprefsdialog.ui" line="324"/>
        <source>&amp;Remember initial state from last invocation</source>
        <translation>&amp;Запам&apos;ятати початковий стан з останнього виклику</translation>
    </message>
    <message>
        <location filename="qdjviewprefsdialog.ui" line="333"/>
        <source>Show</source>
        <translation>Показати</translation>
    </message>
    <message>
        <location filename="qdjviewprefsdialog.ui" line="339"/>
        <source>&amp;Menu bar</source>
        <translation>Панель &amp;меню</translation>
    </message>
    <message>
        <location filename="qdjviewprefsdialog.ui" line="346"/>
        <source>&amp;Tool bar</source>
        <translation>Панель &amp;інструментів</translation>
    </message>
    <message>
        <location filename="qdjviewprefsdialog.ui" line="353"/>
        <source>Sc&amp;rollbars</source>
        <translation>Смуги &amp;гортання</translation>
    </message>
    <message>
        <location filename="qdjviewprefsdialog.ui" line="360"/>
        <source>Stat&amp;us bar</source>
        <translation>Панель &amp;статусу</translation>
    </message>
    <message>
        <location filename="qdjviewprefsdialog.ui" line="367"/>
        <source>&amp;Side bar</source>
        <translation>&amp;Бічний пенал</translation>
    </message>
    <message>
        <location filename="qdjviewprefsdialog.ui" line="377"/>
        <source>Display</source>
        <translation>Вигляд</translation>
    </message>
    <message>
        <location filename="qdjviewprefsdialog.ui" line="385"/>
        <source>&amp;Zoom: </source>
        <translation>&amp;Масштаб: </translation>
    </message>
    <message>
        <location filename="qdjviewprefsdialog.ui" line="452"/>
        <source>Display page &amp;frames</source>
        <translation>Показувати &amp;рамки сторінок</translation>
    </message>
    <message>
        <location filename="qdjviewprefsdialog.ui" line="459"/>
        <source>Display &amp;annotations</source>
        <translation>Показувати &amp;анотації</translation>
    </message>
    <message>
        <location filename="qdjviewprefsdialog.ui" line="628"/>
        <source>&amp;Keys</source>
        <translation>&amp;Клавіші</translation>
    </message>
    <message>
        <location filename="qdjviewprefsdialog.ui" line="640"/>
        <source>Keys</source>
        <translation>Клавіші</translation>
    </message>
    <message>
        <location filename="qdjviewprefsdialog.ui" line="652"/>
        <source>For displaying hyper&amp;links: </source>
        <translation>Для показу гіпер&amp;посилань: </translation>
    </message>
    <message>
        <location filename="qdjviewprefsdialog.ui" line="662"/>
        <source>For &amp;selecting text or images: </source>
        <translation>Для &amp;вибору тексту або картинок: </translation>
    </message>
    <message>
        <location filename="qdjviewprefsdialog.ui" line="672"/>
        <source>For displaying the &amp;lens: </source>
        <translation>Для відо&amp;браження лінзи: </translation>
    </message>
    <message>
        <location filename="qdjviewprefsdialog.ui" line="728"/>
        <source>&amp;Lens</source>
        <translation>&amp;Лінза</translation>
    </message>
    <message>
        <location filename="qdjviewprefsdialog.ui" line="740"/>
        <source>Enable magnifying &amp;lens</source>
        <translation>Дозволити з&amp;більшувальну лінзу</translation>
    </message>
    <message>
        <location filename="qdjviewprefsdialog.ui" line="750"/>
        <source>Lens options</source>
        <translation>Параметри лінзи</translation>
    </message>
    <message>
        <location filename="qdjviewprefsdialog.ui" line="762"/>
        <source>Lens &amp;size: </source>
        <translation>Розмір &amp;лінзи: </translation>
    </message>
    <message>
        <location filename="qdjviewprefsdialog.ui" line="772"/>
        <source>Magnifying &amp;power: </source>
        <translation>&amp;Збільшувальна сила: </translation>
    </message>
    <message>
        <location filename="qdjviewprefsdialog.ui" line="782"/>
        <source>x</source>
        <translation>x</translation>
    </message>
    <message>
        <location filename="qdjviewprefsdialog.ui" line="798"/>
        <source> pixels</source>
        <translation> точок</translation>
    </message>
    <message>
        <location filename="qdjviewprefsdialog.ui" line="970"/>
        <source>Cache</source>
        <translation>Кеш</translation>
    </message>
    <message>
        <location filename="qdjviewprefsdialog.ui" line="982"/>
        <source>&amp;Clear</source>
        <translation>&amp;Очистити</translation>
    </message>
    <message>
        <location filename="qdjviewprefsdialog.ui" line="1021"/>
        <source>&amp;Decoded page cache: </source>
        <translation>Кеш &amp;розкодованих сторінок: </translation>
    </message>
    <message>
        <location filename="qdjviewprefsdialog.ui" line="1031"/>
        <source>&amp;Pixel cache: </source>
        <translation>Кеш &amp;пікселів: </translation>
    </message>
    <message>
        <location filename="qdjviewprefsdialog.ui" line="842"/>
        <source>&amp;Network</source>
        <translation>&amp;Мережа</translation>
    </message>
    <message>
        <location filename="qdjviewprefsdialog.ui" line="854"/>
        <source>Use pro&amp;xy to access the network</source>
        <translation>Використовувати &amp;проксі для доступу до мережі</translation>
    </message>
    <message>
        <location filename="qdjviewprefsdialog.ui" line="864"/>
        <source>Proxy settings</source>
        <translation>Налаштування проксі</translation>
    </message>
    <message>
        <location filename="qdjviewprefsdialog.ui" line="876"/>
        <source>Pass&amp;word: </source>
        <translation>Па&amp;роль: </translation>
    </message>
    <message>
        <location filename="qdjviewprefsdialog.ui" line="886"/>
        <source>&amp;User: </source>
        <translation>&amp;Користувач: </translation>
    </message>
    <message>
        <location filename="qdjviewprefsdialog.ui" line="906"/>
        <source>&amp;Port: </source>
        <translation>&amp;Порт: </translation>
    </message>
    <message>
        <location filename="qdjviewprefsdialog.ui" line="916"/>
        <source>&amp;Host: </source>
        <translation>&amp;Сервер: </translation>
    </message>
    <message>
        <location filename="qdjviewprefsdialog.ui" line="1144"/>
        <source>&amp;Defaults</source>
        <translation>&amp;Типове</translation>
    </message>
    <message>
        <location filename="qdjviewprefsdialog.ui" line="1170"/>
        <source>&amp;Apply</source>
        <translation>&amp;Застосувати</translation>
    </message>
    <message>
        <location filename="qdjviewprefsdialog.ui" line="1177"/>
        <source>&amp;Ok</source>
        <translation>&amp;Гаразд</translation>
    </message>
    <message>
        <location filename="qdjviewprefsdialog.ui" line="1184"/>
        <source>Cancel</source>
        <translation>Скасувати</translation>
    </message>
    <message>
        <location filename="qdjviewprefs.cpp" line="706"/>
        <source>Preferences[*] - DjView</source>
        <translation>Налаштування[*] - DjView</translation>
    </message>
    <message>
        <location filename="qdjviewprefs.cpp" line="789"/>
        <source>&lt;html&gt;&lt;b&gt;Initial interface setup.&lt;/b&gt;&lt;br&gt;DjView can run as a standalone viewer, as a full screen viewer, as a full page browser plugin, or as a plugin embedded inside a html page. For each case, check the &lt;tt&gt;Remember&lt;/tt&gt; box to automatically save and restore the interface setup. Otherwise, specify an initial configuration.&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;b&gt;Початкове налаштування відображення.&lt;/b&gt;&lt;br&gt;DjView може працювати як самодостатній переглядач, як повноекранний переглядач, як додаток переглядача на повну сторінку, як додаток вбудований до сторінки html. У будь-якому випадку, позначте &lt;tt&gt;Запам&apos;ятати&lt;/tt&gt;, щоб автоматично зберігати та відновлювати налаштування відображення. У протилежному випадку зазначте початкову конфігурацію.&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="qdjviewprefs.cpp" line="799"/>
        <source>&lt;html&gt;&lt;b&gt;Modifiers keys.&lt;/b&gt;&lt;br&gt;Define which combination of modifier keys will show the manifying lens, temporarily enable the selection mode, or highlight the hyperlinks.&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;b&gt;Клавіші-модифікатори.&lt;/b&gt;&lt;br&gt;Визначте яка комбінація клавіш-модифікаторів показуватиме збільшувальну лінзу, тимчасово вмикатиме режим вибору або підсвічуватиме гіперпосилання.&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="qdjviewprefs.cpp" line="806"/>
        <source>&lt;html&gt;&lt;b&gt;Magnifying lens.&lt;/b&gt;&lt;br&gt;The magnifying lens appears when you depress the modifier keys specified in tab &lt;tt&gt;Keys&lt;/tt&gt;. This panel lets you choose the power and the size of the magnifying lens.&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;b&gt;Збільшувальна лінза.&lt;/b&gt;&lt;br&gt;Збільшувальна лінза з&apos;являється коли Ви натиснете клавіші-модифікатори задані на сторінці меню &lt;tt&gt;Клавіші&lt;/tt&gt;. Ця панель дозволить Вам обрати силу та розмір збільшувальної лінзи.&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="qdjviewprefs.cpp" line="814"/>
        <source>&lt;html&gt;&lt;b&gt;Caches.&lt;/b&gt;&lt;br&gt;The &lt;i&gt;pixel cache&lt;/i&gt; stores image data located outside the visible area. This cache makes panning smoother. The &lt;i&gt;decoded page cache&lt;/i&gt; contains partially decoded pages. It provides faster response times when navigating a multipage document or when returning to a previously viewed page. Clearing this cache might be useful to reflect a change in the page data without restarting the DjVu viewer.&lt;p&gt;&lt;b&gt;Miscellaneous.&lt;/b&gt;&lt;br&gt;Forcing a manual color correction can be useful when using ancient printers. The advanced features check box enables a small number of additional menu entries useful for authoring DjVu files.&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;b&gt;Кеші.&lt;/b&gt;&lt;br&gt; &lt;i&gt;Кеш пікселів&lt;/i&gt; зберігає дані картинок розташованих поза видимою областю. Цей кеш робить пересування документом плавнішим. &lt;i&gt;Кеш розкодованих сторінок&lt;/i&gt; мстить частково розкодовані сторінки. Він забезпечує швидшу реакцію переглядача під час пересування багатосторінковим документом або під час повернення для раніше переглянутої сторінки. Очищення цього кешу може бути корисним для відображення змін у даних сторінки без перезапуску переглядача DjVu.&lt;p&gt;&lt;b&gt;Різне.&lt;/b&gt;&lt;br&gt;Примусова корекція кольорів вручну може бути корисною, якщо ви користуєтеся застарілим принтером. Поле позначки додаткових можливостей надає вам змогу увімкнути невелику кількість додаткових пунктів меню, корисних для авторської роботи над файлами DjVu.&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="qdjviewprefs.cpp" line="833"/>
        <source>&lt;html&gt;&lt;b&gt;Network proxy settings.&lt;/b&gt;&lt;br&gt;These proxy settings are used when the standalone djview viewer accesses a djvu document through a http url. The djview plugin always uses the proxy settings of the web browser.&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;b&gt;Налаштування проксі сервера.&lt;/b&gt;&lt;br&gt;Ці налаштування проксі сервера буде використано коли самодостатній переглядач djview намагатиметься отримати доступ до документу djvu через адресу http. Додаток djview завжди використовуватиме налаштування проксі сервера, які використовує переглядач сторінок інтернету.&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="qdjviewprefs.cpp" line="776"/>
        <source>&lt;html&gt;&lt;b&gt;Screen gamma correction.&lt;/b&gt;&lt;br&gt;The best color rendition is achieved by adjusting the gamma correction slider and choosing the position that makes the gray square as uniform as possible.&lt;p&gt;&lt;b&gt;Screen resolution.&lt;/b&gt;&lt;br&gt;This option forces a particular resolution instead of using the unreliable resolution advertised by the operating system. Forcing the resolution to 100 dpi matches the behavior of the djvulibre command line tools.&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;b&gt;Коригування гамми кольорів екрана.&lt;/b&gt;&lt;br&gt;Найкращого відтворення кольорів можна досягти за допомогою коригування гами, яке можна виконати повзунком коригування гами, обравши таке його розташування, за якого сірий квадратик стає якомога більш однорідним.&lt;p&gt;&lt;b&gt;Роздільна здатність.&lt;/b&gt;&lt;br&gt;Цей параметр призначено для визначення роздільної здатності, яка не залежатиме від параметрів, заданих операційною системою. Визначення роздільної здатності у 100 т/д відповідає типовій поведінці інструментів командного рядка з djvulibre.&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="qdjviewprefsdialog.ui" line="34"/>
        <source>&amp;Screen</source>
        <translation>&amp;Екран</translation>
    </message>
    <message>
        <location filename="qdjviewprefsdialog.ui" line="198"/>
        <source>Resolution</source>
        <translation>Роздільна здатність</translation>
    </message>
    <message>
        <location filename="qdjviewprefsdialog.ui" line="204"/>
        <source>Force screen &amp;resolution</source>
        <translation>Лише в&amp;казана роздільна здатність</translation>
    </message>
    <message>
        <location filename="qdjviewprefsdialog.ui" line="233"/>
        <source> dpi</source>
        <translation> т/д</translation>
    </message>
    <message>
        <location filename="qdjviewprefsdialog.ui" line="964"/>
        <source>&amp;Advanced</source>
        <translation>&amp;Додатково</translation>
    </message>
    <message>
        <location filename="qdjviewprefsdialog.ui" line="1044"/>
        <source>Miscellaneous</source>
        <translation>Різне</translation>
    </message>
    <message>
        <location filename="qdjviewprefsdialog.ui" line="1052"/>
        <source>Manual printer color correction</source>
        <translation>Корекція кольорів друку вручну</translation>
    </message>
    <message>
        <location filename="qdjviewprefsdialog.ui" line="1065"/>
        <source>gamma=</source>
        <translation>гамма=</translation>
    </message>
    <message>
        <location filename="qdjviewprefsdialog.ui" line="1102"/>
        <source>Enable advanced features in menus</source>
        <translation>Увімкнути додаткові можливості у меню</translation>
    </message>
    <message>
        <location filename="qdjviewprefsdialog.ui" line="992"/>
        <location filename="qdjviewprefsdialog.ui" line="1005"/>
        <source> MB</source>
        <translation> MB</translation>
    </message>
    <message>
        <location filename="qdjviewprefsdialog.ui" line="1109"/>
        <source>Show hidden text in status bar</source>
        <translation>Показувати прихований текст у рядку стану</translation>
    </message>
    <message>
        <location filename="qdjviewprefsdialog.ui" line="495"/>
        <source>Layout</source>
        <translation>Компонування</translation>
    </message>
    <message>
        <location filename="qdjviewprefsdialog.ui" line="501"/>
        <source>Continuous</source>
        <translation>Неперервно</translation>
    </message>
    <message>
        <location filename="qdjviewprefsdialog.ui" line="553"/>
        <source>Side-by-side</source>
        <translation>Сторінки поруч</translation>
    </message>
    <message>
        <location filename="qdjviewprefsdialog.ui" line="583"/>
        <source>Cover Page</source>
        <translation>Обкладинка</translation>
    </message>
    <message>
        <location filename="qdjviewprefsdialog.ui" line="590"/>
        <source>Right-to-Left</source>
        <translation>Справа ліворуч</translation>
    </message>
</context>
<context>
    <name>QDjViewPrintDialog</name>
    <message>
        <location filename="qdjviewprintdialog.ui" line="13"/>
        <source>Dialog</source>
        <translation>Діалог</translation>
    </message>
    <message>
        <location filename="qdjviewprintdialog.ui" line="29"/>
        <location filename="qdjviewprintdialog.ui" line="41"/>
        <source>Print</source>
        <translation>Друк</translation>
    </message>
    <message>
        <location filename="qdjviewprintdialog.ui" line="53"/>
        <source>&amp;Document</source>
        <translation>&amp;Документ</translation>
    </message>
    <message>
        <location filename="qdjviewprintdialog.ui" line="63"/>
        <source>C&amp;urrent page</source>
        <translation>П&amp;оточна сторінка</translation>
    </message>
    <message>
        <location filename="qdjviewprintdialog.ui" line="78"/>
        <source>&amp;Pages</source>
        <translation>&amp;Сторінки</translation>
    </message>
    <message>
        <location filename="qdjviewprintdialog.ui" line="88"/>
        <source>to</source>
        <translation>до</translation>
    </message>
    <message>
        <location filename="qdjviewprintdialog.ui" line="122"/>
        <source>Destination</source>
        <translation>Призначення</translation>
    </message>
    <message>
        <location filename="qdjviewprintdialog.ui" line="238"/>
        <source>&amp;Defaults</source>
        <translation>&amp;Типове</translation>
    </message>
    <message>
        <location filename="qdjviewprintdialog.ui" line="264"/>
        <source>&amp;Ok</source>
        <translation>&amp;Гаразд</translation>
    </message>
    <message>
        <location filename="qdjviewprintdialog.ui" line="280"/>
        <location filename="qdjviewdialogs.cpp" line="1740"/>
        <source>&amp;Cancel</source>
        <translation>&amp;Скасувати</translation>
    </message>
    <message>
        <location filename="qdjviewprintdialog.ui" line="307"/>
        <source>Stop</source>
        <translation>Зупинити</translation>
    </message>
    <message>
        <location filename="qdjviewdialogs.cpp" line="1525"/>
        <source>&lt;html&gt;&lt;b&gt;Printing.&lt;/b&gt;&lt;br/&gt; You can print the whole document or a page range. Use the &lt;tt&gt;Choose&lt;/tt&gt; button to select a print destination and specify printer options. Additional dialog tabs might appear to specify conversion options.&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;b&gt;Друк.&lt;/b&gt;&lt;br/&gt; Ви можете надрукувати весь документ або проміжок сторінок. Використовуйте кнопку &lt;tt&gt;Обрати&lt;/tt&gt; щоб обрати призначення та задати параметри друкарки. Додаткові сторінки діалогу з&apos;являються для того, щоб задати параметри перетворення.&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="qdjviewdialogs.cpp" line="1779"/>
        <source>This operation has failed.</source>
        <translation>Ця операція зазнала невдачі.</translation>
    </message>
    <message>
        <location filename="qdjviewdialogs.cpp" line="1783"/>
        <source>This operation has been interrupted.</source>
        <translation>Цю операцію було перервано.</translation>
    </message>
    <message>
        <location filename="qdjviewprintdialog.ui" line="134"/>
        <source>Print to file</source>
        <translation>Друк до файла</translation>
    </message>
    <message>
        <location filename="qdjviewprintdialog.ui" line="182"/>
        <source>Printer name: </source>
        <translation>Назва принтера: </translation>
    </message>
    <message>
        <location filename="qdjviewprintdialog.ui" line="207"/>
        <source>Choose</source>
        <translation>Обрати</translation>
    </message>
    <message>
        <location filename="qdjviewdialogs.cpp" line="1658"/>
        <source>Print To File - DjView</source>
        <comment>dialog caption</comment>
        <translation>Друк до файла - DjView</translation>
    </message>
    <message>
        <location filename="qdjviewdialogs.cpp" line="1737"/>
        <source>A file with this name already exists.
Do you want to replace it?</source>
        <translation>Файл з такою назвою вже існує.
Бажаєте замінити його?</translation>
    </message>
    <message>
        <location filename="qdjviewdialogs.cpp" line="1739"/>
        <source>&amp;Replace</source>
        <translation>&amp;Замінити</translation>
    </message>
    <message>
        <location filename="qdjviewdialogs.cpp" line="1651"/>
        <source>All files</source>
        <comment>save filter</comment>
        <translation>Всі файли</translation>
    </message>
    <message>
        <location filename="qdjviewdialogs.cpp" line="1736"/>
        <source>Question - DjView</source>
        <comment>dialog caption</comment>
        <translation>Запитання - DjView</translation>
    </message>
    <message>
        <location filename="qdjviewdialogs.cpp" line="1619"/>
        <source>(invalid printer)</source>
        <translation>(некоректний принтер)</translation>
    </message>
</context>
<context>
    <name>QDjViewPrnExporter</name>
    <message>
        <location filename="qdjviewexporters.cpp" line="1991"/>
        <source>Printer data</source>
        <translation>Дані друкарки</translation>
    </message>
    <message>
        <location filename="qdjviewexporters.cpp" line="1992"/>
        <source>PRN Files (*.prn)</source>
        <translation>Файли PRN (*.prn)</translation>
    </message>
    <message>
        <location filename="qdjviewexporters.cpp" line="2012"/>
        <source>Printing Options</source>
        <comment>tab caption</comment>
        <translation>Параметри друку</translation>
    </message>
    <message>
        <location filename="qdjviewexporters.cpp" line="2014"/>
        <source>&lt;html&gt;&lt;b&gt;Printing options.&lt;/b&gt;&lt;br&gt;Option &lt;tt&gt;Color&lt;/tt&gt; enables color printing. Document pages can be decorated with a frame. Option &lt;tt&gt;Scale to fit&lt;/tt&gt; accommodates whatever paper size your printer uses. Zoom factor &lt;tt&gt;100%&lt;/tt&gt; reproduces the initial document size. Orientation &lt;tt&gt;Automatic&lt;/tt&gt; chooses portrait or landscape on a page per page basis.&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;b&gt;Параметри друку.&lt;/b&gt;&lt;br&gt;Параметр &lt;tt&gt;Колір&lt;/tt&gt; дозволяє кольоровий друк. Сторінки документа можна вкласти до рамок. Параметр &lt;tt&gt;Підігнати до&lt;/tt&gt; пристосовує сторінки до розміру паперу, що використовує Ваша друкарка. Множник масштабування &lt;tt&gt;100%&lt;/tt&gt; відтворює початковий розмір документа. Розташування &lt;tt&gt;Автоматичне&lt;/tt&gt; обирає книжкове чи альбомне розташування з міркувань відображення однієї сторінки на одному листі.&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="qdjviewexporters.cpp" line="2266"/>
        <source>Cannot render page %1.</source>
        <translation>Відображення сторінки неможливе «%1».</translation>
    </message>
</context>
<context>
    <name>QDjViewSaveDialog</name>
    <message>
        <location filename="qdjviewsavedialog.ui" line="13"/>
        <source>Dialog</source>
        <translation>Діалог</translation>
    </message>
    <message>
        <location filename="qdjviewsavedialog.ui" line="25"/>
        <source>Save</source>
        <translation>Зберегти</translation>
    </message>
    <message>
        <location filename="qdjviewsavedialog.ui" line="37"/>
        <source>&amp;Document</source>
        <translation>&amp;Документ</translation>
    </message>
    <message>
        <location filename="qdjviewsavedialog.ui" line="47"/>
        <source>C&amp;urrent page</source>
        <translation>П&amp;оточна сторінка</translation>
    </message>
    <message>
        <location filename="qdjviewsavedialog.ui" line="62"/>
        <source>&amp;Pages</source>
        <translation>&amp;Сторінки</translation>
    </message>
    <message>
        <location filename="qdjviewsavedialog.ui" line="72"/>
        <source>to</source>
        <translation>до</translation>
    </message>
    <message>
        <location filename="qdjviewsavedialog.ui" line="106"/>
        <source>Destination</source>
        <translation>Призначення</translation>
    </message>
    <message>
        <location filename="qdjviewsavedialog.ui" line="134"/>
        <source>Format:</source>
        <translation>Формат:</translation>
    </message>
    <message>
        <location filename="qdjviewsavedialog.ui" line="206"/>
        <source>&amp;Ok</source>
        <translation>&amp;Гаразд</translation>
    </message>
    <message>
        <location filename="qdjviewsavedialog.ui" line="222"/>
        <location filename="qdjviewdialogs.cpp" line="986"/>
        <source>&amp;Cancel</source>
        <translation>&amp;Скасувати</translation>
    </message>
    <message>
        <location filename="qdjviewsavedialog.ui" line="249"/>
        <source>Stop</source>
        <translation>Зупинити</translation>
    </message>
    <message>
        <location filename="qdjviewdialogs.cpp" line="977"/>
        <source>Overwriting the current file is not allowed!</source>
        <translation>Перезапис поточного файла заборонено!</translation>
    </message>
    <message>
        <location filename="qdjviewdialogs.cpp" line="983"/>
        <source>A file with this name already exists.
Do you want to replace it?</source>
        <translation>Файл з такою назвою вже існує.
Бажаєте замінити його?</translation>
    </message>
    <message>
        <location filename="qdjviewdialogs.cpp" line="985"/>
        <source>&amp;Replace</source>
        <translation>&amp;Замінити</translation>
    </message>
    <message>
        <location filename="qdjviewdialogs.cpp" line="1023"/>
        <source>This operation has failed.</source>
        <translation>Ця операція зазнала невдачі.</translation>
    </message>
    <message>
        <location filename="qdjviewdialogs.cpp" line="1027"/>
        <source>This operation has been interrupted.</source>
        <translation>Цю операцію було перервано.</translation>
    </message>
    <message>
        <location filename="qdjviewsavedialog.ui" line="142"/>
        <source>Bundled DjVu Document</source>
        <translation>Зв&apos;язаний документ DjVu</translation>
    </message>
    <message>
        <location filename="qdjviewsavedialog.ui" line="147"/>
        <source>Indirect DjVu Document</source>
        <translation>Непрямий документ DjVu</translation>
    </message>
    <message>
        <location filename="qdjviewdialogs.cpp" line="887"/>
        <source>&lt;html&gt;&lt;b&gt;Saving.&lt;/b&gt;&lt;br/&gt; You can save the whole document or a page range. The bundled format creates a single file. The indirect format creates multiple files suitable for web serving.&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;b&gt;Збереження.&lt;/b&gt;&lt;br/&gt; Ви можете зберегти увесь документ або діапазон сторінок. Зв&apos;язаний формат створює одиночний файл. Непрямий формат створює декілька файлів придатних до перегляду у мережі.&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="qdjviewdialogs.cpp" line="976"/>
        <source>Error - DjView</source>
        <comment>dialog caption</comment>
        <translation>Помилка - DjView</translation>
    </message>
    <message>
        <location filename="qdjviewdialogs.cpp" line="982"/>
        <source>Question - DjView</source>
        <comment>dialog caption</comment>
        <translation>Запитання - DjView</translation>
    </message>
    <message>
        <location filename="qdjviewdialogs.cpp" line="1054"/>
        <source>All files</source>
        <comment>save filter</comment>
        <translation>Всі файли</translation>
    </message>
    <message>
        <location filename="qdjviewdialogs.cpp" line="1061"/>
        <source>Save - DjView</source>
        <comment>dialog caption</comment>
        <translation>Збереження - DjView</translation>
    </message>
</context>
<context>
    <name>QDjViewThumbnails</name>
    <message>
        <location filename="qdjviewsidebar.cpp" line="719"/>
        <source>Control Left Mouse Button</source>
        <translation>Контроль правою кнопкою миші</translation>
    </message>
    <message>
        <location filename="qdjviewsidebar.cpp" line="721"/>
        <source>Right Mouse Button</source>
        <translation>Права кнопка миші</translation>
    </message>
    <message>
        <location filename="qdjviewsidebar.cpp" line="723"/>
        <source>&lt;html&gt;&lt;b&gt;Document thumbnails.&lt;/b&gt;&lt;br/&gt; This panel display thumbnails for the document pages. Double click a thumbnail to jump to the selected page. %1 to change the thumbnail size or the refresh mode. The smart refresh mode only computes thumbnails when the page data is present (displayed or cached.)&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;b&gt;Мініатюри документа.&lt;/b&gt;&lt;br/&gt; Ця панель показує мініатюри сторінок документа. Подвійне клацання на мініатюрі відкриє у вікні перегляду обрану сторінку. %1 дозволить змінити розмір мініатюр або оновить їх. Режим розумного оновлення відтворюватиме мініатюри тільки у разі наявності даних сторінки (відображених або кешованих.)&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="qdjviewsidebar.cpp" line="691"/>
        <source>Tiny</source>
        <comment>thumbnail menu</comment>
        <translation>Малюсінький</translation>
    </message>
    <message>
        <location filename="qdjviewsidebar.cpp" line="696"/>
        <source>Small</source>
        <comment>thumbnail menu</comment>
        <translation>Маленький</translation>
    </message>
    <message>
        <location filename="qdjviewsidebar.cpp" line="701"/>
        <source>Medium</source>
        <comment>thumbnail menu</comment>
        <translation>Нормальний</translation>
    </message>
    <message>
        <location filename="qdjviewsidebar.cpp" line="706"/>
        <source>Large</source>
        <comment>thumbnail menu</comment>
        <translation>Великий</translation>
    </message>
    <message>
        <location filename="qdjviewsidebar.cpp" line="712"/>
        <source>Smart</source>
        <comment>thumbnail menu</comment>
        <translation>Розумний</translation>
    </message>
</context>
<context>
    <name>QDjViewTiffExporter</name>
    <message>
        <location filename="qdjviewexporters.cpp" line="1338"/>
        <source>TIFF Document</source>
        <translation>Документ TIFF</translation>
    </message>
    <message>
        <location filename="qdjviewexporters.cpp" line="1339"/>
        <source>TIFF Files (*.tiff *.tif)</source>
        <translation>Файли TIFF (*.tiff *.tif)</translation>
    </message>
    <message>
        <location filename="qdjviewexporters.cpp" line="1516"/>
        <source>Cannot open output file.</source>
        <translation>Неможливо відкрити файл виведення.</translation>
    </message>
    <message>
        <location filename="qdjviewexporters.cpp" line="1628"/>
        <source>Out of memory.</source>
        <translation>Переповнення пам&apos;яті.</translation>
    </message>
    <message>
        <location filename="qdjviewexporters.cpp" line="1630"/>
        <source>Internal error.</source>
        <translation>Внутрішня помилка.</translation>
    </message>
    <message>
        <location filename="qdjviewexporters.cpp" line="1642"/>
        <source>TIFF export has not been compiled.</source>
        <translation>Експорт до TIFF не було скомпільовано.</translation>
    </message>
    <message>
        <location filename="qdjviewexporters.cpp" line="1360"/>
        <source>&lt;html&gt;&lt;b&gt;TIFF options.&lt;/b&gt;&lt;br&gt;The resolution box specifies an upper limit for the resolution of the TIFF images. Forcing bitonal G4 compression encodes all pages in black and white using the CCITT Group 4 compression. Allowing JPEG compression uses lossy JPEG for all non bitonal or subsampled images. Otherwise, allowing deflate compression produces more compact (but less portable) files than the default packbits compression.&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;b&gt;Параметри TIFF.&lt;/b&gt;&lt;br&gt;Поле \&quot;роздільна здатність\&quot; визначає верхню межу роздільної здатності зображень у форматі TIFF. Примусове двотональне стиснення G4 кодує всі сторінки у чорно-білому форматі за допомогою методу стиснення CCITT Group 4. Використання стиснення JPEG дозволяє стискати з втратами не чорно-білі зображення або зображення з підмножинами у формат JPEG. Або ж дозвіл на стиснення призводить до отримання менших (але і менш сумісних) файлів ніж звичайне побітове стиснення.&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="qdjviewexporters.cpp" line="1358"/>
        <source>TIFF Options</source>
        <comment>tab caption</comment>
        <translation>Параметри TIFF</translation>
    </message>
</context>
<context>
    <name>QDjVuHttpDocument</name>
    <message>
        <location filename="qdjvuhttp.cpp" line="140"/>
        <source>Unsupported url scheme &apos;%1:&apos;.</source>
        <translation>Непідтримувана схема адреси &apos;%1:&apos;.</translation>
    </message>
    <message>
        <location filename="qdjvuhttp.cpp" line="196"/>
        <source>Requesting &apos;%1&apos;</source>
        <translation>Запит &apos;%1&apos;</translation>
    </message>
    <message>
        <location filename="qdjvuhttp.cpp" line="274"/>
        <source>%1 while retrieving &apos;%2&apos;.</source>
        <translation>%1 під час отримання &apos;%2&apos;.</translation>
    </message>
    <message>
        <location filename="qdjvuhttp.cpp" line="234"/>
        <source>Received %1 data while retrieving %2.</source>
        <comment>%1 is a mime type</comment>
        <translation>Отримано %1 дані під час отримання %2.</translation>
    </message>
    <message>
        <location filename="qdjvuhttp.cpp" line="240"/>
        <source>Received http status %1 while retrieving %2.</source>
        <comment>%1 is an http status code</comment>
        <translation>Отримано статус http %1 під час отримання %2.</translation>
    </message>
</context>
</TS>
